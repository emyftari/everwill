import { useContext } from 'react'
import AuthContext from '../../context/auth/AuthContext'

import Snackbar from '@material-ui/core/Snackbar'
import MuiAlert from '@material-ui/lab/Alert'

function AlertMaterial(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />
}

const Alert = () => {
  const { error, clearErrors } = useContext(AuthContext)

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return
    }
    clearErrors()
  }


  return (
    <Snackbar
      open={error && true}
      autoHideDuration={3000}
      onClose={handleClose}
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
    >
      <AlertMaterial onClose={handleClose} severity="info">
        {error}
      </AlertMaterial>
    </Snackbar>
  )
}

export default Alert