import { Link, useHistory } from 'react-router-dom'
import { container, content, leftSide, block, before, rightSide } from './Showcase.module.css'
import chevronRight from '../../assets/icons/chevronRight.svg'

import Slideshow from './Slideshow'

const Showcase = ({ title, paragraph, btnText, linkText }) => {
  const history = useHistory()

  return (
    <div className={container}>
      <div className={content}>
        <div className={leftSide}>
          <div className={block}>
            <div className={before}>
              <h1>{title}</h1>
              <p>{paragraph}</p>
              <button onClick={() => history.push('/signup')}>{btnText}</button>
              {
                linkText ? (
                  <Link to='/wgs'>
                    {linkText}
                    <img src={chevronRight} alt="" />
                  </Link>
                ) : null
              }
            </div>
          </div>
        </div>
        <div className={rightSide}>
          <Slideshow />
        </div>
      </div>
    </div>
  )
}

export default Showcase