import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

import slide1 from '../../assets/images/slideshow/image1@2x.jpg'
import slide2 from '../../assets/images/slideshow/image2@2x.jpg'
import slide3 from '../../assets/images/slideshow/image3@2x.jpg'
import slide4 from '../../assets/images/slideshow/image4@2x.jpg'
import slide5 from '../../assets/images/slideshow/image5@2x.jpg'

const Slideshow = () => {
  const settings = {
    fade: true,
    infinite: true,
    speed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    arrows: false
  }

  return (
    <Slider {...settings}>
      <img src={slide1} alt="" />
      <img src={slide2} alt="" />
      <img src={slide3} alt="" />
      <img src={slide4} alt="" />
      <img src={slide5} alt="" />
    </Slider>
  )
}

export default Slideshow