import { card, iconContainer, icon, textContent } from './Card.module.css'

const Card = ({ svg, title, desc }) => {
  return (
    <div className={card}>
      <div className={iconContainer}>
        <div className={icon}>
          <img src={svg} alt="" />
        </div>
        <div></div>
      </div>
      <div className={textContent}>
        <strong>{title}</strong>
        <span>{desc}</span>
      </div>
    </div>
  )
}

export default Card
