import { useHistory } from 'react-router-dom'
import { container, content, leftSide, rightSide } from './GetStartedSection.module.css'

import Slideshow from '../showcase/Slideshow'

const GetStartedSection = () => {
  const history = useHistory()

  return (
    <div className={container}>
      <div className={content}>
        <div className={leftSide}>
          <div>
            <h1>Let’s start by creating a free account.</h1>
            <p>
              We help you securely store and manage your digital legacy of websites, social media, documents, last will and deliver it all the right people today or when you pass away.
            </p>
            <button onClick={() => history.push('/signup')}>Get started</button>
          </div>
        </div>
        <div className={rightSide}>
          <Slideshow />
        </div>
      </div>
    </div>
  )
}

export default GetStartedSection