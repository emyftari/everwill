import { container, content, grid, line } from './HelpSection.module.css'

import touch from '../../assets/icons/touch.svg'
import squarePlus from '../../assets/icons/squarePlus.svg'
import checkNote from '../../assets/icons/checkNote.svg'

const HelpSection = () => {
  return (
    <div className={container}>
      <div className={content}>
        <h2>Easy, fast and secure way to help your loved ones.</h2>
        <div className={grid}>
          <div className={line}></div>
          <article>
            <div>
              <img src={touch} alt="" />
            </div>
            <p>01. SELECT SITES & ACTIONS</p>
            <p>
              Select the websites you need help with and the action you want taken. Each website has a unique set of options.
            </p>
          </article>
          <article>
            <div>
              <img src={squarePlus} alt="" />
            </div>
            <p>02. UPLOAD & SIGN DOCUMENTS</p>
            <p>
              Provide information about the account, and upload documents like proof that the person has passed away and sign online power of attorney.
            </p>
          </article>
          <article>
            <div>
              <img src={checkNote} alt="" />
            </div>
            <p>03. EVERWILL MAKES IT DONE</p>
            <p>
              We'll reach out to all the global websites you selected and have them take the action you asked. We will keep you continuously updated.
            </p>
          </article>
        </div>
      </div>
    </div>
  )
}

export default HelpSection