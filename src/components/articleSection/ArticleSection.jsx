import { container, content, leftSide, rightSide, articles } from './ArticleSection.module.css'

import digitalLegacy from '../../assets/images/digitalLegacy.jpg'

const ArticleSection = ({ data }) => {
  return (
    <div className={container}>
      <div className={content}>
        <div className={leftSide}>
          <img src={digitalLegacy} alt="" />
        </div>
        <div className={rightSide}>
          <h2>How can Everwill help you today?</h2>
          <div className={articles}>
            {
              data.map(({ title, desc }) => (
                <article key={title}>
                  <p>{title}</p>
                  <p>{desc}</p>
                </article>
              ))
            }
          </div>
        </div>
      </div>
    </div>
  )
}

export default ArticleSection