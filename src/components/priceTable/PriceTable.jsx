import { container, title } from './PriceTable.module.css'

import blueCheck from '../../assets/icons/checkBlue.svg'
import { useHistory } from 'react-router-dom'

const PriceTable = ({ titleText, price, desc, btnText, list }) => {
  const history = useHistory()
  return (
    <div className={container}>
      <div className={title}>
        <h2>{titleText}</h2>
        <p>
          <span>${price}</span>/<span>month</span>
        </p>
      </div>
      <p>
        {desc}
      </p>
      <button onClick={() => history.push('/signup')}>
        <span>{btnText}</span>
      </button>
      <ul>
        {list.map(listItem => (
          <li key={listItem}>
            <p>
              <img src={blueCheck} alt="" />
              {listItem}
            </p>
          </li>
        ))}
      </ul>
    </div>
  )
}

export default PriceTable