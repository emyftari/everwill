import helmet from '../../assets/icons/helmet.svg'
import note from '../../assets/icons/note.svg'
import plane from '../../assets/icons/plane.svg'
import book from '../../assets/icons/book.svg'
import people from '../../assets/icons/people.svg'

export const cardsData = [
  {
    svg: helmet,
    title: 'Sites & Social',
    desc:
      'Secure online accounts like email, social, financial, photos, subscriptions etc.'
  },
  {
    svg: note,
    title: 'Safeguard Documents',
    desc:
      'Make your will, insurance, house title, contracts, passport, etc. easy to find.'
  },
  {
    svg: plane,
    title: 'Your Last Goodbyes',
    desc:
      'Deliver messages from the afterlife to your loved ones via email or videos.'
  },
  {
    svg: book,
    title: 'Will & Directives',
    desc: 'Create and share a last will and final wishes with family members.'
  },
  {
    svg: people,
    title: 'Help You Loved Ones',
    desc:
      'Protect the digital legacy of a loved one who is incapacitated or passed away.'
  }
]