import { actionSection, container, cardGrid } from './TakeAction.module.css'
import { cardsData } from './cardsData'
import Card from '../card/Card'

const TakeAction = () => {
  return (
    <div className={actionSection}>
      <div className={container}>
        <h2>Take action today</h2>
        <div className={cardGrid}>
          {cardsData.map((card) => (
            <Card key={card.title} {...card} />
          ))}
        </div>
      </div>
    </div>
  )
}

export default TakeAction
