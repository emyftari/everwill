import { useState, useContext } from 'react'
import { Link, NavLink } from 'react-router-dom'

import AuthContext from '../../../context/auth/AuthContext'

import {
  header,
  container,
  content,
  logo,
  active,
  openNav,
  closedNav
} from './Navbar.module.css'

import logoNav from '../../../assets/logo/logo.png'

const Section = () => {
  const [open, setOpen] = useState(false)
  const { isAuthenticated } = useContext(AuthContext)

  const handleClick = () => {
    setOpen(!open)
  }

  return (
    <div className={header}>
      <div className={container}>
        <div className={content}>
          <div className={logo}>
            <Link to='/'>
              <img src={logoNav} alt="" />
            </Link>
            <div>
              <button onClick={handleClick}>
                <svg
                  width="17"
                  height="9"
                  viewBox="0 0 17 9"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M17 0H0v2h17V0zM17 7H0v2h17V7z"
                    fill="#657795"
                  ></path>
                </svg>
              </button>
            </div>
          </div>
          <nav className={open ? openNav : closedNav}>
            <NavLink exact to='/' activeClassName={active}>
              For You
            </NavLink>
            <NavLink exact to='/loved-ones' activeClassName={active}>
              For Someone Else
            </NavLink>
            <NavLink exact to="/pricing" activeClassName={active}>
              Pricing
            </NavLink>
            <NavLink exact to="/login" activeClassName={active}>
              {isAuthenticated ? 'Dashboard' : 'Log in'}
            </NavLink>
            {!isAuthenticated && (
              <NavLink to="/signup" activeClassName={active}>
                Get started
              </NavLink>
            )}
          </nav>
        </div>
      </div>
    </div>
  )
}

export default Section
