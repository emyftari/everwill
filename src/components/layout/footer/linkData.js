export const linkDataOne = [
  { path: '/', title: 'For You' },
  { path: '/loved-ones', title: 'For Someone Else' },
  { path: '/security', title: 'Security' }
]

export const linkDataTwo = [
  { path: '/contact', title: 'Contact' },
  { path: '/terms-of-services', title: 'Terms of Service' },
  { path: '/privacy-policy', title: 'Privacy policy' },
  { path: '/signup', title: 'Get started' },
  { path: '/login', title: 'Log in' }
]