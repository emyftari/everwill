import { container, content, box } from './Footer.module.css'
import { linkDataOne, linkDataTwo } from './linkData'
import Logo from './logo/Logo'
import LinkList from './links/LinkList'
import Contacts from './contacts/Contacts'

const Footer = () => {
  return (
    <div className={container}>
      <footer>
        <div className={content}>
          <div className={box}>
            <Logo />
            <p>© 2021 All rights reserved.</p>
          </div>
          <div className={box}>
            <LinkList data={linkDataOne} />
          </div>
          <div className={box}>
            <LinkList data={linkDataTwo} />
          </div>
          <div className={box}>
            <Contacts />
          </div>
        </div>
      </footer>
    </div>
  )
}

export default Footer