import { logoContainer } from './Logo.module.css'
import logo from '../../../../assets/logo/logo.png'

const Logo = () => {
  return (
    <div className={logoContainer}>
      <img src={logo} alt="" />
    </div>
  )
}

export default Logo