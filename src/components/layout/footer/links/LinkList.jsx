import { Link } from 'react-router-dom'

const LinkList = ({ data }) => {
  return (
    <>
      {
        data.map(({ path, title }) => (
          <Link key={title} to={path}>{title}</Link>
        ))
      }
    </>
  )
}

export default LinkList