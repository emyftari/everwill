import { contactDetails } from './Contacts.module.css'

const Contacts = () => {
  return (
    <>
      <div className={contactDetails}>
        <a href="mailto:">
          <span>
            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path clipRule="evenodd" d="M18.1 16.3c0 .58-.47 1.05-1.05 1.05h-14c-.58 0-1.05-.47-1.05-1.05V8.6L10.05 3l8.05 5.6v7.7z" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round"></path><path d="M4.1 14.9l3.5-2.45h4.9L16 14.9M18.1 8.6l-4.2 2.8M2 8.6l4.2 2.8" stroke="#657795" strokeLinecap="round" strokeLinejoin="round"></path></svg>
          </span>
          contact@everwill.com
        </a>
        {/* <p>GoodTrust, Inc. <br />555 Bryant Street, <br />Palo Alto, CA 94301</p> */}
      </div>
      {/*       
      <div className={social}>
        <a href="/" rel="nonreferrer">
          <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M14 10a4 4 0 11-8 0 4 4 0 018 0z" stroke="currentColor" strokeWidth="2"></path><rect x="1" y="1" width="18" height="18" rx="5" stroke="currentColor" strokeWidth="2"></rect><path fillRule="evenodd" clipRule="evenodd" d="M15 6a1 1 0 100-2 1 1 0 000 2z" fill="currentColor"></path></svg>
        </a>
        <a href="/">
          <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M10 5.46H6.521V3.965c0-.702.484-.866.825-.866h2.436V.01L6.252 0c-3.2 0-3.807 2.304-3.807 3.778v1.681H0v3.135h2.445V18h4.076V8.594h3.14L10 5.46z" fill="currentColor"></path></svg>
        </a>
        <a href="/">
          <svg width="22" height="18" viewBox="0 0 22 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M22 2.13a8.965 8.965 0 01-2.592.716A4.55 4.55 0 0021.392.332a9.03 9.03 0 01-2.866 1.104A4.494 4.494 0 0015.23 0c-2.492 0-4.514 2.035-4.514 4.543 0 .356.04.703.117 1.036-3.75-.19-7.076-2-9.302-4.748a4.541 4.541 0 00-.61 2.284c0 1.576.795 2.968 2.007 3.782A4.453 4.453 0 01.885 6.33v.057a4.541 4.541 0 003.62 4.456 4.502 4.502 0 01-2.038.078 4.524 4.524 0 004.215 3.156A9.02 9.02 0 010 15.958C2 17.247 4.37 18 6.92 18c8.302 0 12.842-6.924 12.842-12.93 0-.196-.005-.392-.014-.588A9.178 9.178 0 0022 2.13z" fill="currentColor"></path></svg>
        </a>
        <a href="/">
          <svg width="19" height="18" viewBox="0 0 19 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M4.708 18V5.855H.263V18h4.445zM2.485 4.197C4.035 4.197 5 3.264 5 2.098 4.971.907 4.035 0 2.515 0S0 .907 0 2.098c0 1.166.965 2.099 2.456 2.099h.03zM7 18h3.931v-6.548c0-.35.026-.7.13-.95.284-.7.93-1.426 2.017-1.426 1.422 0 1.991 1.075 1.991 2.651V18H19v-6.723C19 7.676 17.06 6 14.474 6c-2.12 0-3.052 1.175-3.569 1.976h.026v-1.7h-3.93C7.05 7.375 7 18 7 18z" fill="currentColor"></path></svg>
        </a>
      </div> */}
    </>
  )
}

export default Contacts