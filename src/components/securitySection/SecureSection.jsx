import { container, content, leftSide, rightSide } from './SecureSection.module.css'
import { useHistory } from 'react-router-dom'
import callToAction from '../../assets/images/callToAction.jpg'
import plusBlue from '../../assets/icons/plus-blue.svg'

const SecureSection = () => {
  const history = useHistory()
  return (
    <div className={container}>
      <div className={content}>
        <div className={leftSide}>
          <h1>Secure digital assets and memories of your closest friends.</h1>
          <div>
            <p>
              The easy, safe and affordable way to safeguard your digital assets and share them with loved ones today or after you pass away.
            </p>
            <button onClick={() => history.push('/signup')}>
              <span>
                <img src={plusBlue} alt="" />
              </span>
              Get started
            </button>
          </div>
        </div>
        <div className={rightSide}>
          <img src={callToAction} alt="" />
        </div>
      </div>
    </div>
  )
}

export default SecureSection