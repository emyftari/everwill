import { useHistory } from 'react-router-dom'
import { container, content, description, rightSide } from './SecuritySection.module.css'

import securityInfo from '../../assets/images/securityInfo.jpg'

const SecuritySection = () => {
  const history = useHistory()
  return (
    <div className={container}>
      <div className={content}>
        <div className={description}>
          <h2>We are serious about security.</h2>
          <p>
            We realize that you are trusting the Everwill service with some of the most important information, data and documents that you own. Your information is encrypted and protected using industry-leading technology and security.
          </p>
          <p>KEEPING YOU SAFE</p>
          <p>
            Similar to your bank and healthcare information we secure your stored data as all data in transit from our site to secure encrypted data storage. Our communications and storage infrastructure are encrypted using the latest best practices. Only you have full access to your data.
          </p>
          <div>
            <button onClick={() => history.push('/security')}>
              <span>
                Learn more
              </span>
            </button>
          </div>
        </div>
        <div className={rightSide}>
          <div>
            <img src={securityInfo} alt="" />
          </div>
        </div>
      </div>
    </div>
  )
}

export default SecuritySection