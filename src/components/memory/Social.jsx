import { useHistory } from 'react-router-dom'
import { box } from './Social.module.css'

const Social = ({ svg, title, desc }) => {
  const history = useHistory()
  return (
    <div className={box} onClick={() => history.push('/wgs')}>
      <img src={svg} alt="" />
      <span>{title}</span>
      <span>{desc}</span>
    </div>
  )
}

export default Social