import { useHistory } from 'react-router-dom'
import { container, background, content, leftSide, rightSide } from './Memorialize.module.css'

import Social from './Social'
import { socialData } from './socialData'

import plusBlue from '../../assets/icons/plus-blue.svg'

const Memorialize = () => {
  const history = useHistory()

  return (
    <div className={container}>
      <div className={background}>
        <div className={content}>
          <div className={leftSide}>
            <h1>Memorialize or close down accounts at ease</h1>
            <p>Take charge of social media sites quickly to protect the memory of your loved ones. Secure important photos and other priceless content and stop paying subscriptions quickly.</p>
            <button onClick={() => history.push('/wgs')}>
              <span>
                <img src={plusBlue} alt="" />
              </span>
              Get Started
            </button>
          </div>
          <div className={rightSide}>
            {
              socialData.map(dataSet => (
                <Social key={dataSet.title} {...dataSet} />
              ))
            }
          </div>
        </div>
      </div>
    </div>
  )
}

export default Memorialize