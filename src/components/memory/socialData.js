import plus from '../../assets/icons/plus.svg'
import facebook from '../../assets/icons/facebook.svg'
import google from '../../assets/icons/google.svg'
import netflix from '../../assets/icons/netflix.svg'

export const socialData = [
  {
    svg: plus,
    title: 'Protect your loved ones',
    desc: 'Select which sites and social media accounts are most valuable.'
  },
  {
    svg: facebook,
    title: 'Memorialize Facebook',
    desc: 'Memorialize Facebook pages to protect the legacy of loved ones.'
  },
  {
    svg: google,
    title: 'Extract Google Photos',
    desc: 'Make sure that all your invaluable photos and videos are not lost.'
  },
  {
    svg: netflix,
    title: 'Stop Netflix subscription',
    desc: 'Annoyed by recurring payments of Netflix? Take action right now.'
  },
]