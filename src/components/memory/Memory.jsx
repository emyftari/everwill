import { container, content, text, desc, social, background } from './Memory.module.css'

import Social from './Social'
import { socialData } from './socialData'

const Memory = () => {
  return (
    <div className={container}>
      <div className={content}>
        <div className={text}>
          <div className={desc}>
            <h1>Protect the memory of your loved ones too.</h1>
            <p>
              Take charge of social media sites quickly to protect the memory of your loved ones. Secure important photos and other priceless content and stop paying subscriptions quickly.
            </p>
          </div>
          <div className={social}>
            {
              socialData.map(dataSet => (
                <Social key={dataSet.title} {...dataSet} />
              ))
            }
          </div>
        </div>
        <div className={background}></div>
      </div>
    </div>
  )
}

export default Memory