import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  USER_LOADED,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  CLEAR_ERRORS,
  UPDATE_SUCCESS
} from '../types'

const AuthReducer = (state, action) => {
  switch (action.type) {
    case USER_LOADED:
      return {
        ...state,
        isAuthenticated: true,
        loading: false,
        loadingUser: false,
        user: action.payload.data
      }
    case UPDATE_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        loading: false,
        profileUpdated: true,
        user: action.payload.data
      }
    case REGISTER_SUCCESS:
      localStorage.setItem('token', action.payload)
      return {
        ...state,
        token: action.payload,
        isAuthenticated: true,
        loading: false,
        error: false
      }
    case LOGIN_SUCCESS:
      localStorage.setItem('token', action.payload.data.auth_token)
      return {
        ...state,
        ...action.payload,
        isAuthenticated: true,
        loading: false,
      }
    case AUTH_ERROR:
    case LOGIN_FAIL:
      localStorage.removeItem('token')
      return {
        ...state,
        token: null,
        isAuthenticated: false,
        profileUpdated: false,
        loading: false,
        user: null,
        error: action.payload
      }
    case REGISTER_FAIL:
      localStorage.removeItem('token')
      return {
        ...state,
        token: null,
        isAuthenticated: false,
        profileUpdated: false,
        loading: false,
        user: null,
        error: action.payload[0]
      }
    case LOGOUT:
      localStorage.removeItem('token')
      return {
        ...state,
        token: null,
        isAuthenticated: false,
        profileUpdated: false,
        loading: false,
        user: null,
        error: false
      }
    case "UPDATE_FAILED": {
      return {
        ...state,
        error: action.payload
      }
    }
    case CLEAR_ERRORS:
      return {
        ...state,
        error: false
      }
    case 'RESET_PASSWORD':
      return {
        ...state,
        error: action.payload
      }
    case 'RESET_FAIL':
      return {
        ...state,
        error: action.payload
      }
    case 'GOOGLE_LOGIN':
      localStorage.setItem('token', action.payload)
      return {
        ...state,
        isAuthenticated: true,
        loading: false,
      }
    default:
      return state
  }
}

export default AuthReducer