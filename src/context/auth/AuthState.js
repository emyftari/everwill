import React, { useReducer } from 'react'
import axios from 'axios'
import AuthContext from './AuthContext'
import AuthReducer from './AuthReducer'
import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  UPDATE_SUCCESS,
  USER_LOADED,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  CLEAR_ERRORS
} from '../types'

const AuthState = (props) => {
  axios.defaults.baseURL = 'https://mighty-peak-18791.herokuapp.com'
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.token

  const initialState = {
    token: localStorage.getItem('token'),
    isAuthenticated: null,
    profileUpdated: null,
    loading: true,
    user: null,
    error: null
  }

  const [state, dispatch] = useReducer(AuthReducer, initialState)

  // Load User
  const loadUser = async () => {
    try {
      const res = await axios.get('/api/auth/profile')
      dispatch({ type: USER_LOADED, payload: res.data })
    } catch (error) {
      dispatch({ type: AUTH_ERROR })
    }
  }

  // Register User
  const register = async formData => {
    try {
      const res = await axios.post('/api/auth/register', formData)

      dispatch({
        type: REGISTER_SUCCESS,
        payload: res.data.data.auth_token
      })

      loadUser()
    } catch (err) {
      dispatch({
        type: REGISTER_FAIL,
        payload: err.response.data.errors[Object.keys(err.response.data.errors)[0]]
      })
    }
  }

  // Login User
  const login = async formData => {
    try {
      const res = await axios.post('/api/auth/login', formData)
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data
      })

      loadUser()
    } catch (err) {
      dispatch({
        type: LOGIN_FAIL,
        payload: err.response.data.message
      })
    }
  }

  // Update User
  const update = async formData => {
    try {
      const res = await axios.post('/api/users', formData)

      dispatch({
        type: UPDATE_SUCCESS,
        payload: res.data
      })

      loadUser()
    } catch (error) {
      dispatch({
        type: 'UPDATE_FAILED',
        payload: error.response.data.errors.birthday[0]
      })
    }
  }

  const resetPassword = async email => {
    try {
      const res = await axios.post('/api/auth/forgot-password', email)
      console.log(res)
      dispatch({ type: 'RESET_PASSWORD', payload: 'Please check your email!' })
    } catch (error) {
      dispatch({ type: 'RESET_FAIL', payload: 'Invalid Email!' })
    }
  }

  const recoverPassword = async (formData, v_key) => {

    try {
      await axios.post(`/api/auth/reset-password?v_key=${v_key}`, formData)
    } catch (error) {

      console.log(error.response.data.errors.confirm_password)
      dispatch({
        type: 'UPDATE_FAILED',
        payload: error.response.data.errors.confirm_password[0]
      })
    }
  }

  // const enable2fa = async () => { }

  const facebookLogin = async response => {
    try {
      const res = await axios.post('/api/register-social', { token: response.accessToken, method: 'facebook' })

      dispatch({
        type: 'GOOGLE_LOGIN',
        payload: res.data.data.token
      })

      loadUser()

    } catch (error) {
      console.log(error.response)
    }
  }

  const googleLogin = async response => {
    try {
      const res = await axios.post('/api/register-social', { token: response.accessToken, method: 'google' })

      dispatch({
        type: 'GOOGLE_LOGIN',
        payload: res.data.data.token
      })

      loadUser()

    } catch (error) {
      console.log(error.response)
    }
  }

  // Logout
  const logout = () => dispatch({ type: LOGOUT })

  // Clear Errors
  const clearErrors = () => dispatch({ type: CLEAR_ERRORS })

  // Buy plan
  const buyPlan = async (data) => {
    const formData = {
      card_number: data.card_number,
      card_cvc: data.card_cvc,
      zip_code: data.zip_code,
      card_date: '12/31',
      remember_card: false
    }

    try {
      const res = await axios.post('/api/buy-plan/2', formData)
      console.log(res)
    } catch (error) {
      console.log(error.response)
    }
  }

  // Contact us
  const contactUs = async data => {
    const formData = {
      email: data.email,
      phone_number: data.phone_number,
      full_name: data.full_name,
      category: data.category,
      message: data.notes
    }

    try {
      const res = await axios.post('/api/contact-us', formData)
      console.log(res.data.data)
    } catch (error) {
      console.log(error.response)
    }
  }

  return (
    <AuthContext.Provider
      value={{
        token: state.token,
        isAuthenticated: state.isAuthenticated,
        loading: state.loading,
        user: state.user,
        error: state.error,
        profileUpdated: state.profileUpdated,
        register,
        update,
        loadUser,
        login,
        logout,
        facebookLogin,
        googleLogin,
        resetPassword,
        recoverPassword,
        clearErrors,
        contactUs,
        buyPlan
      }}
    >
      {props.children}
    </AuthContext.Provider>
  )
}

export default AuthState
