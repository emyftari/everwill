import { useForm, FormProvider } from 'react-hook-form'
import { useContext } from 'react'
import { useHistory } from 'react-router-dom'

import AuthContext from '../../../../context/auth/AuthContext'

import PageContainer from '../../../../app/components/templates/PageContainer'
import CardInput from '../../../../app/components/inputs/creditCardInput/CardInput'

import {
  header,
  form__container,
  left__side,
  left__side__content,
  divider,
  details,
  details__container,
  details__top,
  details__bottom,
  right__side,
  fields,
  cards,
  button__container,
  right__button,
  left__button
} from './Checkout.module.css'

import { JCB, Master, Visa, AmericanExpress } from '../../../../app/assets/Icons'

const Checkout = () => {
  const methods = useForm()
  const { buyPlan } = useContext(AuthContext)
  const history = useHistory()

  const onSubmit = async data => {
    await buyPlan(data)
    history.push('/profile')
  }

  return (
    <PageContainer>
      <h1 className={header}>Checking out is easy - and secure.</h1>
      <FormProvider {...methods}>
        <form onSubmit={methods.handleSubmit(onSubmit)}>
          <div className={form__container}>
            <div className={left__side}>
              <p>Your order</p>
              <div className={left__side__content}>
                <div className={details}>
                  <div className={details__container}>
                    <div className={details__top}>
                      <p >1 Month Premium Free Trial</p>
                      <p>$0/Month</p>
                    </div>
                    <div className={details__bottom}>
                      <p>May 30, 2021 - Jun 30, 2021</p>
                      <p>$5.99/Month after trial</p>
                    </div>
                  </div>
                  <div className={divider}></div>
                </div>
                <p>No commitment. Cancel at any time in your Account Settings at least one day before each renewal date. Plan automatically renews until cancelled.</p>
              </div>
            </div>
            <div className={right__side}>
              <p>100% secured payment</p>
              <div className={cards}>
                <JCB />
                <Master />
                <Visa />
                <AmericanExpress />
              </div>
              <div className={fields}>
                <CardInput name='card_number' label='Card number' mask='9999 9999 9999 9999' required />
                <CardInput name='zip_code' label='ZIP code' mask='99999' required />
                <CardInput name='card_date' label='Expiration Date' mask='99/99' required />
                <CardInput name='card_cvc' label='CVV/CVC' mask='999' required />
              </div>
            </div>
          </div>
          <div className={button__container}>
            <div className={left__button}>

            </div>
            <div className={right__button}>
              <div>
                <button>Confirm</button>
              </div>
            </div>
          </div>
        </form>
      </FormProvider>
    </PageContainer>
  )
}

export default Checkout