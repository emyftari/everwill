import { useHistory } from 'react-router-dom'

import { container, title } from './PriceTable.module.css'
import { CheckBlueSvg } from '../../../../assets/icons/IconSvg'

const PriceTable = ({ titleText, price, desc, btnText, list }) => {
  const history = useHistory()

  const handleClick = () => {
    if (titleText === 'Free') {
      history.push('/profile')
    } else {
      history.push('/checkout')
    }
  }

  return (
    <div className={container}>
      <div className={title}>
        <h2>{titleText}</h2>
        <p>
          <span>${price}</span>/<span>month</span>
        </p>
      </div>
      <p>{desc}</p>
      <button onClick={handleClick}>
        <span>{btnText}</span>
      </button>
      <ul>
        {list.map((listItem) => (
          <li key={listItem}>
            <p>
              <CheckBlueSvg />
              {listItem}
            </p>
          </li>
        ))}
      </ul>
    </div>
  )
}

export default PriceTable
