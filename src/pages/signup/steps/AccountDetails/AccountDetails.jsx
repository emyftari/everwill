import { useContext, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { FormProvider, useForm } from 'react-hook-form'

import AuthContext from '../../../../context/auth/AuthContext'

import Alert from '../../../../components/snackbar/Alert'
import TextInput from '../../../customInputs/TextInput'
import CountrySelect from '../../../customInputs/CountrySelect'
import DateInput from '../../../customInputs/DateInput'
import GenderSelect from '../../../customInputs/GenderSelect'
import SocialStatusSelect from '../../../customInputs/SocialStatusSelect'
import ChildrenSelect from '../../../customInputs/ChildrenSelect'

import {
  formContainer,
  formContent,
  center,
  section,
  sectionTitle,
  infoP,
  fieldContainer,
  button
} from './AccountDetails.module.css'

import { main, container } from '../../Signup.module.css'


const AccountDetails = () => {
  const { update, profileUpdated } = useContext(AuthContext)
  const history = useHistory()
  const methods = useForm()

  useEffect(() => {
    if (profileUpdated) {
      history.push('/dashboard')
    }
  }, [profileUpdated, history])

  const onSubmit = async (data) => {
    const formatData = {
      ...data,
      zip_code: parseInt(data.zip_code),
      children: parseInt(data.children)
    }
    await update(formatData)
  }

  return (
    <div className={main}>
      <Alert />
      <div className={container}>
        <div className={formContainer}>
          <h1>Help us learn more about you.</h1>
          <FormProvider {...methods} autoComplete='off'>
            <form autoComplete='off' onSubmit={methods.handleSubmit(onSubmit)} >
              <div className={formContent}>
                <div className={center}>
                  <div center={section}>
                    <p className={sectionTitle}>How may we contact you?</p>
                    <div className={fieldContainer}>
                      <TextInput name="first_name" label='Name' half required='required' />
                      <TextInput name="last_name" label='Surname' half required='required' />
                      <TextInput name="email" label='Email' required={false} />
                      <TextInput name="phone_number" label='Phone number' required={false} />
                      <p className={infoP}>
                        6-digit confirmation code will be sent to this number in the
                        next step.
                  </p>
                    </div>
                  </div>
                  {/* <div className={section}>
                    <p className={sectionTitle}>Profile Picture</p>
                    <div className={fieldContainer}>
                       <div className={buttonContainer}>
                        <button type="button" name="avatar">
                          <span>
                            <PlusSvg />
                          </span>
                          Upload a profile picture
                        </button>
                        <input type="file" accept="image/*" />
                      </div> 
                    </div>
                  </div> */}
                  <div className={section}>
                    <p className={sectionTitle}>Address</p>
                    <div className={fieldContainer}>
                      <TextInput name="street_address" label='Street Address' required='required' />
                      <TextInput name="city" label='City' required='required' />
                      <TextInput name="zip_code" label='ZIP Code' half required='required' type='number' />
                      <TextInput name='state' label="State/Provice/Region" half required='required' />
                      <CountrySelect />
                    </div>
                  </div>
                  <div className={section}>
                    <p className={sectionTitle}>MORE ABOUT YOU</p>
                    <div className={fieldContainer}>
                      <DateInput half />
                      <GenderSelect half />
                      <SocialStatusSelect half />
                      <ChildrenSelect half />
                    </div>
                  </div>
                </div>
              </div>
              <div className={button}>
                <button>Continue</button>
              </div>
            </form>
          </FormProvider>
        </div>
      </div>
    </div>
  )
}

export default AccountDetails
