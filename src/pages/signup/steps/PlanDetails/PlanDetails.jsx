import { useHistory } from 'react-router-dom'

import {
  main,
  container,
  content,
  head,
  planContainer,
  step,
  back
} from './PlanDetails.module.css'

import PriceTable from '../PriceTable/PriceTable'

import { free, premium } from '../planData'

import { BackArrow } from '../../../../app/assets/Icons'

const PlanDetails = () => {
  const history = useHistory()

  return (
    <div className={main}>
      <div className={container}>
        <div className={content}>
          <div className={back}>
            <div onClick={() => history.goBack()}>
              <BackArrow />
            </div>
          </div>
          <div className={step}>
          </div>
          <div className={head}>
            <h1>Our pricing</h1>
            <p>
              Select a plan that suits you the best. Go with the Free plan if
              you want to explore Everill or upgrade to Premium to enjoy the
              full experience.
            </p>
          </div>
          <div className={planContainer}>
            <PriceTable {...free} />
            <PriceTable {...premium} />
          </div>
        </div>
      </div>
    </div>
  )
}

export default PlanDetails
