import { useContext, useEffect } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { Link, useHistory } from 'react-router-dom'

import GoogleLogin from 'react-google-login'
import FacebookLogin from 'react-facebook-login'

import Alert from '../../../../components/snackbar/Alert'

import AuthContext from '../../../../context/auth/AuthContext'

import TextInput from '../../../customInputs/TextInput'

import { main, container, center, rightSide, step, button, checkbox, inp } from '../../Signup.module.css'
import { social__button, leftSide } from '../../../login/Login.module.css'
import facebook from '../../../../assets/icons/facebook.svg'
import googleWhite from '../../../../assets/icons/googleWhite.svg'

const EmailDetails = () => {
  const { register, isAuthenticated, error, facebookLogin, googleLogin } = useContext(AuthContext)
  const history = useHistory()
  const methods = useForm()

  useEffect(() => {
    if (isAuthenticated && !error) history.push('/plan')

    // eslint-disable-next-line
  }, [isAuthenticated, error])

  const onSubmit = data => {
    register(data)
  }

  return (
    <div className={main}>
      <Alert />
      <div className={container}>
        <FormProvider {...methods}>
          <form onSubmit={methods.handleSubmit(onSubmit)}>
            <p className={step}>Step 1 of 3</p>
            <h1>First, let's set up your account.</h1>
            <div className={center}>
              <div className={leftSide}>
                <p>Sign up with an existing account</p>
                <div>
                  <FacebookLogin
                    appId="1117697788639861"
                    fields="name,email,picture"
                    textButton='Continue with Facebook'
                    cssClass={social__button}
                    callback={facebookLogin}
                    icon={<img src={facebook} alt="" />}
                  />
                  <GoogleLogin
                    clientId="126571944175-ug751vd0rk493gpg1nget2mnroqjjg0m.apps.googleusercontent.com"
                    onSuccess={googleLogin}
                    onFailure={googleLogin}
                    cookiePolicy={'single_host_origin'}
                    render={renderProps => (
                      <button onClick={renderProps.onClick} className={social__button}>
                        <img src={googleWhite} alt="" />
                        Continue with Google
                      </button>
                    )}
                  />
                </div>
              </div>
              <div className={rightSide}>
                <p>SIGN UP WITH EMAIL & PASSWORD</p>
                <div>
                  <TextInput name="email" type="email" label='Email' required='required' />
                  <TextInput name="password" type="password" label='Password' required='required' />
                  {/* <label className={checkbox}>
                    <input name="is_promocode" type="checkbox" className={inp} />
                    <div>
                      <svg width="15" height="11" viewBox="0 0 15 11" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M14.694.28a1 1 0 01.026 1.414l-8.666 9a1 1 0 01-1.441 0L.28 6.194a1 1 0 111.44-1.388l3.613 3.752L13.28.306A1 1 0 0114.694.28z" fill="currentColor"></path></svg>
                    </div>
                    <p>I have a sponsor’s promo code</p>
                  </label> */}
                </div>
                <p>
                  Already have an account? <Link to="/login">Log in.</Link>
                </p>
              </div>
            </div>
            <div className={button}>
              <button type="submit">Sign up with email</button>
            </div>
          </form>
        </FormProvider>
      </div>
    </div>
  )
}

export default EmailDetails
