import { main, container, section } from './PrivacyPolicy.module.css'

const Section = () => {
  return (
    <div className={main}>
      <div className={container}>
        <div className={section}>
          <div>
            <p>Everwill</p>
            <h1>Privacy policy</h1>
            <p>Last updated October 22, 2020</p>
            <p>
              We at Everwill Inc. (“Everwill,” “we,” and “us”) believe you
              should always know what data we collect from you and how we use
              it. This privacy notice provides information on how we collect and
              process your personal data when you use our products or services
              (collectively the “Services”) or visit our website
              www.myEverwill.com (the “Site”), contact us or have a
              relationship with us.
            </p>
            <p>
              PLEASE READ THIS POLICY CAREFULLY TO UNDERSTAND OUR POLICIES AND
              PRACTICES REGARDING YOUR INFORMATION AND HOW WE WILL TREAT IT. IF
              YOU DO NOT AGREE TO THE PRIVACY POLICY, INCLUDING ANY CHANGES
              THERETO, THEN YOU MUST NOT ACCESS AND USE THE SERVICES.
            </p>
            <p>
              By using the Site and the Services, and in particular when you
              fill out an account, you agree to the terms of this Privacy Policy
              and the collection, storage, use, processing and disclosure by
              Everwill and its services providers of the Personal Information
              that you enter into the Services. If you don’t agree with this
              Privacy Policy or the Terms of Service, please do not use the
              Services.
            </p>
            <h2>1. Information We Collect</h2>
            <p>
              When you use our Services, we collect the following types of
              information (collectively “Personal Information”).
            </p>
            <h3>Account Information</h3>
            <p>
              Some general and basic information is required to create an
              account with Everwill and while signing up for our Services, such
              as your name, email address, phone number, password, and in some
              cases your date of birth and gender, language preferences, etc.
            </p>
            <h3>Other Information</h3>
            <p>
              Your private, sensitive, personal information other than the basic
              information to form an account that you voluntarily submit through
              the Services, including your stored documents, other passwords,
              financial and legal information (“Secure Information”). Our
              systems will indicate whether you have submitted certain
              information if it is provided under certain functions of our
              Services but we do not access this information for the puspose of
              operating our Services other than storing the information until it
              is released and shared as per your instructions.{' '}
            </p>
            <h3>Health Information</h3>
            <p>
              If the situation calls for it, you may voluntarily provide us with
              your health and medical information which may be considered
              “Protected Health Information” or “PHI,” as defined by the Health
              Insurance Portability and Accountability Act of 1996 (“HIPAA”).
              Our systems will indicate whether you have submitted certain
              information if it is provided under certain functions of our
              Services but we do not access this information for the puspose of
              operating our Services other than storing the information until it
              is released and shared as per your instructions.
            </p>
            <h3>Additional Information</h3>
            <p>
              To help improve your experience or enable certain features of the
              Services, you may choose to provide us with additional information
              such as a profile photo, biography, nationality, location, and
              profession.
            </p>
            <h3>Data From Third Party Services</h3>
            <p>
              If you choose to connect your account on our Services to your
              account on another third party services, such as financial
              advisors, legal advisors or other third party service providers,
              we may receive information from mentioned third parties as well.
              We may also partner with third parties, such as funeral providers,
              employers, insurance companies, that offer Everwill’s Services to
              their customers, employees, clients, and consumers. In such cases,
              those organizations may provide us with your name, email address,
              or similar information, including personal health information, so
              that we can provide the Services to these third parties.
            </p>
            <p>
              For the Services to fully function, it may therefore be necessary
              for you to authorize one or more third party providers to provide
              us with the information needed for performing the Services. This
              will only be done with your express permission, which will be
              collected upon the creation of your account with the third party
              service providers that we have an agreement with and who you have
              given the permission to share the information with us.
            </p>
            <h3>Payment and Card Information</h3>
            <p>
              If you purchase a service or product from us, your credit card
              number, type, expiration date, name and billing address
              (collectively, “Payment Information”) is collected, stored, used
              and processed by Stripe Inc. (“Stripe”), our third party payment
              processing company, and not by us. Accordingly, the collection,
              storage, use and processing of your Payment Information is
              governed by Stripe’s applicable terms of service available at
              https://stripe.com/us/terms and privacy policy available at
              https://stripe.com/us/privacy. From time to time, we may request
              and receive some of your Payment Information from Stripe in order
              to complete certain transactions you initiated through the
              Services, to enroll you in a discount or other rebate program you
              elected to participate in, to protect against or identify
              potentially fraudulent transactions, or otherwise as necessary to
              manage our business. Accordingly, sometimes we may obtain Payment
              Information pertaining to you as a result of your use of the
              Services but we will only use any such Payment Information we
              obtain in furtherance of the Services.
            </p>
            <h3>Information Collected Automatically</h3>
            <p>
              Whenever you interact with our Services, we automatically receive
              and record information on our server logs from your browser or
              device, which may include your internet protocol (“IP”) address,
              geolocation data, device identification, “cookie” information, the
              type of browser and/or device you’re using to access our Services,
              and the page or feature you requested. “Cookies” are identifiers
              we transfer to your browser or device that allow us to recognize
              your browser or device and tell us how and when pages and features
              in our Services are visited and by how many people. You may be
              able to change the preferences on your browser or device to
              prevent or limit your device’s acceptance of cookies, but this may
              prevent you from taking advantage of some of our features.
            </p>
            <h3>Messages</h3>
            <p>
              We collect information about you when you send, receive, or engage
              with messages in connection with our Services.
            </p>
            <h2>2. How We Use Your Personal Information</h2>
            <p>We use the information we collect for the following purposes:</p>
            <h3>Registration</h3>
            <p>
              When you register for the Service, or when a third party registers
              you for the Service on your behalf, we collect your Personal
              Information as part of the registration process, including but not
              limited to your name, address, phone number, email address.{' '}
            </p>
            <h3>Service Delivery</h3>
            <p>
              We collect information to provide you with our Services and to
              manage your relationship with us, including to send you technical
              notices, updates, offers, promotions, rewards, events, news,
              security alerts, support and administrative messages.
            </p>
            <h3>Improve, Personalize, and Develop the Services</h3>
            <p>
              We use the information we collect to improve and personalize the
              Services and to develop new ones. For example, we use the
              information to troubleshoot and protect against errors; perform
              data analysis and testing; conduct research and surveys; and
              develop new features and Services.
            </p>
            <h3>Promote Safety and Security</h3>
            <p>
              We use the information we collect to promote the safety and
              security of the Services, our users, and other parties. For
              example, we may use the information to authenticate users,
              facilitate secure payments, protect against fraud and abuse,
              respond to a legal request or claim, conduct audits, and enforce
              our terms and policies.
            </p>
            <h3>Legal Obligation</h3>
            <p>
              We collect information to enforce or defend any legal obligation
              or rights or to comply with any legal obligation.
            </p>
            <h3>Aggregate Insights</h3>
            <p>
              We use your data to produce and share aggregated insights that do
              not identify you for statistical purposes and to improve the
              Services. For example, we may use your data to generate statistics
              about our users, their profession or nationality, to publish
              cognitive demographic insights.
            </p>
            <h2>3. How Information Is Shared</h2>
            <p>
              Subject to applicable state and federal law, including but not
              limited to our obligations under HIPAA and HITECH, we may share
              aggregated, de-identified or identified versions of your Personal
              Information with our subsidiaries, affiliates, partners,
              investors, and contractors for the purposes below.
            </p>
            <p>
              Everwill personnel may access Personal Information in order to
              provide Services and customer support to our users. We have
              developed internal processes to govern the way our personnel
              accesses user data. Internal access to this data is supervised to
              ensure user privacy.
            </p>
            <p>
              We use third parties to process some of your Personal Information.
              When we allow them access to your data, we do not permit them to
              use it for their own purposes. We have in place with each
              processor, a contract that requires them only to process the data
              on our instructions and to take proper care in using it. They are
              not permitted to keep the data after our relationship with them
              has ended, unless required by law. If you connect or communicate
              with us using social media, for example, Instagram, Twitter or
              Facebook, your personal information will be shared with us and the
              social media platform. We may be able to communicate with you
              using that platform but we are not otherwise responsible for the
              ways in which these platforms might use your personal information.
              Please look at the privacy notices and preference centers
              available on these platforms to understand how your personal
              information is used and what options you might have in respect of
              it.
            </p>
            <p>
              <u>We use service providers for the following:</u>
            </p>
            <ul class="css-4ihv1n e162lf2u4">
              <li>
                <p>Cloud Hosting Services – to provide hosting services;</p>
              </li>
              <li>
                <p>
                  Analytic Tools – to provide data analytics on how the website
                  is used;
                </p>
              </li>
              <li>
                <p>
                  Authorized contractors and third party companies involved in
                  the development, build, operation and optimization of our
                  Services and websites;
                </p>
              </li>
              <li>
                <p>
                  Authorized third party companies involved in processing of
                  information generated by use of the Services and to manage our
                  communications;
                </p>
              </li>
              <li>
                <p>
                  Authorized service providers to provide maintenance and
                  support, including customer support;
                </p>
              </li>
              <li>
                <p>
                  other service providers which may be used from time to time.
                </p>
              </li>
            </ul>
            <p>
              We will update this page with details of any major service
              providers (or categories of service providers) who may have access
              to personal information.
            </p>
            <h3>Law and the Public Interest</h3>
            <p>
              We may preserve or disclose information about you to comply with a
              law, regulation, legal process, or governmental request; to assert
              legal rights or defend against legal claims; or to prevent,
              detect, or investigate illegal activity, fraud, abuse, violations
              of our terms, or threats to the security of the Services or the
              physical safety of any person.
            </p>
            <h3>Non-Personal Information</h3>
            <p>
              We may share non-personal information that is aggregated or
              de-identified so that it cannot reasonably be used to identify an
              individual. We may disclose such information publicly and to third
              parties, for example, in public reports, to partners under
              agreement with us, or as part of the community benchmarking
              information we provide to users of our subscription services.
            </p>
            <h3>Anonymous or Aggregated Data</h3>
            <p>
              We may share de-identified versions of your Personal Information
              and other data in aggregated or non-aggregated forms with
              partners, investors, contractors or prospective advertisers and
              research partners for any purposes related to our business
              practices.
            </p>
            <h3>Successor Businesses</h3>
            <p>
              We may share your Personal Information during a corporate
              transaction like a merger, or sale of our assets, or as part of
              the due diligence for such contemplated transactions. If a
              corporate transaction occurs, we will provide notification of any
              changes to control of your personal information, as well as
              choices you may have at that time.
            </p>
            <h3>Sharing with Authorized Persons</h3>
            <p>
              As part of your use of the Services, you may designate specific
              persons who may access your information given certain conditions
              met. We may share your Personal Information with your designated
              specific persons and only pursuant to and in accordance with your
              explicit instruction, including the permissions you set that give
              any particular Deputy access to designated portions of the
              Services.
            </p>
            <h3>Sharing with Financial Advisors or Partner Providers</h3>
            <p>
              We may share your Personal Information with third party service
              providers for the case where you signed up for the Services using
              an invitations received from external advisors and third party
              service providers such as financial advisors, funeral providers or
              any other third party provider.
            </p>
            <h2>
              4. Your Rights to Access and Control Your Personal Information
            </h2>
            <p>
              We give you account settings and tools to access and control your
              Personal Information. If you live in certain jurisdictions, you
              may have legal rights with respect to your information, which your
              account settings and tools may allow you to exercise.
            </p>
            <h3>Accessing and Exporting Data</h3>
            <p>
              By logging into your account, you can access much of your personal
              information, including your account settings. You can also
              download information in a commonly used file format, including
              data about your activities and results.
            </p>
            <h3>Editing and Deleting Data</h3>
            <p>
              By logging into your account and using your account settings, you
              can change and delete your personal information.
            </p>
            <p>
              If you choose to delete your account, please note that all data,
              scores, and information will be permanently deleted, except as
              noted below.
            </p>
            <p>
              We retain your Personal Information even after you have closed
              your account if reasonably necessary to comply with our legal
              obligations (including law enforcement requests), meet regulatory
              requirements, resolve disputes, maintain security, prevent fraud
              and abuse, enforce our Terms and Conditions, or fulfill your
              request to “unsubscribe” from further messages from us. We will
              retain de-personalized information after your account has been
              closed.
            </p>
            <p>
              Information you have shared with others may remain visible after
              you closed your account or deleted the information from your own
              profile or mailbox, and we do not control data that other users
              copied out of our Services.
            </p>
            <p>
              Note that while most of your information will be deleted within 30
              days, it may take up to 90 days to delete all of your information,
              like data stored in our backup systems.
            </p>
            <h2>5. How Long do We Keep the Data?</h2>
            <p>
              We maintain information about you for as long as we provide
              services to you, and as long it is required for us to perform any
              related business activities. After your information is no longer
              necessary for our business purposes, we will destroy the
              information unless we have agreed with you that we should keep the
              information, or a court or administrative order is made to
              preserve the information, or if it is otherwise stipulated by law.
            </p>
            <h2>6. Our Policies for Children</h2>
            <p>
              The Services are not directed to children under 18 years of age.
              Unless otherwise disclosed during collection and with parent or
              guardian consent, Everwill does not knowingly collect Personal
              Information from children under 18 years of age. Should it come to
              our knowledge that we have collected Personal Information of a
              person under 18 years of age without verified consent from a
              parent or guardian, we will take action to remove the information
              from our systems.
            </p>
            <h2>7. Information Security</h2>
            <p>
              We have put in place appropriate security measures to prevent your
              Personal Information from being accidentally lost, used or
              accessed in an unauthorized way, altered or disclosed. In
              addition, we limit access to your Personal Information to those
              employees, agents, contractors and other service providers who
              have a business need to know and they are contractually obliged to
              keep this information confidential. We also endeavor to take
              reasonable steps to protect Personal Information from external
              threats, for example hacking or malicious software. Please be
              aware that there are always risks in sending Personal Information
              over public networks and that we cannot guarantee the security of
              data sent to us using these networks.
            </p>
            <p>
              We have procedures to deal with any suspected Personal Information
              breach and will notify you and any applicable regulator when it is
              appropriate for us to do so.
            </p>
            <p>
              If you have a security-related concern, please contact customer
              support at privacy@myEverwill.com.
            </p>
            <h2>8. Our International Operations and Data Transfers</h2>
            <p>
              We currently operate in the United States but we also have offices
              in EU and your Personal Information may be transferred to and from
              the EU for the purposes described in this Privacy Policy.
            </p>
            <h2>9. California Privacy Disclosures</h2>
            <p>
              If you are a California resident, please review the following
              additional privacy disclosures under the California Consumer
              Privacy Act of 2018 (“CCPA”).
            </p>
            <h3>How to Exercise your Legal Rights</h3>
            <p>
              You have the right to understand how we collect, use, and disclose
              your PHI, to access your information, to request that we delete
              certain information, and to not be discriminated against for
              exercising your privacy rights.
            </p>
            <p>
              You may exercise these rights using your account settings and
              tools as described in the Your Rights to Access and Control Your
              PHI section, for example:
            </p>
            <ul class="css-4ihv1n e162lf2u4">
              <li>
                <p>
                  By logging into your account and using your account settings,
                  you may exercise your right to access your PHI and to
                  understand how we collect, use, and disclose it.
                </p>
              </li>
              <li>
                <p>
                  Your account settings also let you exercise your right to
                  delete PHI.
                </p>
              </li>
            </ul>
            <p>
              If you need further assistance regarding your rights, please
              contact our privacy support at privacy@myEverwill.com and we will
              consider your request in accordance with applicable laws.
            </p>
            <p>
              California residents also have the right, at any time, to direct
              us not to sell your Personal Information to third parties. We are
              not in the business of selling consumer information to third
              parties, we only use and share your Personal Information as
              described in this Privacy Policy. You may still send a “Do Not
              Sell My Info” request to us at any time by clicking the email link
              below to make your request, or mail your request to our address
              listed under “CONTACT DETAILS” below.
            </p>
            <h3>Response Timing and Format</h3>
            <p>
              We endeavor to respond to a verifiable consumer request within
              forty-five (45) days of its receipt. If we require more time (up
              to 90 days), we will inform you of the reason and extension period
              in writing.
            </p>
            <p>
              Any disclosures we provide will only cover the 12-month period
              preceding the verifiable consumer request's receipt. The response
              we provide will also explain the reasons we cannot comply with a
              request, if applicable. For data portability requests, we will
              select a format to provide your Personal Information that is
              readily useable and should allow you to transmit the information
              from one entity to another entity without hindrance. We do not
              charge a fee to process or respond to your verifiable consumer
              request unless it is excessive, repetitive, or manifestly
              unfounded. If we determine that the request warrants a fee, we
              will tell you why we made that decision and provide you with a
              cost estimate before completing your request.
            </p>
            <h3>
              Categories of Information we Collect, Use, and Disclose for
              Business Purposes
            </h3>
            <p>
              We collect the categories of Personal Information listed below. We
              receive this information from you, your device, your use of the
              Services, third parties and as otherwise described in this policy.
              We use and disclose these categories of information for the
              business purposes described above. The categories are:
            </p>
            <ul class="css-4ihv1n e162lf2u4">
              <li>
                <p>
                  Identifiers, like your name or username, email address,
                  mailing address, phone number, IP address, account ID, cookie
                  ID, and other similar identifiers.
                </p>
              </li>
              <li>
                <p>
                  Demographic information, such as your gender, age, health
                  information or description, which may be protected by law.
                </p>
              </li>
              <li>
                <p>
                  Commercial information, including your payment information or
                  records of the Services you purchased, obtained, or considered
                  (for example, if you added them to your shopping cart on the
                  Everwill platforms but did not purchase them).
                </p>
              </li>
              <li>
                <p>
                  Internet or other electronic network activity information,
                  such as the usage data we receive when you access or use our
                  Services. This includes information about your interactions
                  with the Services and about the devices and computers you use
                  to access the Services.
                </p>
              </li>
              <li>
                <p>
                  Geolocation data, including GPS signals, device sensors, Wi-Fi
                  access points, and cell tower IDs, if you have granted us
                  access to that information.
                </p>
              </li>
              <li>
                <p>
                  Electronic, visual, or similar information, such as your
                  profile photo or other photos.
                </p>
              </li>
              <li>
                <p>
                  Professional, educational or employment related information,
                  including any information (like your name, email address, or
                  similar information) that your employers, insurance companies,
                  etc. provides to us so that we can invite you to participate
                  in or determine your eligibility for Everwill’s Services that
                  they offer to their customers, employees, clients, and
                  consumers.
                </p>
              </li>
              <li>
                <p>
                  Other information that you provide, including account
                  information such as your biography or country; ‘messages on
                  the Services; and information recorded by your device which
                  may vary depending on the device you use.
                </p>
              </li>
            </ul>
            <p>We never sell the personal information of our users.</p>
            <h2>10. Changes to This Policy</h2>
            <p>
              We may update this Privacy Policy from time to time, without
              notice, by posting a new version online. You should check this
              page occasionally to review any changes. If we make material
              changes affecting you we will notify you by posting the revised
              Privacy Policy on our websites and, if you are a registered user
              of our platform, by providing notice by email. This helps you
              always be aware of what information we collect, how we use it and
              under what circumstances, if any, it is disclosed. Modifications
              will be effective immediately. Your continued use of the Service
              and/or continued provision of Personal Information to us will be
              deemed as an acceptance of such modifications to this Privacy
              Policy.
            </p>
            <h2>11. How to Contact Us</h2>
            <p>
              If you have questions about this policy or need help exercising
              your privacy rights, please contact us at privacy@myEverwill.com
              Everwill Inc., 555 Bryant Street, Palo Alto, CA 94301, USA
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Section
