import Showcase from '../../components/showcase/Showcase'
import TakeActions from '../../components/takeActions/TakeAction'
import ArticleSection from '../../components/articleSection/ArticleSection'
import Memory from '../../components/memory/Memory'
import SecuritySection from '../../components/securitySection/SecuritySection'
import GetStartedSection from '../../components/getStartedSection/GetStartedSection'

import { main } from './ForYou.module.css'

import { forYou } from '../pagesData'

const ForYou = () => {
  const { showcase, articleSection } = forYou

  return (
    <div className={main}>
      <Showcase {...showcase} />
      <TakeActions />
      <ArticleSection data={articleSection} />
      <Memory />
      <SecuritySection />
      <GetStartedSection />
    </div>
  )
}

export default ForYou