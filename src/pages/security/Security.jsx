import {
  main,
  container,
  middleSection,
  content,
  rightSide,
  contactSection,
  middleSectionContent,
  grid,
  item
} from './Security.module.css'

import image from './images-securityInfo.jpg'
import { useHistory } from 'react-router-dom'

const Security = () => {
  const history = useHistory()
  return (
    <div className={main}>
      <div className={container}>
        <div className={content}>
          <img src={image} alt="" />
          <div className={rightSide}>
            <div>
              <h1>Your privacy is our highest priority.</h1>
              <p>
                We realize that you are trusting the Everwill service with some
                of the most important information and documents you own. Your
                information is encrypted and protected with industry-leading
                technology and security.
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className={middleSection}>
        <div className={middleSectionContent}>
          <h1>
            Protected with <br />
            industry-leading <br />
            technology and security.
          </h1>
          <div className={grid}>
            <div className={item}>
              {/* SVG */}
              <div>
                <p>Secure communication</p>
                <p>
                  All communication is secured from your browser to our service
                  with SSL-encryption (2048 bit certificates).
                </p>
              </div>
            </div>
            <div className={item}>
              {/* SVG */}
              <div>
                <p>ENCRYPTED DATA</p>
                <p>
                  All personally identifiable data stored in our databases is
                  encrypted using AES-256 encryption.
                </p>
              </div>
            </div>
            <div className={item}>
              {/* SVG */}
              <div>
                <p>SECURE FILE STORAGE</p>
                <p>
                  Any files uploaded to Everwill servers are encrypted using
                  AES-256 encryption.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={container}>
        <div className={contactSection} onClick={() => history.push('/contact')}>
          <div>
            <p>
              More questions? Let us help.
              <br />
              <strong>
                Contact us
                {/* SVG */}
              </strong>
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Security
