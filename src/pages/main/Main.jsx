import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import ScrollToTop from 'react-router-scroll-top'

import { useContext, useEffect } from 'react'
import AuthContext from '../../context/auth/AuthContext'

import PrivateRoute from '../../utils/PrivateRoute'

import Navbar from '../../components/layout/navbar/Navbar'
import Footer from '../../components/layout/footer/Footer'
import ForYou from '../forYou/ForYou'
import ForSomeoneElse from '../forSomeoneElse/ForSomeoneElse'
import Pricing from '../pricing/Pricing'
import Login from '../login/Login'
import TermsOfServices from '../termsOfServices/TermsOfServices'
import PrivacyPolicy from '../privacyPolicy/PrivacyPolicy'
import Security from '../security/Security'
import Contact from '../contact/Contact'
import ResetPassword from '../resetPassword/ResetPassword'
import RecoverPassword from '../recoverPassword/RecoverPassword'
import Wgs from '../wgs/Wgs'
import EmailDetails from '../signup/steps/EmailDetails/EmailDetails'
import PlanDetails from '../signup/steps/PlanDetails/PlanDetails'
import AccountDetails from '../signup/steps/AccountDetails/AccountDetails'
import Dashboard from '../../app/Dashboard'
import Store from '../../store/Store'
import Checkout from '../signup/steps/Checkout/Checkout'

const Main = () => {
  const { loadUser } = useContext(AuthContext)
  const showFooter = [
    '/',
    '/loved-ones',
    '/terms-of-services',
    '/security',
    '/contact',
    '/reset-password',
    '/wgs',
    '/recover-password'
  ]

  const showNavbar = [
    '/',
    '/loved-ones',
    '/pricing',
    '/login',
    '/signup',
    '/profile',
    '/contact',
    '/terms-of-services',
    '/terms-of-services',
    '/privacy-policy',
    '/security',
    '/reset-password',
    '/recover-password',
    '/wgs',
    '/checkout',
    '/plan'
  ]

  useEffect(() => {
    loadUser()
    // eslint-disable-next-line
  }, [])

  return (
    <Router>
      <ScrollToTop>
        <Route exact path={showNavbar} component={Navbar} />
        <Switch>
          <Route exact path='/' component={ForYou} />
          <Route path='/loved-ones' component={ForSomeoneElse} />
          <Route path='/pricing' component={Pricing} />
          <Route path='/login' component={Login} />
          <Route path='/signup' component={EmailDetails} />
          <Route path='/plan' component={PlanDetails} />
          <Route path='/profile' component={AccountDetails} />
          <Route path='/terms-of-services' component={TermsOfServices} />
          <Route path='/privacy-policy' component={PrivacyPolicy} />
          <Route path='/security' component={Security} />
          <Route path='/contact' component={Contact} />
          <Route path='/reset-password' component={ResetPassword} />
          <Route path='/recover-password' component={RecoverPassword} />
          <Route path='/wgs' component={Wgs} />
          <Route path='/checkout' component={Checkout} />
          <Route path='/store' component={Store} />
          <PrivateRoute path='/dashboard' component={Dashboard} />
        </Switch>
        <Route exact path={showFooter} component={Footer} />
      </ScrollToTop>
    </Router>
  )
}

export default Main