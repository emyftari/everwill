import { useContext } from 'react'
import { useForm, FormProvider } from 'react-hook-form'
import { main, container, content, leftSide } from './Contact.module.css'

import AuthContext from '../../context/auth/AuthContext'

import { Plane } from '../../assets/icons/IconSvg'

import TextInput from '../../app/components/inputs/TextInput'
import TextArea from '../../app/components/inputs/TextArea'
import QuestionCategorySelect from '../../app/components/inputs/QuestionCategorySelect'

const Contact = () => {
  const { contactUs } = useContext(AuthContext)
  const methods = useForm()

  const onSubmit = data => {
    contactUs(data)
    methods.reset()
  }

  return (
    <div className={main}>
      <div className={container}>
        <div className={content}>
          <div className={leftSide}>
            <h2>How can we help you?</h2>
          </div>
          <FormProvider {...methods}>
            <form onSubmit={methods.handleSubmit(onSubmit)}>
              <TextInput fullWidth label="Email" name="email" required />
              <TextInput fullWidth label="Phone number (incl. country code )" name="phone_number" required />
              <TextInput fullWidth label="Full name" name="full_name" required />
              <QuestionCategorySelect />
              <TextArea required />
              <button>
                <span>
                  <Plane />
                </span>
              Send message
            </button>
            </form>
          </FormProvider>
        </div>
      </div>
    </div>
  )
}

export default Contact
