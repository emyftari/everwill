export const forYou = {
  showcase: {
    title: 'Secure your digital assets and memories.',
    paragraph: 'The easy, safe and inexpensive way to safeguard your digital assets and share them with loved ones today or after you pass away.',
    btnText: 'Get started for free',
    linkText: 'I need Everwill to help a loved one'
  },
  articleSection: [
    {
      title: 'SECURE YOUR DIGITAL LEGACY AND ASSETS',
      desc: 'Ensure that your important online accounts like email, photos, documents and financial sites are discoverable.'
    },
    {
      title: 'MEMORIALIZE OR DELETE SOCIAL MEDIA PAGES',
      desc: 'Start a social media will and make a plan for Facebook, Instagram, Twitter, Snapchat, LinkedIn and many others.'
    },
    {
      title: 'BRING YOUR PRICELESS PHOTOS TO LIFE',
      desc: 'Create shareable animations from your photos to capture life moments in new and meaningful ways.'
    },
    {
      title: 'FREE WILL, MEDICAL AND FUNERAL DIRECTIVES',
      desc: 'Make your wishes known in a will, medical or funeral directive, to be shared now or when you pass away.'
    }
  ]
}

export const forSomeoneElse = {
  showcase: {
    title: 'Protect the memory of your loved ones.',
    paragraph: 'Take control of social media sites, subscriptions and digital assets like photos, email, and other accounts to protect the memory of your loved ones that passed away.',
    btnText: 'Get started',
    linkText: null
  },
  articleSection: [
    {
      title: 'SECURE DIGITAL LEGACY AND ASSETS OF YOUR LOVED ONES',
      desc: 'Make sure to ensure your important online accounts like email, photos, documents and financial accounts are found.'
    },
    {
      title: 'MEMORIALIZE OR DELETE SOCIAL MEDIA PAGES',
      desc: 'Start a social media will and make a plan for Facebook, Instagram, Twitter, Snapchat, LinkedIn and many others.'
    },
    {
      title: 'CLOSE DOWN INACTIVE SUBSCRIPTION ACCOUNTS',
      desc: 'Stop charges from subscriptions and shopping accounts like Microsoft, Netflix, Amazon, Spotify and many others.'
    },
  ]
}