import { article } from './Article.module.css'

const Article = ({ icon, title, desc }) => {
  return (
    <article className={article}>
      <div>{icon}</div>
      <p>{title}</p>
      <p>{desc}</p>
    </article>
  )
}

export default Article
