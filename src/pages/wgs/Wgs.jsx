import {
  main,
  container,
  content,
  cards,
  articles,
  line,
  button
} from './Wgs.module.css'

import {
  FacebookSvg,
  NetflixSvg,
  PaypalSvg,
  GoogleSvg,
  LinkedinSvg
} from './SocialSvg'

import { TouchSvg, SquarePlusSvg, NoteTickSvg } from './HowItWorks'

import Article from './article/Article'
import Card from './card/Card'

const Wgs = () => {
  return (
    <div className={main}>
      <div className={container}>
        <div className={content}>
          <h1>Protect the memory of your loved ones.</h1>
          <p>HOW IT WORKS</p>
          <div className={articles}>
            <div className={line}></div>
            <Article
              icon={<TouchSvg />}
              title="01. SELECT SITES & ACTIONS"
              desc="Select the websites you need help with and the action you want taken."
            />
            <Article
              icon={<SquarePlusSvg />}
              title="02. UPLOAD & SIGN DOCUMENTS"
              desc="Provide information about the account, and upload documents."
            />
            <Article
              icon={<NoteTickSvg />}
              title="03. EVERWILL MAKES IT DONE"
              desc="We'll reach out to all the global websites you selected."
            />
          </div>
          <p>TRENDING ON EVERWILL</p>
          <div className={cards}>
            <Card
              logo={<FacebookSvg />}
              title="Memorialize Facebook Page"
              desc="Choose how a Facebook account is preserved."
            />
            <Card
              logo={<PaypalSvg />}
              title="Extract remaining PayPal funds"
              desc="Transfer any remaining funds from PayPal"
            />
            <Card
              logo={<NetflixSvg />}
              title="Stop Netflix Subscription"
              desc="Annoyed by recurring payments of Netflix?"
            />
            <Card
              logo={<GoogleSvg />}
              title="Extract Google Photos"
              desc="Keep memories of your loved one safe."
            />
            <Card
              logo={<LinkedinSvg />}
              title="Linkedin"
              desc="Say goodbye to followers of your loved one."
            />
          </div>
          <div className={button}>
            <button>Start now</button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Wgs
