export const TouchSvg = () => (
  <svg
    width="36"
    height="37"
    viewBox="0 0 36 37"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      clipRule="evenodd"
      d="M9 3a2 2 0 012-2h14a2 2 0 012 2v14a2 2 0 01-2 2v0H11v0a2 2 0 01-2-2V3z"
      stroke="#007DBC"
      strokeWidth="2"
    ></path>
    <path fill="#fff" d="M11 17h14v4H11z"></path>
    <path
      clipRule="evenodd"
      d="M15.75 11.977a2.236 2.236 0 012.238-2.226 2.261 2.261 0 012.262 2.25v9l6.689 2.25c1.482.477 1.82 2.229 1.468 3.75l-1.53 8.25H14.901l-6.024-9c-.681-1.041-.7-3 .8-3s3.073.628 6.073 5.25V11.977v0z"
      stroke="#007DBC"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    ></path>
  </svg>
)

export const SquarePlusSvg = () => (
  <svg
    width="36"
    height="36"
    viewBox="0 0 36 36"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect
      x="1"
      y="1"
      width="34"
      height="34"
      rx="2"
      stroke="#007DBC"
      strokeWidth="2"
    ></rect>
    <path fill="#007DBC" d="M17 8h2v20h-2z"></path>
    <path fill="#007DBC" d="M28 17v2H8v-2z"></path>
  </svg>
)

export const NoteTickSvg = () => (
  <svg
    width="37"
    height="37"
    viewBox="0 0 37 37"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      clipRule="evenodd"
      d="M23.25 23.25v12H.75V.75h34.5v22.5h-12v0z"
      stroke="#007DBC"
      strokeWidth="2"
      strokeLinejoin="round"
    ></path>
    <path
      d="M35.25 23.25l-12 12"
      stroke="#007DBC"
      strokeWidth="2"
      strokeLinejoin="round"
    ></path>
    <path
      d="M26.25 11L13.5 25.25 9 20.75"
      stroke="#007DBC"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    ></path>
  </svg>
)
