import { container, head, svg, content } from './Card.module.css'

const Card = ({ logo, title, desc }) => {
  return (
    <div className={container}>
      <div className={head}>
        <div className={svg}>{logo}</div>
        <div></div>
      </div>
      <div className={content}>
        <strong>{title}</strong>
        <span>{desc}</span>
      </div>
    </div>
  )
}

export default Card
