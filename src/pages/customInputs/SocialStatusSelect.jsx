import { field, halfField } from './TextInput.module.css'
import { ConnectForm } from '../../utils/ConnectForm'

const SocialStatusSelect = ({ half }) => {
  return (
    <fieldset className={half ? halfField : field}>
      <ConnectForm>
        {({ register }) => (
          <select
            name="social_status"
            placeholder="&nbsp;"
            {...register('social_status')}
          >
            <option value="married">Married</option>
            <option value="divorced">Divorced</option>
            <option value="widowed">Widowed</option>
            <option value="other">Other</option>
          </select>
        )}
      </ConnectForm>
      <label>Social status</label>
    </fieldset>
  )
}

export default SocialStatusSelect
