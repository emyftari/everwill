import { ConnectForm } from '../../utils/ConnectForm'
import { field, halfField } from './TextInput.module.css'

const ChildrenSelect = ({ half }) => {
  return (
    <fieldset className={half ? halfField : field}>
      <ConnectForm>
        {({ register }) => (
          <select
            name="children"
            placeholder="&nbsp;"
            {...register('children')}
          >
            <option value='1'>1</option>
            <option value='2'>2</option>
            <option value='3'>3</option>
            <option value="THREE_AND_MORE">3+</option>
          </select>
        )}
      </ConnectForm>
      <label>Children</label>
    </fieldset>
  )
}

export default ChildrenSelect
