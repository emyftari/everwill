import { ConnectForm } from '../../utils/ConnectForm'
import { field, halfField } from './TextInput.module.css'

const DateInput = ({ half, value }) => {
  return (
    <fieldset className={half ? halfField : field}>
      <ConnectForm>
        {({ register }) => (
          <input
            defaultValue={value}
            type="date"
            name="birthday"
            placeholder="&nbsp;"
            {...register('birthday')}
          />
        )}
      </ConnectForm>
      <label>Birthday</label>
    </fieldset>
  )
}

export default DateInput
