import { ConnectForm } from '../../utils/ConnectForm'
import { field, halfField } from './TextInput.module.css'

const GenderSelect = ({ half }) => {
  return (
    <fieldset className={half ? halfField : field}>
      <ConnectForm>
        {({ register }) => (
          <select name="gender" placeholder="&nbsp;" {...register('gender')}>
            <option value="male">Male</option>
            <option value="female">Female</option>
          </select>
        )}
      </ConnectForm>
      <label>Gender</label>
    </fieldset>
  )
}

export default GenderSelect
