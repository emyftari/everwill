import { ConnectForm } from '../../utils/ConnectForm'

import clsx from 'clsx'
import { field, halfField, fieldError, popError } from './TextInput.module.css'

const TextInput = ({ half, name, label, type, required, value }) => {
  return (
    <ConnectForm>
      {({ register, formState: { errors } }) => (
        <fieldset className={clsx(half ? halfField : field, errors[name]?.type === 'required' && fieldError)}>
          <input
            autoComplete="new-password"
            type={type}
            placeholder="&nbsp;"
            value={value}
            {...register(name, { required: true })}
          />
          {errors[name]?.type === required && <span className={popError}>Required field</span>}
          <label>{label}</label>
        </fieldset>
      )}
    </ConnectForm>
  )
}


export default TextInput
