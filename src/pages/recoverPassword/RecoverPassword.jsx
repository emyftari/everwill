import { useContext } from 'react'
import { useLocation } from 'react-router-dom'
import { useForm, FormProvider } from 'react-hook-form'
import queryString from 'query-string'

import AuthContext from '../../context/auth/AuthContext'
import Alert from '../../components/snackbar/Alert'

import TextInput from '../customInputs/TextInput'

import { main, container, section, fieldContainer } from './ResetPassword.module.css'

const RecoverPassword = () => {
  const methods = useForm()
  const { recoverPassword } = useContext(AuthContext)
  const { search } = useLocation()
  const { v_key } = queryString.parse(search)

  const onSubmit = data => recoverPassword(data, v_key)

  return (
    <div className={main}>
      <Alert />
      <div className={container}>
        <FormProvider {...methods}>
          <form onSubmit={methods.handleSubmit(onSubmit)}>
            <div className={section}>
              <h1>Set new password.</h1>
              <p>Enter your new password to proceed.</p>
            </div>
            <div className={fieldContainer}>
              <div>
                <TextInput name="confirm_password" type="password" label='New password' required='required' />
                <TextInput name="new_password" type="password" label='Password confirmation' required='required' />
              </div>
            </div>
            <div>
              <button>Reset password</button>
            </div>
          </form>
        </FormProvider>
      </div>
    </div>
  )
}

export default RecoverPassword
