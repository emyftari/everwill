import { main, container, content, back, head, planContainer, bottom } from './Pricing.module.css'

import PriceTable from '../../components/priceTable/PriceTable'
import leftArrow from '../../assets/icons/leftArrow.svg'
import rightArrow from '../../assets/icons/rightArrow.svg'

import { free, premium } from './data'

const Pricing = () => {
  return (
    <div className={main}>
      <div className={container}>
        <div className={content}>
          <div className={back}>
            <div>
              <img src={leftArrow} alt="" />
            </div>
          </div>
          <div className={head}>
            <h1>Our pricing</h1>
            <p>
              Select a plan that suits you the best. Go with the Free plan if you want to explore Everill or upgrade to Premium to enjoy the full experience.
            </p>
          </div>
          <div className={planContainer}>
            <PriceTable {...free} />
            <PriceTable {...premium} />
          </div>
          <div className={bottom}>
            <button>
              <p>Looking for photo animation packages?</p>
              <p>See pricing for animation packs
                <img src={rightArrow} alt="" />
              </p>
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Pricing