export const free = {
  titleText: 'Free',
  price: '0',
  desc: 'For people trying out Everwill for an unlimited period of time. Upgrade anytime.',
  btnText: 'Get started',
  list: [
    "Save 3 top websites",
    "Create social media will",
    "Store 3 key documents",
    "Write 1 last goodbye",
    "Share with deputies anytime",
    "Create 3 animations",
    "Advertisement free"
  ],

}

export const premium = {
  titleText: 'Premium',
  price: '5.99',
  desc: 'For people securing all their digital assets and memories with a peace of mind.',
  btnText: 'Start now',
  list: [
    "Everything from Free plan +",
    "Save unlimited sites",
    "Store unlimited documents",
    "Unlimited last goodbye emails",
    "Last goodbye videos",
    "Create funeral & medical directives",
    "10 photo animations/month"
  ]
}
