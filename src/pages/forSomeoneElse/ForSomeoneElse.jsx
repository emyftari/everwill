import Showcase from '../../components/showcase/Showcase'
import Memorialize from '../../components/memory/Memorialize'
import ArticleSection from '../../components/articleSection/ArticleSection'
import HelpSection from '../../components/helpSection/HelpSection'
import SecuritySection from '../../components/securitySection/SecuritySection'
import SecureSection from '../../components/securitySection/SecureSection'

import { forSomeoneElse } from '../pagesData'

import { main } from './ForSomeoneElse.module.css'

const ForSomeoneElse = () => {
  const { showcase, articleSection } = forSomeoneElse

  return (
    <div className={main}>
      <Showcase {...showcase} />
      <Memorialize />
      <ArticleSection data={articleSection} />
      <HelpSection />
      <SecuritySection />
      <SecureSection />
    </div>
  )
}

export default ForSomeoneElse