import { useContext } from 'react'
import { useForm, FormProvider } from 'react-hook-form'

import AuthContext from '../../context/auth/AuthContext'

import TextInput from '../customInputs/TextInput'
import Alert from '../../components/snackbar/Alert'

import { main, container, section, fieldContainer } from './ResetPassword.module.css'

const ResetPassword = () => {
  const { resetPassword } = useContext(AuthContext)
  const methods = useForm()

  const onSubmit = data => resetPassword(data)

  return (
    <div className={main}>
      <Alert />
      <div className={container}>
        <FormProvider {...methods}>
          <form onSubmit={methods.handleSubmit(onSubmit)}>
            <div className={section}>
              <h1>Reset your password.</h1>
              <p>Enter your email and we will send you an activation link.</p>
            </div>
            <div className={fieldContainer}>
              <div>
                <TextInput name="email" type="email" label='Email' required='required' />
              </div>
            </div>
            <div>
              <button>Reset password</button>
            </div>
          </form>
        </FormProvider>
      </div>
    </div>
  )
}

export default ResetPassword
