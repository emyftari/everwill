import { useForm } from 'react-hook-form'
import { useContext, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'

import GoogleLogin from 'react-google-login'
import FacebookLogin from 'react-facebook-login'

import Alert from '../../components/snackbar/Alert'

import AuthContext from '../../context/auth/AuthContext'

import {
  main,
  container,
  grid,
  button,
  leftSide,
  rightSide,
  fields,
  social__button
} from './Login.module.css'

import facebook from '../../assets/icons/facebook.svg'
import googleWhite from '../../assets/icons/googleWhite.svg'

const Login = () => {
  const { login, isAuthenticated, facebookLogin, googleLogin } = useContext(AuthContext)
  const { register, handleSubmit } = useForm()
  const history = useHistory()

  useEffect(() => {
    if (isAuthenticated) {
      history.push('/dashboard')
    }
  }, [isAuthenticated, history])

  const onSubmit = ({ email, password }) => login({ email, password })

  return (
    <div className={main}>
      <div className={container}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <h1>Welcome back! Let's get started.</h1>
          <div className={grid}>
            <div className={leftSide}>
              <p>LOGIN WITH AN EXISTING ACCOUNT</p>
              <div>
                <FacebookLogin
                  appId="1117697788639861"
                  fields="name,email,picture"
                  textButton='Continue with Facebook'
                  cssClass={social__button}
                  callback={facebookLogin}
                  icon={<img src={facebook} alt="" />}
                />
                <GoogleLogin
                  clientId="126571944175-ug751vd0rk493gpg1nget2mnroqjjg0m.apps.googleusercontent.com"
                  onSuccess={googleLogin}
                  onFailure={googleLogin}
                  cookiePolicy={'single_host_origin'}
                  render={renderProps => (
                    <button onClick={renderProps.onClick} className={social__button}>
                      <img src={googleWhite} alt="" />
                      Continue with Google
                    </button>
                  )}
                />
              </div>
            </div>
            <div className={rightSide}>
              <p>LOG IN WITH EMAIL & PASSWORD</p>
              <div className={fields}>
                <fieldset>
                  <input
                    type="email"
                    placeholder="&nbsp;"
                    {...register('email', { required: true })}
                  />
                  <label>Email</label>
                </fieldset>
                <fieldset>
                  <input
                    type="password"
                    placeholder="&nbsp;"
                    {...register('password', { required: true })}
                  />
                  <label>Password</label>
                </fieldset>
              </div>
              <div>
                <p>
                  Don't have an account yet? <Link to="/signup">Sign up</Link>
                </p>
                <Link to="/reset-password">Forgot password?</Link>
              </div>
            </div>
          </div>
          <div className={button}>
            <button>Log in with email</button>
          </div>
        </form>
      </div>
      <Alert />
    </div>
  )
}

export default Login
