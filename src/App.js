import Main from './pages/main/Main'
import AuthState from './context/auth/AuthState'

const App = () => {
  return (
    <AuthState>
      <Main />
    </AuthState>
  )
}

export default App
