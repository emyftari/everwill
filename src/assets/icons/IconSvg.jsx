export const FacebookSvg = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M24 12c0-6.627-5.373-12-12-12S0 5.373 0 12c0 5.989 4.388 10.954 10.125 11.855V15.47H7.077V12h3.048V9.356c0-3.008 1.792-4.67 4.533-4.67 1.313 0 2.686.235 2.686.235v2.953h-1.513c-1.49 0-1.956.925-1.956 1.875V12h3.328l-.532 3.47h-2.796v8.385C19.612 22.955 24 17.99 24 12z"
      fill="#1977F3"
    ></path>
    <path
      d="M16.671 15.47l.532-3.47h-3.328V9.75c0-.95.464-1.876 1.956-1.876h1.514V4.921s-1.374-.234-2.687-.234c-2.74 0-4.533 1.66-4.533 4.669V12H7.078v3.47h3.047v8.385a12.257 12.257 0 003.75 0V15.47h2.796z"
      fill="#FEFEFE"
    ></path>
  </svg>
)

export const GoogleSvg = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M20.983 12.4c0-.622-.05-1.248-.158-1.86H12.19v3.526h4.945a4.239 4.239 0 01-1.83 2.783v2.287h2.95c1.733-1.595 2.73-3.95 2.73-6.735z"
      fill="#4285F4"
    ></path>
    <path
      d="M12.188 21.347c2.47 0 4.552-.811 6.07-2.21l-2.95-2.289c-.822.559-1.882.875-3.116.875-2.39 0-4.415-1.611-5.141-3.778H4.006v2.358a9.158 9.158 0 008.182 5.044z"
      fill="#34A853"
    ></path>
    <path
      d="M7.047 13.945a5.485 5.485 0 010-3.506V8.081H4.006a9.164 9.164 0 000 8.222l3.041-2.358z"
      fill="#FBBC04"
    ></path>
    <path
      d="M12.188 6.657a4.976 4.976 0 013.513 1.373l2.614-2.614a8.8 8.8 0 00-6.127-2.382A9.155 9.155 0 004.006 8.08l3.041 2.358c.724-2.17 2.753-3.782 5.141-3.782z"
      fill="#EA4335"
    ></path>
  </svg>
)

export const PlusSvg = () => (
  <svg
    width="26"
    height="26"
    viewBox="0 0 26 26"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M13.931 12.069V.897h-1.862v11.172H.897v1.862h11.172v11.172h1.862V13.931h11.172v-1.862H13.931z"
      fill="currentColor"
    ></path>
  </svg>
)

export const CheckBlueSvg = () => (
  <svg
    width="12"
    height="12"
    viewBox="0 0 12 12"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <circle cx="6" cy="6" r="6" fill="#007DBC"></circle>
    <path
      d="M3 5.75L5.16 8 9 4"
      stroke="#fff"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    ></path>
  </svg>
)

export const LeftArrow = () => (
  <svg
    width="22"
    height="15"
    viewBox="0 0 22 15"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M8.621 1.414L7.207 0 0 7.207l7.207 7.207L8.621 13 3.828 8.207h17.586v-2H3.83L8.62 1.414z"
      fill="#007DBC"
    ></path>
  </svg>
)

export const Plane = () => (
  <svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg"><path clipRule="evenodd" d="M29.5 7.5L6.5 17l9 3.5 14-13v0z" stroke="#fff" strokeLinejoin="round"></path><path clipRule="evenodd" d="M29.5 7.5l-4 18.5-10-5.5 14-13v0z" stroke="#fff" strokeLinejoin="round"></path><path d="M15.5 20.5v8l3.5-6" stroke="#fff" strokeLinejoin="round"></path></svg>
)