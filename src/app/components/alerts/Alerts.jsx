import { useContext } from 'react'
import AlertContext from '../../context/alert/alertContext'

import Snackbar from '@material-ui/core/Snackbar'
import MuiAlert from '@material-ui/lab/Alert'

function AlertMaterial(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />
}

const Alert = () => {
  const { alerts } = useContext(AlertContext)

  return (
    alerts.length > 0 &&
    alerts.map(alert => (
      <Snackbar
        open={true}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      >
        <AlertMaterial severity={alert.type}>
          {alert.msg}
        </AlertMaterial>
      </Snackbar>
    ))
  )
}

export default Alert