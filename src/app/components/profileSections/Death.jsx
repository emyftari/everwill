import { useContext } from 'react'
import UserContext from '../../context/user/userContext'

import SettingContainer from '../templates/SettingContainer'
import Setting from '../setting/Setting'

import { Switch } from '../../assets/Icons'

const Death = () => {
  const { deadman } = useContext(UserContext)


  return (
    <SettingContainer title="DEAD MAN'S SWITCH">
      <Setting
        icon={<Switch />}
        primaryText='Dead man’s switch'
        secondaryText={deadman !== null ? `Enabled, contacting you ${deadman?.times} times ${deadman?.often}.` : 'Disabled'}
        btnText={deadman !== null ? 'Disable' : 'Enable'}
        path='/dashboard/dead-man-switch'
      />
    </SettingContainer>
  )
}

export default Death