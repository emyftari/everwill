import {
  AvatarRound,
  Mail,
  HomeV2,
  IDCard,
  Facebook,
  Google,
  TrashBin
} from '../../assets/Icons'

export const generalData = {
  basics: [
    { icon: null, primaryText: 'Profile image', secondaryText: 'Not uploaded' },
    {
      icon: <AvatarRound />,
      primaryText: 'Name',
      secondaryText: 'Not uploaded'
    },
    {
      icon: <Mail />,
      primaryText: 'Personal e-mail',
      secondaryText: 'Not uploaded'
    },
    { icon: <HomeV2 />, primaryText: 'Address', secondaryText: 'Not uploaded' },
    {
      icon: <IDCard />,
      primaryText: 'About you',
      secondaryText: 'Not uploaded'
    }
  ],
  accounts: [
    {
      icon: <Facebook />,
      primaryText: 'Facebook account',
      secondaryText: 'Not connected'
    },
    {
      icon: <Google />,
      primaryText: 'Google account',
      secondaryText: 'Not connected'
    }
  ],
  delete: [
    {
      icon: <TrashBin />,
      primaryText: 'Delete Everwill account',
      secondaryText:
        'If you delete your account, your data will be gone forever.'
    }
  ]
}
