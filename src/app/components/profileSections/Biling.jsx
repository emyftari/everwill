import { useContext, useEffect } from 'react'

import ModalContext from '../../context/modal/modalContext'
import UserContext from '../../context/user/userContext'

import SettingContainer from '../templates/SettingContainer'
import Setting from '../setting/Setting'

import { Plus } from '../../assets/Icons'

import { button } from './Billing.module.css'

const Billing = () => {
  const { openModal } = useContext(ModalContext)
  const { getCreditCard, creditCard } = useContext(UserContext)

  useEffect(() => {
    getCreditCard()
    // eslint-disable-next-line
  }, [])

  return (
    <SettingContainer title="PAYMENT METHODS">
      {creditCard?.card_number !== undefined ? (
        <Setting
          primaryText={`Card Number: ${creditCard.card_number}`}
          secondaryText={creditCard.card_date}
          btnText="Delete Card"
        />
      ) : (
        <button className={button} onClick={() => openModal('Add credit card')}>
          <span>
            <Plus />
          </span>
          Add new credit card
        </button>
      )}
    </SettingContainer>
  )
}

export default Billing
