import SettingContainer from '../templates/SettingContainer'
import Setting from '../setting/Setting'

import { LogoSimple } from '../../assets/Icons'

import { plan } from './Billing.module.css'

const Plan = () => {
  return (
    <SettingContainer title='current plan'>
      <Setting icon={<LogoSimple />} primaryText='Everwill Free' btnText='Upgrade' />
      <p className={plan}>No commitment. Cancel at any time in your at least one day before each renewal date. Plan automatically renews until cancelled.</p>
    </SettingContainer>
  )
}


export default Plan