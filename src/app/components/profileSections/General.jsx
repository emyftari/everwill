import { useContext } from 'react'

import AuthContext from '../../../context/auth/AuthContext'

import SettingContainer from '../templates/SettingContainer'
import Setting from '../setting/Setting'
import ConnectWithGoogle from '../setting/ConnectWithGoogle'
import ConnectWithFacebook from '../setting/ConnectWithFacebook'

import { AvatarRound, Mail, HomeV2, IDCard, TrashBin } from '../../assets/Icons'

import { image } from './General.module.css'

const General = () => {
  const { user } = useContext(AuthContext)

  const showImage = () => {
    if (user.attachment) {
      return <img src={user.attachment.file} alt="" className={image} />
    }
  }

  return (
    <>
      <SettingContainer title="basics">
        <Setting
          btnText="Edit"
          icon={showImage()}
          primaryText="Profile image"
          secondaryText="Not uploaded"
        />
        <Setting
          btnText="Edit"
          icon={<AvatarRound />}
          primaryText="Name"
          secondaryText={
            user.first_name !== null
              ? `${user.first_name} ${user.last_name}`
              : 'Not set'
          }
        />
        <Setting
          btnText="Edit"
          icon={<Mail />}
          primaryText="Personal e-mail"
          secondaryText={user.email}
        />
        <Setting
          btnText="Edit"
          icon={<HomeV2 />}
          primaryText="Address"
          secondaryText={
            user.city !== null
              ? `${user.street_address}, ${user.city}, ${user.state} ${user.zip_code}, ${user.country}`
              : 'Not set'
          }
        />
        <Setting
          btnText="Edit"
          icon={<IDCard />}
          primaryText="About you"
          secondaryText={
            user.gender !== null
              ? `${user.gender}, ${user.social_status}, Born on ${user.birthday}, ${user.children}`
              : 'Not set'
          }
        />
      </SettingContainer>
      <SettingContainer title='Connected Accounts'>
        <ConnectWithFacebook />
        <ConnectWithGoogle />
      </SettingContainer>
      <SettingContainer title="delete account">
        <Setting
          btnText="Delete"
          icon={<TrashBin />}
          primaryText="Delete Everwill account"
          secondaryText="If you delete your account, your data will be gone forever."
        />
      </SettingContainer>
    </>
  )
}

export default General
