import SettingContainer from '../templates/SettingContainer'
import Setting from '../setting/Setting'

import { Lock, CheckShield, ExlamationPoint } from '../../assets/Icons'

import { twoStepVerification, info } from './General.module.css'

const Plan = () => {
  return (
    <SettingContainer title='password'>
      <Setting
        icon={<Lock />}
        primaryText='Your Password'
        secondaryText='Set a unique password to protect your Everwill account.'
        btnText='Change'
      />
      <div className={twoStepVerification}>
        <p>2-step verification</p>
        <div className={info}>
          <ExlamationPoint />
          <p>
            Secure your account by enabling 2-step verification. Require type-in SMS code when you log in.
          </p>
        </div>
        <Setting
          icon={<CheckShield />}
          primaryText='Two step verification'
          secondaryText='Set a unique password to protect your Everwill account.'
          btnText='Enable'
        />
      </div>
    </SettingContainer >
  )
}

export default Plan