import { useHistory } from 'react-router-dom'

import { Cross } from '../../assets/Icons'

import {
  todo__container,
  todo__content,
  todo__inner,
  todo__header,
  todo__title,
  todo__description,
  todo__footer
} from './Todo.module.css'

const Todo = ({ icon, title, description, button, path }) => {
  const history = useHistory()

  return (
    <div className={todo__container} onClick={() => history.push(path)}>
      <div className={todo__content}>
        <div className={todo__inner}>
          <div className={todo__header}>
            {icon}
            <span>
              <button>
                <Cross />
              </button>
            </span>
          </div>
          <p className={todo__title}>
            <span>{title}</span>
          </p>
          <p className={todo__description}>
            <span>{description}</span>
          </p>
          <div className={todo__footer}>
            <button>
              <span>{button}</span>
            </button>
            <span>
              <span>Takes 1 min.</span>
            </span>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Todo
