import { useState, useContext } from 'react'
import { useHistory } from 'react-router-dom'

import UserContext from '../../context/user/userContext'
import DeviceContext from '../../context/device/deviceContext'
import DeputyContext from '../../context/deputy/deputyContext'
import SiteContext from '../../context/site/siteContext'

import {
  container,
  content,
  button,
  list__container,
  list__header,
  list,
  list__item__completed,
  list__item__not__completed
} from './ProfileChecklist.module.css'

const ProfileChecklist = () => {
  const [openChecklist, setOpenChecklist] = useState(false)

  const toggleModal = () => setOpenChecklist(!openChecklist)

  return (
    <section className={container}>
      <div className={content}>
        {openChecklist && <CheckList />}
        <button className={button} onClick={toggleModal}>
          <span>{openChecklist ? <Close /> : <ThreeDotsVertical />}</span>
        </button>
      </div>
    </section>
  )
}

const CheckList = () => {
  const { devices } = useContext(DeviceContext)
  const { sites } = useContext(SiteContext)
  const { deputies } = useContext(DeputyContext)
  const { deadman } = useContext(UserContext)

  return (
    <div className={list__container}>
      <p className={list__header}>Profile checklist</p>
      <div className={list}>
        <ListItem
          completed={devices.length !== 0 ? true : false}
          title="Add phone code"
          path='/dashboard/add-new-device'
        />
        <ListItem
          completed={deputies.length !== 0 ? true : false}
          title="Add first deputy"
          path='/dashboard/deputy'
        />
        <ListItem
          completed={sites.length !== 0 ? true : false}
          title="Add your first site"
          path='/dashboard/add-new-site'
        />
        <ListItem
          completed={deadman !== null ? true : false}
          title="Set dead man's switch"
          path='/dashboard/dead-man-switch'
        />
        <ListItem
          completed={false}
          title="Upgrade to premium"
          path='/plan'
        />
      </div>
      <div>
        {/* <Link to="/">Don't show again</Link> */}
      </div>
    </div>
  )
}

const ListItem = ({ completed, title, path }) => {
  const history = useHistory()

  return (
    <div
      className={completed ? list__item__completed : list__item__not__completed}
      onClick={() => history.push(path)}
    >
      <div>{completed ? <CheckedCircle /> : <EmptyCircle />}</div>
      <p>{title}</p>
      {!completed && <ChevronRight />}
    </div>
  )
}

const ThreeDotsVertical = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M10.836 5.914a2 2 0 112.828-2.828 2 2 0 01-2.828 2.828zM10.836 13.664a2 2 0 112.828-2.828 2 2 0 01-2.828 2.828zM10.836 21.414a2 2 0 112.828-2.828 2 2 0 01-2.828 2.828z"
      fill="#fff"
      stroke="#fff"
    ></path>
  </svg>
)

const Close = () => (
  <svg
    width="17"
    height="17"
    viewBox="0 0 17 17"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M1 1l7.5 7.5M16 16L8.5 8.5m0 0L16 1M8.5 8.5L1 16"
      stroke="#fff"
      strokeWidth="2"
    ></path>
  </svg>
)

const CheckedCircle = () => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <circle
      cx="10"
      cy="10"
      r="9"
      stroke="currentColor"
      strokeWidth="2"
    ></circle>
    <path
      d="M7 9.75L9.16 12 13 8"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    ></path>
  </svg>
)

const EmptyCircle = () => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <circle
      cx="10"
      cy="10"
      r="9"
      stroke="currentColor"
      strokeWidth="2"
    ></circle>
  </svg>
)

const ChevronRight = () => (
  <svg
    width="36"
    height="36"
    viewBox="0 0 36 36"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M14 26l8-8-8-8" stroke="currentColor" strokeWidth="2"></path>
  </svg>
)

export default ProfileChecklist
