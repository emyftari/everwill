import { useHistory } from 'react-router-dom'

import { Clock } from '../../assets/Icons'

import {
  section__title,
  card__container,
  card,
  card__content,
  card__icon,
  card__info,
  card__status
} from './DeputyCard.module.css'

const DeputyCard = ({ deputies }) => {
  const history = useHistory()

  return (
    <section>
      <p className={section__title}>Your deputies</p>
      <div className={card__container}>
        {
          deputies.map(deputy => (
            <div className={card} onClick={() => history.push(`/dashboard/deputy${deputy.id}`)} key={deputy.id}>
              <div className={card__content}>
                <div className={card__icon}>
                  <div>{deputy.name.charAt(0)} {deputy.surname.charAt(0)}</div>
                </div>
                <div className={card__info}>
                  <p>{deputy.name} {deputy.surname}</p>
                  <p>{deputy.email}</p>
                </div>
                <div className={card__status}>
                  <p>
                    {deputy.status}
                    <Clock />
                  </p>
                </div>
              </div>
            </div>
          ))
        }

        {/* <div className={card}>
          <div className={card__content}>
            <div className={card__icon}>
              <div>[n][s]</div>
            </div>
            <div className={card__info}>
              <p>[full name]</p>
              <p>[email]</p>
            </div>
            <div className={card__status}>
              <p>1 shared item</p>
              <p>Friend</p>
            </div>
          </div>
        </div> */}
      </div>
    </section>
  )
}

export default DeputyCard
