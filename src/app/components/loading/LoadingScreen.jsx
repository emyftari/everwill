import { box, cube } from './LoadingScreen.module.css'

const LoadingScreen = () => {
  return (
    <div className={box}>
      <div className={cube}></div>
      <div className={cube}></div>
      <div className={cube}></div>
      <div className={cube}></div>
    </div>
  )
}

export default LoadingScreen