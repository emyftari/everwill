import { search__bar } from '../modal/ActionModal.module.css'

import { Search } from '../../assets/Icons'

const SearchBar = ({ keyword, setKeyword }) => {
  return (
    <label className={search__bar}>
      <button>
        <Search />
      </button>
      <div>
        <input
          type="text"
          name="search"
          placeholder="Search"
          value={keyword}
          onChange={e => setKeyword(e.target.value)}
        />
      </div>
    </label>
  )
}

export default SearchBar