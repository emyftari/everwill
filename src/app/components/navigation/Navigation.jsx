import { NavLink } from 'react-router-dom'
import { linkDataWithIcons, linkDataWithoutIcons } from './linkData'
import {
  nav__container,
  nav__link,
  nav__separator,
  selected
} from './Navigation.module.css'

const Navigation = () => {
  return (
    <nav className={nav__container}>
      {linkDataWithIcons.map((data) => (
        <NavLink
          exact
          key={data.name}
          to={data.pathname}
          className={nav__link}
          activeClassName={selected}
        >
          {data.icon}
          {data.name}
        </NavLink>
      ))}
      <hr className={nav__separator} />
      {linkDataWithoutIcons.map((data) => (
        <NavLink
          exact
          key={data.name}
          to={data.pathname}
          className={nav__link}
          activeClassName={selected}
        >
          {data.name}
        </NavLink>
      ))}
    </nav>
  )
}

export default Navigation
