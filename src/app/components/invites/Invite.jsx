import { useContext, useEffect } from 'react'
import { Link } from 'react-router-dom'

import DeputyContext from '../../context/deputy/deputyContext'

import PageContainer from '../templates/PageContainer'

import { head, invites__container, invite__card, card__body, card__head } from './Invite.module.css'

const Invites = () => {
  const { invitationsRecieved, invitations } = useContext(DeputyContext)

  useEffect(() => {
    invitationsRecieved()
    // eslint-disable-next-line
  }, [])

  return (
    <PageContainer>
      <section>
        <div className={head}>
          <p>Shared with me</p>
          <Link to="/dashboard/shared">See all</Link>
        </div>
        <div className={invites__container}>
          {invitations?.map(invite => <Invite key={invite.id} {...invite} />)}
        </div>
      </section>
    </PageContainer>
  )
}

const Invite = ({ email, id }) => {
  const { acceptInvite } = useContext(DeputyContext)

  return (
    <button className={invite__card} onClick={() => acceptInvite(id)}>
      <span className={card__head}>
        <span>
          <svg width="16" height="25" xmlns="http://www.w3.org/2000/svg"><g fill="none" strokeLinejoin="round" strokeLinecap="round" stroke="currentColor"><path d="M6.47 8.412c0-.82.671-1.484 1.491-1.484.828 0 1.508.672 1.508 1.5v6l4.46 1.5c.987.318 1.212 1.486.978 2.5l-1.02 5.5H5.903l-4.016-6c-.454-.694-.467-2 .533-2s2.05.419 4.05 3.5V8.412h0z"></path><path d="M2.938 12.878a7.001 7.001 0 019.898-9.9 7 7 0 010 9.9"></path></g></svg>
        </span>
      </span>
      <span className={card__body}>
        <span>{email} has made you his deputy with rights to trigger events after his death</span>
        <span>Jun-13 2021</span>
      </span>
    </button>
  )
}


export default Invites