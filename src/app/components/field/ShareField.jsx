import { useContext } from 'react'
import ModalContext from '../../context/modal/modalContext'
// import { ConnectForm } from '../../context/form/formContext'
// import Select from 'react-select'

import { ChevronRight } from '../../assets/Icons'
import {
  field__container,
  label__v2__one,
  label__v2__two,
  field__error
} from './Field.module.css'

const ShareField = ({ error }) => {
  const { openModal } = useContext(ModalContext)
  // const [shared, setShared] = useState([])

  // console.log(shared)


  return (
    <div className={error ? field__error : field__container} onClick={() => openModal('Share with')}>
      <div>
        <p className={label__v2__one}>Shared with</p>
        <p className={label__v2__two}>Nobody yet</p>
      </div>
      <ChevronRight />
      {error}
    </div>
    // <ConnectForm>
    //   {
    //     ({ register }) => (
    //       <Select
    //         isMulti
    //         options={options}
    //         className="basic-multi-select"
    //         classNamePrefix="select"
    //         onChange={setShared}
    //       />
    //     )
    //   }

    // </ConnectForm>
  )
}

export default ShareField
