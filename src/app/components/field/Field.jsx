import { ChevronRight } from '../../assets/Icons'
import {
  field__container,
  label,
  label__v2__one,
  label__v2__two,
  field__error
} from './Field.module.css'

const Field = ({ strong, labelOne, labelTwo, v2, toggleModal, error }) => {
  return (
    <div className={error ? field__error : field__container} onClick={toggleModal}>
      <div>
        {v2 ? (
          <>
            <p className={label__v2__one}>{labelOne}</p>
            <p className={label__v2__two}>{labelTwo}</p>
          </>
        ) : (
          <p className={label}>
            <strong>{strong}</strong>
          </p>
        )}
      </div>
      <ChevronRight />
      {error}
    </div>
  )
}

export default Field
