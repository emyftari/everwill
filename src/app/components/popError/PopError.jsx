import { pop__error } from './PopError.module.css'

const PopError = () => <span className={pop__error}>Required field</span>

export default PopError
