import { useEffect, useContext } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { useForm, FormProvider } from 'react-hook-form'

import DeputyContext from '../../context/deputy/deputyContext'
import TextInput from '../../components/inputs/TextInput'
import RelationshipSelect from '../../components/inputs/RelationshipSelect'

import {
  container,
  content,
  inner__container,
  header,
  form,
} from './DeputyProfile.module.css'

import { left__side, right__side, form__container, p, button__container } from './EditDeputy.module.css'

const EditDeputy = () => {
  const methods = useForm()
  const history = useHistory()
  const { deputyId } = useParams()
  const { getDeputy, deputy, loading, deleteDeputy, updateDeputy } = useContext(DeputyContext)

  useEffect(() => {
    getDeputy(deputyId)
    // eslint-disable-next-line
  }, [])

  if (loading) {
    return 'loading'
  }

  const onSubmit = (data) => {
    updateDeputy({ id: deputyId, relationship: data.relationship })
    history.push(`/dashboard/deputy${deputyId}`)
  }

  const handleClick = () => {
    deleteDeputy(deputyId)
    history.push('/dashboard/deputy')
  }

  return (
    <div className={container}>
      <div className={content}>
        <div className={inner__container}>
          <main>
            <h1 className={header}>Edit {deputy.deputy_name}'s profile</h1>
            <p className={p}>Edit Information</p>
            <FormProvider {...methods}>
              <form className={form} onSubmit={methods.handleSubmit(onSubmit)}>
                <div className={form__container}>
                  <div className={left__side}>
                    <div>
                      <TextInput label="Name" name='first_name' disabled value={deputy.deputy_name} />
                      <TextInput label="Surname" name='last_name' disabled value={deputy.deputy_surname} />
                    </div>
                  </div>
                  <div className={right__side}>
                    <div>
                      <RelationshipSelect />
                    </div>
                  </div>
                </div>
                <div className={button__container}>
                  <button type="button" onClick={handleClick}>Delete the deputy</button>
                  <button type='submit'>Save</button>
                </div>
              </form>
            </FormProvider>
          </main>
        </div>
      </div>
    </div>
  )
}

export default EditDeputy
