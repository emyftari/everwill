import { useEffect, useContext, useState } from 'react'
import { Link, useParams, useHistory } from 'react-router-dom'
import { BackArrow, Clock, ArrowDown, Check } from '../../assets/Icons'
import { v4 as uuidv4 } from 'uuid'

import DeputyContext from '../../context/deputy/deputyContext'

import {
  container,
  content,
  inner__container,
  arrow__container,
  header,
  header__info,
  header__logo,
  header__action,
  checkbox__container,
  svg__container,
  checkbox,
  form,
  form__container,
  section__header,
  section__info,
  primary,
  section__buttons,
  radio__button,
  selected,
  divider
} from './DeputyProfile.module.css'

const DeputyProfile = () => {
  const history = useHistory()
  const { deputyId } = useParams()
  const { getDeputy, loading, deputy } = useContext(DeputyContext)

  useEffect(() => {
    getDeputy(deputyId)
    // eslint-disable-next-line
  }, [])

  if (loading) {
    return ''
  }

  return (
    <>
      <div className={container}>
        <div className={content}>
          <div className={inner__container}>
            <div className={arrow__container}>
              <div>
                <BackArrow />
              </div>
              <p
                onClick={() =>
                  history.push(`/dashboard/edit-deputy${deputyId}`)
                }
              >
                Edit {deputy.deputy_name}'s profile
              </p>
            </div>
            <main>
              <h1 className={header}>
                <div className={header__info}>
                  <div className={header__logo}>
                    <div>
                      {deputy.deputy_name.charAt(0)}{' '}
                      {deputy.deputy_surname.charAt(0)}
                    </div>
                  </div>
                  {deputy.deputy_name} {deputy.deputy_surname}
                </div>
                <div className={header__action}>
                  <p>Current Status</p>
                  <div>
                    <button type="button">
                      <span>
                        <Clock />
                      </span>
                      <label>
                        Pending invite
                        <ArrowDown />
                      </label>
                    </button>
                  </div>
                  <label className={checkbox__container}>
                    <input
                      name="trusted"
                      type="checkbox"
                      className={checkbox}
                    />
                    <div className={svg__container}>
                      <Check />
                    </div>
                    <p>Can trigger events following my death.</p>
                  </label>
                </div>
              </h1>
              <form className={form}>
                <div className={form__container}>
                  {deputy.sharing_sites.length !== 0 && (
                    <section>
                      <div className={section__header}>
                        <p>My sites</p>
                        <p>Select sharing options</p>
                      </div>
                      {deputy.sharing_sites.map((site) => (
                        <SharingSites key={site.site_id} site={site} radioId={uuidv4()} />
                      ))}
                    </section>
                  )}
                  {deputy.sharing_documents.length !== 0 && (
                    <section>
                      <div className={section__header}>
                        <p>My document</p>
                        <p>Select sharing options</p>
                      </div>
                      {deputy.sharing_documents.map((document) => (
                        <SharingDocuments
                          key={document.document_id}
                          document={document}
                          radioId={uuidv4()}
                        />
                      ))}
                    </section>
                  )}
                  {deputy.sharing_devices.length !== 0 && (
                    <section>
                      <div className={section__header}>
                        <p>My devices</p>
                        <p>Select sharing options</p>
                      </div>
                      {deputy.sharing_devices.map((device) => (
                        <SharingDevices
                          key={device.device_id}
                          device={device}
                          radioId={uuidv4()}
                        />
                      ))}
                    </section>
                  )}
                </div>
              </form>
            </main>
          </div>
        </div>
      </div>
      <div className={divider}>
        <span>
          <p>Maybe you want to also share</p>
        </span>
        <hr />
      </div>
      <div className={container}>
        <div>
          <main>
            <div className={form__container}>
              {deputy.to_share_sites.length !== 0 && (
                <section>
                  <div className={section__header}>
                    <p>My sites</p>
                    <p>Select sharing options</p>
                  </div>
                  {deputy.to_share_sites.map((site) => (
                    <ToShareSites key={site.site_id} site={site} />
                  ))}
                </section>
              )}
              {deputy.to_share_documents.length !== 0 && (
                <section>
                  <div className={section__header}>
                    <p>My documents</p>
                    <p>Select sharing options</p>
                  </div>
                  {deputy.to_share_documents.map((document) => (
                    <ToShareDocuments
                      key={document.document_id}
                      document={document}
                    />
                  ))}
                </section>
              )}
              {deputy.to_share_devices.length !== 0 && (
                <section>
                  <div className={section__header}>
                    <p>My devices</p>
                    <p>Select sharing options</p>
                  </div>
                  {deputy.to_share_devices.map((device) => (
                    <ToShareDevices key={device.device_id} device={device} />
                  ))}
                </section>
              )}
            </div>
          </main>
        </div>
      </div>
    </>
  )
}

const SharingSites = ({ site, radioId }) => {
  const { deputyId } = useParams()
  const { updateSharingSite } = useContext(DeputyContext)
  const [siteId, setSiteId] = useState()

  const handleClick = (id) => {
    setSiteId(id)
  }

  const handleChange = (e) => {
    const data = {
      siteId,
      time: e.target.value,
      deputyId
    }

    updateSharingSite(data)
  }

  return (
    <div className={section__info} key={site.site_id}>
      <div className={primary}>
        <Link to="">{site.name}</Link>
        <p>{site.happen_next}</p>
      </div>
      <div
        className={section__buttons}
        onClick={() => handleClick(site.site_id)}
      >
        <label className={radio__button}>
          <input
            type="radio"
            value="Now"
            name={radioId}
            onChange={handleChange}
            checked={site.time === 'Now'}
          />
          <div className={selected}>Now</div>
        </label>
        <label className={radio__button}>
          <input
            type="radio"
            value="After Death"
            name={site.site_id}
            onChange={handleChange}
            checked={site.time === 'After Death'}
          />
          <div className={selected}>After Death</div>
        </label>
        <label className={radio__button}>
          <input
            type="radio"
            value="Never"
            name={site.site_id}
            onChange={handleChange}
            checked={site.time === 'Never'}
          />
          <div className={selected}>Never</div>
        </label>
      </div>
    </div>
  )
}

const ToShareSites = ({ site }) => {
  const { deputyId } = useParams()
  const { getDeputy, updateToShareSite } = useContext(DeputyContext)
  const [siteId, setSiteId] = useState()

  const handleChange = async (e) => {
    const data = {
      siteId,
      time: e.target.value,
      deputyId
    }

    await updateToShareSite(data)
    getDeputy(deputyId)
  }

  const handleClick = (id) => setSiteId(id)

  return (
    <div className={section__info}>
      <div className={primary}>
        <Link to="">{site.name}</Link>
        <p>{site.happen_next}</p>
      </div>
      <div
        className={section__buttons}
        onClick={() => handleClick(site.site_id)}
      >
        <label className={radio__button}>
          <input type="radio" value="Now" name="site" onChange={handleChange} />
          <div className={selected}>Now</div>
        </label>
        <label className={radio__button}>
          <input
            type="radio"
            value="After Death"
            name="site"
            onChange={handleChange}
          />
          <div className={selected}>After Death</div>
        </label>
        <label className={radio__button}>
          <input
            type="radio"
            value="Never"
            name="site"
            onChange={handleChange}
          />
          <div className={selected}>Never</div>
        </label>
      </div>
    </div>
  )
}

const SharingDocuments = ({ document, radioId }) => {
  const { deputyId } = useParams()
  const { getDeputy, updateSharingDocuments } = useContext(DeputyContext)
  const [documentId, setDocumentId] = useState()

  const handleClick = (id) => setDocumentId(id)

  const handleChange = async (e) => {
    const data = {
      documentId,
      time: e.target.value,
      deputyId
    }

    await updateSharingDocuments(data)
    getDeputy(deputyId)
  }

  return (
    <div className={section__info} key={document.document_id}>
      <div className={primary}>
        <Link to="">{document.title}</Link>
        <p>{ }</p>
      </div>
      <div
        className={section__buttons}
        onClick={() => handleClick(document.document_id)}
      >
        <label className={radio__button}>
          <input
            type="radio"
            value="Now"
            name={radioId}
            onChange={handleChange}
            checked={document.time === 'Now'}
          />
          <div className={selected}>Now</div>
        </label>
        <label className={radio__button}>
          <input
            type="radio"
            value="After Death"
            name={radioId}
            onChange={handleChange}
            checked={document.time === 'After Death'}
          />
          <div className={selected}>After Death</div>
        </label>
        <label className={radio__button}>
          <input
            type="radio"
            value="Never"
            name={radioId}
            onChange={handleChange}
            checked={document.time === 'Never'}
          />
          <div className={selected}>Never</div>
        </label>
      </div>
    </div>
  )
}

const ToShareDocuments = ({ document }) => {
  const { deputyId } = useParams()
  const { getDeputy, updateToShareDocument } = useContext(DeputyContext)
  const [documentId, setDocumentId] = useState()

  const handleClick = (id) => setDocumentId(id)

  const handleChange = async (e) => {
    const data = {
      documentId,
      time: e.target.value,
      deputyId
    }

    await updateToShareDocument(data)
    getDeputy(deputyId)
  }

  return (
    <div className={section__info}>
      <div className={primary}>
        <Link to="">{document.id}</Link>
        <p>{document.happen_next}</p>
      </div>
      <div
        className={section__buttons}
        onClick={() => handleClick(document.document_id)}
      >
        <label className={radio__button}>
          <input
            type="radio"
            value="Now"
            name={document.document_id}
            onChange={handleChange}
          />
          <div className={selected}>Now</div>
        </label>
        <label className={radio__button}>
          <input
            type="radio"
            value="After Death"
            name={document.document_id}
            onChange={handleChange}
          />
          <div className={selected}>After Death</div>
        </label>
        <label className={radio__button}>
          <input
            type="radio"
            value="Never"
            name={document.document_id}
            onChange={handleChange}
          />
          <div className={selected}>Never</div>
        </label>
      </div>
    </div>
  )
}

const SharingDevices = ({ device, radioId }) => {
  const { deputyId } = useParams()
  const { getDeputy, updateSharingDevice } = useContext(DeputyContext)
  const [deviceId, setDeviceID] = useState()

  const handleClick = (id) => setDeviceID(id)

  const handleChange = async (e) => {
    const data = {
      deviceId,
      time: e.target.value,
      deputyId
    }

    await updateSharingDevice(data)
    getDeputy(deputyId)
  }

  return (
    <div className={section__info} key={device.device_id}>
      <div className={primary}>
        <Link to="">{device.name}</Link>
        <p>{ }</p>
      </div>
      <div
        className={section__buttons}
        onClick={() => handleClick(device.device_id)}
      >
        <label className={radio__button}>
          <input
            type="radio"
            value="Now"
            name={radioId}
            onChange={handleChange}
            checked={device.time === 'Now'}
          />
          <div className={selected}>Now</div>
        </label>
        <label className={radio__button}>
          <input
            type="radio"
            value="After Death"
            name={radioId}
            onChange={handleChange}
            checked={device.time === 'After Death'}
          />
          <div className={selected}>After Death</div>
        </label>
        <label className={radio__button}>
          <input
            type="radio"
            value="Never"
            name={radioId}
            onChange={handleChange}
            checked={device.time === 'Never'}
          />
          <div className={selected}>Never</div>
        </label>
      </div>
    </div>
  )
}

const ToShareDevices = ({ device }) => {
  const { deputyId } = useParams()
  const { getDeputy, updateToShareDevice } = useContext(DeputyContext)
  const [deviceId, setDeviceId] = useState()

  const handleClick = (id) => setDeviceId(id)

  const handleChange = async (e) => {
    const data = {
      deviceId,
      time: e.target.value,
      deputyId
    }

    await updateToShareDevice(data)
    getDeputy(deputyId)
  }

  return (
    <div className={section__info}>
      <div className={primary}>
        <Link to="">{device.name}</Link>
        <p>{ }</p>
      </div>
      <div
        className={section__buttons}
        onClick={() => handleClick(device.device_id)}
      >
        <label className={radio__button}>
          <input
            type="radio"
            value="Now"
            name={device.device_id}
            onChange={handleChange}
          />
          <div className={selected}>Now</div>
        </label>
        <label className={radio__button}>
          <input
            type="radio"
            value="After Death"
            name={device.device_id}
            onChange={handleChange}
          />
          <div className={selected}>After Death</div>
        </label>
        <label className={radio__button}>
          <input
            type="radio"
            value="Never"
            name={device.device_id}
            onChange={handleChange}
          />
          <div className={selected}>Never</div>
        </label>
      </div>
    </div>
  )
}

export default DeputyProfile
