import { useContext, useState } from 'react'
import ModalContext from '../../context/modal/modalContext'
import FieldContext from '../../context/field/fieldContext'

import ModalTemplate from '../templates/ModalTemplate'
import SiteButton from '../buttons/SiteButton'
import SearchBar from '../searchBar/SearchBar'

import { main__container, main__head } from './ActionModal.module.css'
import { site__container, site__content, add__new__site } from './SiteModal.module.css'

import { Plus } from '../../assets/Icons'

import {
  top,
  social,
  shopping,
  payments,
  software,
  telecom,
  dating,
  media,
  crypto,
  music,
  tv,
  insurance
} from '../../assets/images/siteImages'

const SiteModal = () => {
  const { openSiteModal, closeModal } = useContext(ModalContext)
  const { setSiteLabel, toggleNewSite } = useContext(FieldContext)

  const handleClick = () => {
    console.log('object')
    setSiteLabel('New Site', 'Site')
    toggleNewSite()
    closeModal()
  }

  const [keyword, setKeyword] = useState('')
  const [topSites, setTopSites] = useState(top)
  const [socialSites, setSocialSites] = useState(social)
  const [shoppingSites, setShoppingSites] = useState(shopping)
  const [paymentsSites, setPaymentsSites] = useState(payments)
  const [softwareSites, setSoftwareSites] = useState(software)
  const [telecomSites, setTelecomSites] = useState(telecom)
  const [datingSites, setDatingSites] = useState(dating)
  const [mediaSites, setMediaSites] = useState(media)
  const [cryptoSites, setCryptoSites] = useState(crypto)
  const [musicSites, setMusicSites] = useState(music)
  const [tvSites, setTvSites] = useState(tv)
  const [insuranceSites, setInsuranceSites] = useState(insurance)

  const updateInput = async (keyword) => {
    const filteredTop = top.filter(site => site.site.toLowerCase().includes(keyword.toLowerCase()))
    const filteredSocial = social.filter(site => site.site.toLowerCase().includes(keyword.toLowerCase()))
    const filteredShopping = shopping.filter(site => site.site.toLowerCase().includes(keyword.toLowerCase()))
    const filteredPayments = payments.filter(site => site.site.toLowerCase().includes(keyword.toLowerCase()))
    const filteredSoftware = software.filter(site => site.site.toLowerCase().includes(keyword.toLowerCase()))
    const filteredTelecom = telecom.filter(site => site.site.toLowerCase().includes(keyword.toLowerCase()))
    const filteredDating = dating.filter(site => site.site.toLowerCase().includes(keyword.toLowerCase()))
    const filteredMedia = media.filter(site => site.site.toLowerCase().includes(keyword.toLowerCase()))
    const filteredCrypto = crypto.filter(site => site.site.toLowerCase().includes(keyword.toLowerCase()))
    const filteredMusic = music.filter(site => site.site.toLowerCase().includes(keyword.toLowerCase()))
    const filteredTv = tv.filter(site => site.site.toLowerCase().includes(keyword.toLowerCase()))
    const filteredInsurance = insurance.filter(site => site.site.toLowerCase().includes(keyword.toLowerCase()))
    setKeyword(keyword)
    setTopSites(filteredTop)
    setSocialSites(filteredSocial)
    setShoppingSites(filteredShopping)
    setPaymentsSites(filteredPayments)
    setSoftwareSites(filteredSoftware)
    setTelecomSites(filteredTelecom)
    setDatingSites(filteredDating)
    setMediaSites(filteredMedia)
    setCryptoSites(filteredCrypto)
    setMusicSites(filteredMusic)
    setTvSites(filteredTv)
    setInsuranceSites(filteredInsurance)
  }

  return (
    <ModalTemplate open={openSiteModal}>
      <main className={main__container}>
        <div className={main__head}>
          <h1>Choose a site.</h1>
          <SearchBar keyword={keyword} setKeyword={updateInput} />
        </div>
        <div className={site__container}>
          {topSites.length > 0 && (
            <div className={site__content}>
              <p>Top</p>
              <div>
                {topSites.map(({ id, site, img }) => <SiteButton key={id} name={site} cat='Top' img={img} />)}
              </div>
            </div>
          )}
          {socialSites.length > 0 && (
            <div className={site__content}>
              <p>Social</p>
              <div>
                {socialSites.map(({ id, site, img }) => <SiteButton key={id} name={site} cat='Social' img={img} />)}
              </div>
            </div>
          )}
          {shoppingSites.length > 0 && (
            <div className={site__content}>
              <p>Shopping</p>
              <div>
                {shoppingSites.map(({ id, site, img }) => <SiteButton key={id} name={site} cat='Shopping' img={img} />)}
              </div>
            </div>
          )}
          {paymentsSites.length > 0 && (
            <div className={site__content}>
              <p>PAYMENT, BANKS AND TRADING</p>
              <div>
                {paymentsSites.map(({ id, site, img }) => <SiteButton key={id} name={site} cat='Payments, Banks And Trading' img={img} />)}
              </div>
            </div>
          )}
          {softwareSites.length > 0 && (
            <div className={site__content}>
              <p>SOFTWARE AND PUBLISHING</p>
              <div>
                {softwareSites.map(({ id, site, img }) => <SiteButton key={id} name={site} cat='Software' img={img} />)}
              </div>
            </div>
          )}
          {telecomSites.length > 0 && (
            <div className={site__content}>
              <p>TELECOM</p>
              <div>
                {telecomSites.map(({ id, site, img }) => <SiteButton key={id} name={site} cat='Telecom' img={img} />)}
              </div>
            </div>
          )}
          {datingSites.length > 0 && (
            <div className={site__content}>
              <p>DATING</p>
              <div>
                {datingSites.map(({ id, site, img }) => <SiteButton key={id} name={site} cat='Dating' img={img} />)}
              </div>
            </div>
          )}
          {mediaSites.length > 0 && (
            <div className={site__content}>
              <p>PHOTO & VIDEO</p>
              <div>
                {media.map(({ id, site, img }) => <SiteButton key={id} name={site} cat='Photo & Video' img={img} />)}
              </div>
            </div>
          )}
          {cryptoSites.length > 0 && (
            <div className={site__content}>
              <p>CRYPTO</p>
              <div>
                {cryptoSites.map(({ id, site, img }) => <SiteButton key={id} name={site} cat='Crypto' img={img} />)}
              </div>
            </div>
          )}
          {musicSites.length > 0 && (
            <div className={site__content}>
              <p>MUSIC, NEWS AND GAMING</p>
              <div>
                {musicSites.map(({ id, site, img }) => <SiteButton key={id} name={site} cat='Music' img={img} />)}
              </div>
            </div>
          )}
          {tvSites.length > 0 && (
            <div className={site__content}>
              <p>TV</p>
              <div>
                {tvSites.map(({ id, site, img }) => <SiteButton key={id} name={site} cat='TV' img={img} />)}
              </div>
            </div>
          )}
          {insuranceSites.length > 0 && (
            <div className={site__content}>
              <p>INSURANCE</p>
              <div>
                {insuranceSites.map(({ id, site, img }) => <SiteButton key={id} name={site} cat='Insurance' img={img} />)}
              </div>
            </div>
          )}
          <button className={add__new__site} onClick={handleClick}>
            <span>
              <Plus />
            </span>
            Add new site
          </button>
        </div>
      </main>
    </ModalTemplate>
  )
}

export default SiteModal