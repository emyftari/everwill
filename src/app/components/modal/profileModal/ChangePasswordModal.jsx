import { useContext } from 'react'
import { FormProvider, useForm } from 'react-hook-form'

import ModalContext from '../../../context/modal/modalContext'
import UserContext from '../../../context/user/userContext'
import AuthContext from '../../../../context/auth/AuthContext'

import ModalTemplate from './ModalTemplate'
import TextInput from '../../inputs/TextInput'

import { Mail } from '../../../assets/Icons'

import {
  content,
  header,
  form,
  form__buttons,
  update__btn
} from './ModalTemplate.module.css'

const ChangePassword = () => {
  const methods = useForm()
  const { changePassword } = useContext(UserContext)
  const { openChangePassword } = useContext(ModalContext)
  const { logout } = useContext(AuthContext)

  const onSubmit = async data => {
    await changePassword(data)
    logout()
  }

  return (
    <ModalTemplate open={openChangePassword}>
      <div className={content}>
        <div className={header}>
          <Mail />
          <p>Change password</p>
        </div>
        <FormProvider {...methods}>
          <form className={form} onSubmit={methods.handleSubmit(onSubmit)}>
            <TextInput fullWidth label="Current password" name="old_password" required type='password' />
            <TextInput fullWidth label="New password" name="new_password" required type='password' />
            <TextInput fullWidth label="Confirm new password" name="confirm_password" required type='password' />
            <p>Changing the password will log you out.</p>
            <div className={form__buttons}>
              <button className={update__btn} type='submit'>Update</button>
            </div>
          </form>
        </FormProvider>
      </div>
    </ModalTemplate>
  )
}

export default ChangePassword
