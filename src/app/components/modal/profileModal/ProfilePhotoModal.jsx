import { useContext, useState, useRef } from 'react'

import ModalContext from '../../../context/modal/modalContext'
import UserContext from '../../../context/user/userContext'
import AuthContext from '../../../../context/auth/AuthContext'

import ModalTemplate from './ModalTemplate'

import { Avatar, Plus, TrashBin } from '../../../assets/Icons'

import {
  content,
  header,
  description,
  form,
  form__content,
  form__buttons,
  update__btn,
  cancel__btn
} from './ModalTemplate.module.css'

const ProfilePhoto = () => {
  const fileInput = useRef(null)
  const [image, setImage] = useState({ raw: null, preview: null })
  const [isFilePicked, setIsFilePicked] = useState(false)

  const { profileImageModal, close } = useContext(ModalContext)
  const { addProfileImage } = useContext(UserContext)
  const { loadUser } = useContext(AuthContext)

  const handleChange = (e) => {
    setImage({
      raw: e.target.files[0],
      preview: URL.createObjectURL(e.target.files[0])
    })
    setIsFilePicked(true)
  }

  const removeFile = () => {
    setIsFilePicked(false)
    setImage({ raw: null, preview: null })
  }

  const handleSubmit = async e => {
    e.preventDefault()
    await addProfileImage(image.raw)
    close()
    loadUser()
  }

  return (
    <ModalTemplate open={profileImageModal}>
      <div className={content}>
        <div className={header}>
          <Avatar />
          <p>Change your profile photo</p>
        </div>
        <p className={description}>
          Update your profile photo. Supported formats are PNG and JPEG. Maximum
          size is 1 MB.
        </p>
        <form className={form} onSubmit={handleSubmit}>
          {
            !isFilePicked
              ? (
                <div className={form__content}>
                  <button onClick={() => fileInput.current.click()} type='button'>
                    <span>
                      <Plus />
                    </span>
                      Upload a profile picture
                    </button>
                  <input type="file" style={{ display: 'none' }} ref={fileInput} onChange={handleChange} />
                </div>
              ) : (
                <div className={form__content}>
                  <div>
                    <img src={image.preview} alt='profile' />
                  </div>
                  <div>
                    <button type="button" onClick={removeFile}>
                      <span>
                        <TrashBin />
                      </span>
                      <label>Delete</label>
                    </button>
                  </div>
                </div>
              )
          }
          <div className={form__buttons}>
            <button className={update__btn} type='submit'>Update</button>
            <button className={cancel__btn} onClick={close} type='button'>Cancel</button>
          </div>
        </form>
      </div>
    </ModalTemplate>
  )
}

export default ProfilePhoto
