import { useContext, useEffect, useState } from 'react'
import { useForm, FormProvider } from 'react-hook-form'

import ModalTemplate from './ModalTemplate'
import ModalContext from '../../../context/modal/modalContext'
import DeputyContext from '../../../context/deputy/deputyContext'
import ShareContext from '../../../context/share/shareContext'

import { AvatarPlus, Cross, Plus } from '../../../assets/Icons'

import { content, form, form__buttons, update__btn } from './ModalTemplate.module.css'
import { spacer, title } from './DeputyModal.module.css'
import { deputies__p, deputy__card, card__container, deputy__container, icon, info, remove, add__deputy__btn } from './ShareWithModal.module.css'

const ShareWithModal = () => {
  const [share, setShare] = useState([])
  const { openShareWithDirectlyModal, openModal, close, sharingItem } = useContext(ModalContext)
  const { loadDeputies, deputies } = useContext(DeputyContext)
  const { shareWith, addDeputyToShareWith, deputiesToShareWith, removeDeputyToShareWith, shareHomeSite, shareHomeNotes, shareHomeDevices, deputiesToShareWithIDs } = useContext(ShareContext)
  const methods = useForm()

  const onSubmit = () => {
    shareWith(share)
    close()
    if (sharingItem.item === 'site') {
      shareHomeSite(sharingItem.id, deputiesToShareWithIDs)
    } else if (sharingItem.item === 'document') {
      shareHomeNotes(sharingItem.id, deputiesToShareWithIDs)
    } else {
      shareHomeDevices(sharingItem.id, deputiesToShareWithIDs)
    }
  }

  const handleClick = (deputy) => {
    if (!share.includes(deputy.id)) {
      setShare([...share, deputy.id])
      addDeputyToShareWith(deputy)
    }
  }

  const removeDeputy = (id) => {
    setShare(share.filter(existingId => existingId !== id))
    removeDeputyToShareWith(id)
  }

  useEffect(() => {
    loadDeputies()
    // eslint-disable-next-line
  }, [])

  return (
    <ModalTemplate open={openShareWithDirectlyModal}>
      <div className={content}>
        <h1 className={title}>
          <AvatarPlus />
          Share with deputies
        </h1>
        <p className={spacer}>Enter the email address to a person you would like to invite. The deputy will be invited by email and have the ability to view information or take action according to your wishes.</p>
        <FormProvider {...methods}>
          <form className={form} onSubmit={methods.handleSubmit(onSubmit)}>
            <button className={add__deputy__btn} type='button' onClick={() => openModal('Deputy')}><Plus /><p>Add new deputy</p></button>
            <div className={card__container}>
              <p className={deputies__p}>Deputies to share with</p>
              {deputiesToShareWith.lenght !== 0 && (
                deputiesToShareWith.map(deputy => (
                  <div key={deputy.id} className={deputy__card}>
                    <div className={deputy__container}>
                      <div className={icon}>
                        <div>TT</div>
                      </div>
                      <div className={info}>
                        <p>{deputy.name} {deputy.surname}</p>
                        <p>{deputy.email}</p>
                      </div>
                    </div>
                    <button type="button" className={remove} onClick={() => removeDeputy(deputy.id)}>
                      <Cross />
                    </button>
                  </div>
                ))
              )}
            </div>
            <p className={deputies__p}>Access of your deputies</p>
            <div>
              <div className={card__container}>
                {deputies.length !== 0 && (
                  deputies.map(deputy => (
                    <div key={deputy.id} className={deputy__card} onClick={() => handleClick(deputy)}>
                      <div className={deputy__container}>
                        <div className={icon}>
                          <div>TT</div>
                        </div>
                        <div className={info}>
                          <p>{deputy.name} {deputy.surname}</p>
                          <p>{deputy.email}</p>
                        </div>
                      </div>
                    </div>
                  ))
                )}
              </div>
            </div>
            <div className={form__buttons}>
              <button className={update__btn}>Continue</button>
            </div>
          </form>
        </FormProvider>
      </div>
    </ModalTemplate>
  )
}

export default ShareWithModal
