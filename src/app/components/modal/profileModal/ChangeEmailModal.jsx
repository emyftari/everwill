import { useContext } from 'react'
import { FormProvider, useForm } from 'react-hook-form'

import ModalContext from '../../../context/modal/modalContext'
import UserContext from '../../../context/user/userContext'
import AuthContext from '../../../../context/auth/AuthContext'

import ModalTemplate from './ModalTemplate'
import TextInput from '../../inputs/TextInput'

import { Mail } from '../../../assets/Icons'

import {
  content,
  header,
  form,
  form__buttons,
  update__btn
} from './ModalTemplate.module.css'

const ChangeEmailModal = () => {
  const methods = useForm()
  const { changeEmail } = useContext(UserContext)
  const { changeEmailModal } = useContext(ModalContext)
  const { logout } = useContext(AuthContext)

  const onSubmit = async data => {
    await changeEmail({ email: data.email })
    logout()
  }

  return (
    <ModalTemplate open={changeEmailModal}>
      <div className={content}>
        <div className={header}>
          <Mail />
          <p>Update [email]</p>
        </div>
        <FormProvider {...methods}>
          <form className={form} onSubmit={methods.handleSubmit(onSubmit)}>
            <TextInput fullWidth label="New email address" name="email" required />
            <TextInput fullWidth label="Confirm new email address" name="email_confirmation" required />
            <p>Changing the email will log you out.</p>
            <div className={form__buttons}>
              <button className={update__btn} type='submit'>Update</button>
            </div>
          </form>
        </FormProvider>
      </div>
    </ModalTemplate>
  )
}

export default ChangeEmailModal
