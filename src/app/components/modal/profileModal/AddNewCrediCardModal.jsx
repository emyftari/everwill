import { useContext } from 'react'
import { useForm, FormProvider } from 'react-hook-form'
import ModalContext from '../../../context/modal/modalContext'
import UserContext from '../../../context/user/userContext'

import ModalTemplate from './ModalTemplate'
import CardInput from '../../inputs/creditCardInput/CardInput'

import { CreditCard, Visa, Master, AmericanExpress, JCB } from '../../../assets/Icons'

import {
  content,
  header,
  form,
  form__buttons,
  update__btn,
  cancel__btn,
  credit__cards,
  card__fields
} from './ModalTemplate.module.css'

const AddNewCreditCardTemplate = () => {
  const { openCreditCard, close } = useContext(ModalContext)
  const { addCreditCard, getCreditCard } = useContext(UserContext)
  const methods = useForm()

  const onSubmit = async data => {
    await addCreditCard(data)
    getCreditCard()
    close()
  }

  return (
    <ModalTemplate open={openCreditCard}>
      <div className={content}>
        <div className={header}>
          <CreditCard />
          <p>Add new credit card</p>
        </div>
        <FormProvider {...methods}>
          <form className={form} onSubmit={methods.handleSubmit(onSubmit)}>
            <div className={credit__cards}>
              <Visa />
              <Master />
              <AmericanExpress />
              <JCB />
            </div>
            <div className={card__fields}>
              <CardInput name='card_number' label='Card number' mask='9999 9999 9999 9999' required />
              <CardInput name='zip_code' label='ZIP code' mask='99999' required />
              <CardInput name='card_date' label='Expiration Date' mask='99/99' required />
              <CardInput name='card_cvc' label='CVV/CVC' mask='999' required />
            </div>
            <div className={form__buttons}>
              <button className={update__btn} type='submit'>Add</button>
              <button className={cancel__btn} type='button' onClick={close}>Cancel</button>
            </div>
          </form>
        </FormProvider>
      </div>
    </ModalTemplate>
  )
}

export default AddNewCreditCardTemplate
