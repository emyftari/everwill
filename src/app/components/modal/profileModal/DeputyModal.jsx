import { useState, useContext } from 'react'
import { useForm, FormProvider } from 'react-hook-form'

import { ConnectForm } from '../../../context/form/formContext'
import ModalContext from '../../../context/modal/modalContext'
import DeputyContext from '../../../context/deputy/deputyContext'

import ModalTemplate from './ModalTemplate'

import { AvatarPlus, Check } from '../../../assets/Icons'

import TextInput from '../../inputs/TextInput'
import TextArea from '../../inputs/deputy/TextArea'
import RelationshipSelect from '../../inputs/RelationshipSelect'

import logo from '../../../assets/images/logo.png'

import {
  content,
  form,
  form__buttons,
  update__btn,
} from './ModalTemplate.module.css'

import {
  spacer,
  label,
  checkbox,
  svg__box,
  desc,
  form__container,
  title,
  preview__btn,
  preview__container,
  preview__title
} from './DeputyModal.module.css'

const ChangeNameModal = () => {
  const [showNote, setShowNote] = useState(false)
  const [preview, setPreview] = useState(false)
  const [textAreaValue, setTextAreaValue] = useState('')
  const { openDeputyModal, close } = useContext(ModalContext)
  const { addDeputy } = useContext(DeputyContext)
  const methods = useForm()

  const onSubmit = data => {
    addDeputy({ ...data, textAreaValue })
    close()
  }



  return (
    <ModalTemplate open={openDeputyModal}>
      <div className={content}>
        <h1 className={title}>
          <AvatarPlus />
          Add a new deputy
        </h1>
        <p className={spacer}></p>
        <FormProvider {...methods}>
          <form className={form} onSubmit={methods.handleSubmit(onSubmit)}>
            {!preview && (
              <div>
                <div className={form__container}>
                  <TextInput fullWidth label="Deputy's email address" name="email" required />
                  <p>Please share more info</p>
                  <TextInput label="Name" name="first_name" required />
                  <TextInput label="Surname" name="last_name" required />
                  <RelationshipSelect />
                  <TextInput fullWidth label="Phone with country code (optional)" name="phone" />
                  <label className={label}>
                    <ConnectForm>
                      {({ register }) => (
                        <input name="trusted" type="checkbox" className={checkbox} {...register('give_right')} />
                      )}
                    </ConnectForm>
                    <div className={svg__box}>
                      <Check />
                    </div>
                    <p className={desc}>
                      Give this deputy the right to trigger events at GoodTrust
                      following my death.
                    </p>
                  </label>
                  <label className={label}>
                    <input
                      name="show_note"
                      type="checkbox"
                      className={checkbox}
                      onChange={() => setShowNote(!showNote)}
                    />
                    <div className={svg__box}>
                      <Check />
                    </div>
                    <p className={desc}>Send an invite message</p>
                  </label>
                  {showNote && <TextArea fullWidth value={textAreaValue} setValue={setTextAreaValue} />}
                </div>
              </div>
            )}
            {preview && (
              <div>
                <div style={{ display: 'block' }}>
                  <p className={preview__title}>preview of your message</p>
                  <div className={preview__container}>
                    <img src={logo} alt="" />
                    <div>
                      <p>Hello,</p>
                      <p>Kristi has sent you an invitation to become deputy.</p>
                      <p>
                        Here is a message what Kristi left to you:<br />
                        <strong>{textAreaValue}</strong>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            )}
            <div className={form__buttons}>
              <button className={update__btn} type='submit'>{preview ? 'Send' : 'Continue'}</button>
              <button type='button' className={preview__btn} onClick={() => setPreview(!preview)}>{preview ? 'Back' : 'Preview'}</button>
            </div>
          </form>
        </FormProvider>
      </div>
    </ModalTemplate>
  )
}

export default ChangeNameModal
