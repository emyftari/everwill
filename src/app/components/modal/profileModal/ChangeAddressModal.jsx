import { useContext } from 'react'
import { FormProvider, useForm } from 'react-hook-form'

import ModalContext from '../../../context/modal/modalContext'
import UserContext from '../../../context/user/userContext'
import AuthContext from '../../../../context/auth/AuthContext'

import ModalTemplate from './ModalTemplate'
import TextInput from '../../inputs/TextInput'
import CountrySelect from '../../../../pages/customInputs/CountrySelect'

import { HomeV2 } from '../../../assets/Icons'

import {
  content,
  header,
  form,
  form__buttons,
  update__btn,
  field__container
} from './ModalTemplate.module.css'

const ChangeAddressModal = () => {
  const methods = useForm()
  const { changeAddressModal, close } = useContext(ModalContext)
  const { changeAddress } = useContext(UserContext)
  const { loadUser } = useContext(AuthContext)

  const onSubmit = async (data) => {
    await changeAddress(data)
    close()
    loadUser()
  }

  return (
    <ModalTemplate open={changeAddressModal}>
      <div className={content}>
        <div className={header}>
          <HomeV2 />
          <p>Change your address</p>
        </div>
        <FormProvider {...methods}>
          <form className={form} onSubmit={methods.handleSubmit(onSubmit)}>
            <div className={field__container}>
              <TextInput
                fullWidth
                label="Street Address"
                name="street_address"
                required
              />
              <TextInput fullWidth label="City" name="city" required />
              <TextInput label="ZIP code" name="zip_code" required />
              <TextInput label="State/Province/Region" name="state" required />
              <CountrySelect />
            </div>
            <div className={form__buttons}>
              <button className={update__btn}>Update</button>
            </div>
          </form>
        </FormProvider>
      </div>
    </ModalTemplate>
  )
}

export default ChangeAddressModal
