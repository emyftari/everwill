import { useContext } from 'react'
import { FormProvider, useForm } from 'react-hook-form'

import ModalContext from '../../../context/modal/modalContext'
import UserContext from '../../../context/user/userContext'
import AuthContext from '../../../../context/auth/AuthContext'

import ModalTemplate from './ModalTemplate'
import TextInput from '../../inputs/TextInput'

import { content, header, form, form__buttons, update__btn } from './ModalTemplate.module.css'

import { PhoneIcon } from '../../../assets/Icons'

const TwoFaAuthModal = () => {
  const methods = useForm()
  const { open2fa, close } = useContext(ModalContext)
  const { addPhoneNumber } = useContext(UserContext)
  const { user, loadUser } = useContext(AuthContext)

  const onSubmit = async data => {
    await addPhoneNumber(data)
    loadUser()
    close()
  }

  return (
    <ModalTemplate open={open2fa}>
      <div className={content}>
        <div className={header}>
          <PhoneIcon />
          <p>Set your phone number</p>
        </div>
        <FormProvider {...methods}>
          <form className={form} onSubmit={methods.handleSubmit(onSubmit)}>
            <TextInput fullWidth label="Phone number" name="phone_number" required value={user && user.phone_number} />
            <div className={form__buttons}>
              <button className={update__btn}>Continue</button>
            </div>
          </form>
        </FormProvider>
      </div>
    </ModalTemplate>
  )
}

export default TwoFaAuthModal
