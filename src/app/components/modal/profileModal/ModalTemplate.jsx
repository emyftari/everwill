import { useContext } from 'react'

import ModalContext from '../../../context/modal/modalContext'

import { Cross } from '../../../assets/Icons'

import {
  hide__modal,
  modal__overlay,
  modal__content,
  modal__container,
  background,
  close__btn
} from './ModalTemplate.module.css'

const ModalTemplate = ({ children, open }) => {
  const { close } = useContext(ModalContext)

  return (
    <div className={!open ? hide__modal : modal__overlay}>
      <div className={modal__content}>
        <div>
          <div className={modal__container}>
            <div className={background}>
              <button className={close__btn} onClick={close}>
                <Cross />
              </button>
              {children}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ModalTemplate
