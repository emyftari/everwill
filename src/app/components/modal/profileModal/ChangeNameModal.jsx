import { useContext } from 'react'
import { FormProvider, useForm } from 'react-hook-form'

import AuthContext from '../../../../context/auth/AuthContext'
import ModalContext from '../../../context/modal/modalContext'
import UserContext from '../../../context/user/userContext'

import ModalTemplate from './ModalTemplate'
import TextInput from '../../inputs/TextInput'

import { Avatar } from '../../../assets/Icons'
import { content, header, form, form__buttons, update__btn } from './ModalTemplate.module.css'

const ChangeNameModal = () => {
  const methods = useForm()
  const { changeNameModal, close } = useContext(ModalContext)
  const { changeName } = useContext(UserContext)
  const { loadUser, user } = useContext(AuthContext)

  const onSubmit = async data => {
    await changeName(data)
    close()
    loadUser()
  }

  return (
    <ModalTemplate open={changeNameModal}>
      <div className={content}>
        <div className={header}>
          <Avatar />
          <p>Change your name</p>
        </div>
        <FormProvider {...methods}>
          <form className={form} onSubmit={methods.handleSubmit(onSubmit)}>
            <TextInput fullWidth label="Name" name="first_name" required value={user && user.first_name} />
            <TextInput fullWidth label="Surname" name="last_name" required value={user && user.last_name} />
            <div className={form__buttons}>
              <button className={update__btn} type='submit'>Update</button>
            </div>
          </form>
        </FormProvider>
      </div>
    </ModalTemplate>
  )
}

export default ChangeNameModal
