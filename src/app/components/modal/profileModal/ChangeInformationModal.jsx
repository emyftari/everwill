import { useContext } from 'react'
import { FormProvider, useForm } from 'react-hook-form'

import ModalContext from '../../../context/modal/modalContext'
import UserContext from '../../../context/user/userContext'
import AuthContext from '../../../../context/auth/AuthContext'

import ModalTemplate from './ModalTemplate'
import DateInput from '../../../../pages/customInputs/DateInput'
import GenderSelect from '../../../../pages/customInputs/GenderSelect'
import SocialStatusSelect from '../../../../pages/customInputs/SocialStatusSelect'
import ChildrenSelect from '../../../../pages/customInputs/ChildrenSelect'

import { IDCard } from '../../../assets/Icons'

import {
  content,
  header,
  form,
  form__buttons,
  update__btn,
  field__info__container
} from './ModalTemplate.module.css'

const ChangeInformationModal = () => {
  const methods = useForm()
  const { changeInformationModal, close } = useContext(ModalContext)
  const { changeInformation } = useContext(UserContext)
  const { loadUser, user } = useContext(AuthContext)

  const onSubmit = async (data) => {
    await changeInformation(data)
    close()
    loadUser()
  }

  return (
    <ModalTemplate open={changeInformationModal}>
      <div className={content}>
        <div className={header}>
          <IDCard />
          <p>Update Information</p>
        </div>
        <FormProvider {...methods}>
          <form className={form} onSubmit={methods.handleSubmit(onSubmit)}>
            <div className={field__info__container}>
              <DateInput half value={user && user.birthday} />
              <GenderSelect half />
              <SocialStatusSelect half />
              <ChildrenSelect half />
            </div>
            <div className={form__buttons}>
              <button className={update__btn}>Update</button>
            </div>
          </form>
        </FormProvider>
      </div>
    </ModalTemplate>
  )
}

export default ChangeInformationModal
