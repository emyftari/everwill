export const categoryModalData = [
  { category: 'Family' },
  { category: 'Health' },
  { category: 'Bank' },
  { category: 'Insurance' },
  { category: 'Contracts' },
  { category: 'Identity' },
  { category: 'Work' },
  { category: 'Will' },
  { category: 'Legal' },
  { category: 'Assets' },
  { category: 'Receipts' },
  { category: 'Photos' },
  { category: 'Taxes' },
  { category: 'Other' }
]