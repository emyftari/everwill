import { useContext, useState } from 'react'
import ModalContext from '../../context/modal/modalContext'
import FieldContext from '../../context/field/fieldContext'

import ModalTemplate from '../templates/ModalTemplate'
import SearchBar from '../searchBar/SearchBar'

import { main__container, main__head } from './ActionModal.module.css'
import { category__container } from './Category.module.css'

import { categoryModalData } from './categoryModalData'

const CategoryModal = () => {
  const { openCategoryModal, closeModal } = useContext(ModalContext)
  const { setCategoryLabel } = useContext(FieldContext)

  const [keyword, setKeyword] = useState('')
  const [categoryList, setCategoryList] = useState(categoryModalData)

  const handleClick = category => {
    setCategoryLabel(category)
    closeModal()
  }

  const updateInput = async (keyword) => {
    const filtered = categoryModalData.filter(category => {
      return category.category.toLowerCase().includes(keyword.toLowerCase())
    })
    setKeyword(keyword)
    setCategoryList(filtered)
  }

  return (
    <ModalTemplate open={openCategoryModal}>
      <main className={main__container}>
        <div className={main__head}>
          <h1>Choose category.</h1>
          <SearchBar keyword={keyword} setKeyword={updateInput} />
        </div>
        <div className={category__container}>
          {
            categoryList.map(data => (
              <button
                key={data.category}
                onClick={() => handleClick(data.category)}
              >
                <span>{data.category}</span>
              </button>
            ))
          }
        </div>
      </main>
    </ModalTemplate>
  )
}

export default CategoryModal