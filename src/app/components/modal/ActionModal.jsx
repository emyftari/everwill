import { useContext } from 'react'
import ModalContext from '../../context/modal/modalContext'

import ModalTemplate from '../templates/ModalTemplate'
import ActionCard from '../card/ActionCard'

import {
  main__container,
  card__container,
  card__grid
} from './ActionModal.module.css'

import { data } from '../card/actionCardData'

const ActionModal = () => {
  const { openActionModal } = useContext(ModalContext)

  return (
    <ModalTemplate open={openActionModal}>
      <main className={main__container}>
        <h1>Choose an action.</h1>
        <div className={card__container}>
          <div className={card__grid}>
            {data.map((card) => (
              <ActionCard key={card.title} {...card} />
            ))}
          </div>
        </div>
      </main>
    </ModalTemplate>
  )
}

export default ActionModal
