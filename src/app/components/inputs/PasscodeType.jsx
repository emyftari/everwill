import { ConnectForm } from '../../context/form/formContext'
import { field } from '../../../pages/customInputs/TextInput.module.css'

const PasscodeType = ({ setType }) => {
  return (
    <fieldset className={field}>
      <ConnectForm>
        {({ register }) => (
          <select name="times" placeholder="&nbsp;" {...register('password_type')} onChange={e => setType(e.target.value)}>
            <option disabled hidden></option>
            <option value="passcode">Type-in passcode</option>
            <option value="swipeing_gesture">Swipe Gesture</option>
          </select>
        )}
      </ConnectForm>
      <label>Passcode type</label>
    </fieldset>
  )
}

export default PasscodeType
