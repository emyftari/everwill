import { ConnectForm } from '../../context/form/formContext'
import clsx from 'clsx'

import { field, halfField, popError, fieldError } from './Inputs.module.css'

const TextInput = ({ fullWidth, label, disabled, name, required, value, type = 'text', fn }) => {
  return (
    <ConnectForm>
      {({ register, formState: { errors } }) => (
        <fieldset
          className={clsx(
            field,
            fullWidth && halfField,
            errors[name] && fieldError
          )}
        >
          <input
            type={type}
            onInput={fn}
            autoComplete="new-password"
            placeholder="&nbsp;"
            disabled={disabled}
            {...register(name, { required })}
            defaultValue={value}
          />
          {errors[name]?.type === 'required' && (
            <span className={popError}>Required field</span>
          )}
          <label>{label}</label>
        </fieldset>
      )}
    </ConnectForm>
  )
}

export default TextInput
