import { ConnectForm } from '../../context/form/formContext'
import { field } from '../../../pages/customInputs/TextInput.module.css'

const QuestionCategorySelect = () => {
  return (
    <fieldset className={field}>
      <ConnectForm>
        {({ register }) => (
          <select name="category" placeholder="&nbsp;" {...register('category')}>
            <option value="General">General</option>
            <option value="Contact">Contact</option>
            <option value="Partnership">Partnership</option>
            <option value="Support">Support</option>
            <option value="Billing">Billing</option>
            <option value="Problem">Problem</option>
          </select>
        )}
      </ConnectForm>
      <label>Question category</label>
    </fieldset>
  )
}

export default QuestionCategorySelect
