import clsx from 'clsx'
import { field, halfField } from '../Inputs.module.css'

const TextArea = ({ fullWidth, value, setValue }) => {
  return (
    <fieldset className={clsx(field, fullWidth && halfField)}>
      <textarea name="note" placeholder="&nbsp;" defaultValue={value} onChange={(e) => setValue(e.target.value)}></textarea>
      <label>Notes &amp; instructions</label>
    </fieldset>
  )
}

export default TextArea
