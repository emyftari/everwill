import { field, halfField } from './Inputs.module.css'
import clsx from 'clsx'

const DisabledInput = ({ fullWidth }) => {
  return (
    <fieldset
      className={clsx(
        field,
        fullWidth && halfField,
      )}
    >
      <input disabled placeholder="&nbsp;" />
      <label>Password (coming soon)</label>
    </fieldset>
  )
}

export default DisabledInput

