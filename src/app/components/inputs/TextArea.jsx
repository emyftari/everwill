import { ConnectForm } from '../../context/form/formContext'
import clsx from 'clsx'
import { field, halfField } from './Inputs.module.css'

const TextArea = ({ fullWidth, value }) => {
  return (
    <ConnectForm>
      {({ register }) => (
        <fieldset className={clsx(field, fullWidth && halfField)}>
          <textarea name="note" placeholder="&nbsp;" {...register('notes')} defaultValue={value}></textarea>
          <label>Notes &amp; instructions</label>
        </fieldset>
      )}
    </ConnectForm>
  )
}

export default TextArea
