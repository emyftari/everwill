import { ConnectForm } from "../../context/form/formContext"
import { field } from "../../../pages/customInputs/TextInput.module.css"

const TimingSelect = ({ setValue, value }) => {
  return (
    <fieldset className={field}>
      <ConnectForm>
        {({ register }) => (
          <select name="timing" placeholder="&nbsp;" {...register("timing")} onChange={e => setValue(e.target.value)} defaultValue={value}>
            <option value="death_reported">When death is reported</option>
            <option value="specific_day">Specific day</option>
          </select>
        )}
      </ConnectForm>
      <label>How many times</label>
    </fieldset>
  )
}

export default TimingSelect
