import { ConnectForm } from '../../context/form/formContext'
import { field } from '../../../pages/customInputs/TextInput.module.css'

const CountrySelect = () => {
  return (
    <fieldset className={field}>
      <ConnectForm>
        {({ register }) => (
          <select name="relationship" placeholder="&nbsp;" {...register('relationship')}>
            <option value="spouse">Spouse</option>
            <option value="children">Children</option>
            <option value="sibling">Sibling</option>
            <option value="family">Family</option>
            <option value="friend">Friend</option>
            <option value="attorney">Attorney</option>
          </select>
        )}
      </ConnectForm>
      <label>Relationship</label>
    </fieldset>
  )
}

export default CountrySelect
