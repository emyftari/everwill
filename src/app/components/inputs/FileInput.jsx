import { useState, useRef, useContext, useEffect } from 'react'
import { triggerBase64Download } from 'react-base64-downloader'

import MediaContext from '../../context/media/mediaContext'

import { Plus, CheckNote, Cross } from '../../assets/Icons'
import {
  container,
  content,
  content__file,
  icon,
  info,
  hide
} from './FileInput.module.css'

const FileInput = ({ file }) => {
  const fileInput = useRef(null)
  const [attachment, setAttachment] = useState()
  const [selectedFile, setSelectedFile] = useState()
  const [isFilePicked, setIsFilePicked] = useState(false)
  const { setMedia } = useContext(MediaContext)

  useEffect(() => {
    if (file) {
      setAttachment(file)
      setIsFilePicked(true)
    }
    // eslint-disable-next-line
  }, [])

  const handleChange = (e) => {
    setSelectedFile(e.target.files[0])
    setMedia(e.target.files[0])
    setIsFilePicked(true)
  }

  const removeFile = () => {
    setIsFilePicked(false)
    setAttachment(null)
    setSelectedFile(null)
    setMedia(null)
  }

  return (
    <div className={container}>
      {!isFilePicked ? (
        <div
          className={isFilePicked ? hide : content}
          onClick={() => fileInput.current.click()}
        >
          <label>Drop an attachment here</label>
          <input type="file" ref={fileInput} onChange={handleChange} />
          <Plus />
          <p>Maximum file size: 5 MB</p>
        </div>
      ) : (
        <div className={content__file}>
          <div className={icon}>
            <CheckNote />
          </div>
          <div>
            <div className={info}>
              <span>
                <strong>{attachment ? attachment.filename : selectedFile.name}</strong>
              </span>
              <button onClick={removeFile}>
                <Cross />
              </button>
              {attachment && (
                <div>
                  <button type='button' onClick={() => triggerBase64Download(file.file, 'file')}>Open</button>
                </div>
              )}
            </div>
          </div>
        </div>
      )}
    </div>
  )
}

export default FileInput
