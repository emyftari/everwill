import clsx from 'clsx'
import { fieldArea, halfField } from './Inputs.module.css'

import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'
import './TextAreaToolbar.css'

const TextAreaToolbar = ({ fullWidth, value, setValue }) => {

  return (
    <fieldset className={clsx(fieldArea, fullWidth && halfField)} >
      <ReactQuill value={value} onChange={setValue} className='textArea' />
    </fieldset>
  )
}

export default TextAreaToolbar
