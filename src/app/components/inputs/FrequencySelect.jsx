import { ConnectForm } from '../../context/form/formContext'
import { field } from '../../../pages/customInputs/TextInput.module.css'

const FrequencySelect = () => {
  return (
    <fieldset className={field}>
      <ConnectForm>
        {({ register }) => (
          <select name="often" placeholder="&nbsp;" {...register('often')}>
            <option value="every_month">Every month</option>
            <option value="every_year">Every year</option>
          </select>
        )}
      </ConnectForm>
      <label>How often</label>
    </fieldset>
  )
}

export default FrequencySelect
