import { ConnectForm } from '../../context/form/formContext'
import { field } from '../../../pages/customInputs/TextInput.module.css'

const TimeSelect = () => {
  return (
    <fieldset className={field}>
      <ConnectForm>
        {({ register }) => (
          <select name="times" placeholder="&nbsp;" {...register('times')}>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
          </select>
        )}
      </ConnectForm>
      <label>How many times</label>
    </fieldset>
  )
}

export default TimeSelect
