import InputMask from 'react-input-mask'
import { ConnectForm } from '../../../context/form/formContext'
import { field, halfField, popError, fieldError } from '../Inputs.module.css'
import clsx from 'clsx'

const CardInput = ({ fullWidth, label, name, required, mask }) => {
  return (
    <ConnectForm>
      {({ register, formState: { errors } }) => (
        <fieldset
          className={clsx(
            field,
            fullWidth && halfField,
            errors[name] && fieldError
          )}
        >
          <InputMask
            placeholder="&nbsp;"
            maskChar=""
            mask={mask}
            {...register(name, { required })}
          />
          {errors[name]?.type === 'required' && (
            <span className={popError}>Required field</span>
          )}
          <label>{label}</label>
        </fieldset>
      )}
    </ConnectForm>
  )
}

export default CardInput
