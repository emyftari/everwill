import { button } from './AddButton.module.css'
import { Plus } from '../../assets/Icons'
import { useHistory } from 'react-router-dom'

const AddButton = ({ path }) => {
  const history = useHistory()
  return (
    <button className={button} onClick={() => history.push(path)}>
      <Plus />
      Add
    </button>
  )
}

export default AddButton