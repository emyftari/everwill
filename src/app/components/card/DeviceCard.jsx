import { useState, useContext } from 'react'
import { useHistory } from 'react-router-dom'

import DeviceContext from '../../context/device/deviceContext'
import ModalContext from '../../context/modal/modalContext'

import { Dots, Edit, Share, Delete } from '../../assets/Icons'
import { uppercase } from './uppercase'

import {
  card__container,
  inner_container,
  card__button,
  image__container,
  text__action,
  button__container,
  site__modal,
  modal__content
} from './SiteCard.module.css'

const SiteCard = (props) => {
  const history = useHistory()
  const [showModal, setShowModal] = useState(false)

  const { openModal, setId } = useContext(ModalContext)
  const { deleteDevice, setCurrentDevice } = useContext(DeviceContext)

  const current = {
    id: props.id,
    name: props.name,
    example: props.example,
    password_type: props.password_type,
    password: props.password
  }

  const handleCurrent = () => {
    setCurrentDevice(current)
    history.push('/dashboard/add-new-device')
  }

  return (
    <div className={card__container}>
      <div className={inner_container}>
        <button className={card__button}>
          <div className={image__container}>
            <p>{props.name}</p>
          </div>
          <p className={text__action}>{uppercase(props.password_type)}</p>
          <div className={button__container}>
            <span
              onClick={() => {
                openModal('Share directly')
                setId({ id: props.id, item: 'device' })
              }}
            >Share</span>
            <span onClick={() => setShowModal(!showModal)}>
              <Dots />
            </span>
          </div>
        </button>
      </div>
      {showModal && (
        <div className={site__modal}>
          <div className={modal__content}>
            {' '}
            <button
              onClick={() => {
                openModal('Share directly')
                setId({ id: props.id, item: 'device' })
              }}
            >
              <span>
                <Share />
              </span>
              <label>Share device</label>
            </button>
            <button onClick={handleCurrent}>
              <span>
                <Edit />
              </span>
              <label>Edit device</label>
            </button>
            <button onClick={() => deleteDevice(props.id)}>
              <span>
                <Delete />
              </span>
              <label>Delete decive</label>
            </button>
          </div>
        </div>
      )}
    </div>
  )
}

export default SiteCard
