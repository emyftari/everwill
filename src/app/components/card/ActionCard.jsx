import { useContext } from 'react'
import ModalContext from '../../context/modal/modalContext'
import FieldContext from '../../context/field/fieldContext'

import { card, card__content, card__head } from './ActionCard.module.css'

const Section = ({ icon, title, description, happen_next }) => {
  const { closeModal } = useContext(ModalContext)
  const { setActionLabel } = useContext(FieldContext)

  const handleClick = () => {
    setActionLabel({ title, happen_next })
    closeModal()
  }

  return (
    <div className={card} onClick={handleClick}>
      <div className={card__content}>
        <div className={card__head}>{icon}</div>
        <p>{title}</p>
        <p>{description}</p>
      </div>
    </div>
  )
}

export default Section
