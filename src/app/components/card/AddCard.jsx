import { useHistory } from 'react-router-dom'

import { card, card__content, card__head } from "./AddCard.module.css"
import { Plus } from '../../assets/Icons'

const Section = ({ title, desc, to }) => {
  const history = useHistory()

  return (
    <div className={card} onClick={() => history.push(to)}>
      <div className={card__content}>
        <div className={card__head}>
          <Plus />
          <span>Takes 1 min.</span>
        </div>
        <p>{title}</p>
        <p>{desc}</p>
      </div>
    </div>
  )
}

export default Section
