export const uppercase = (str) => {
  const removeUnderscore = str.split('_').join(' ')
  return removeUnderscore.charAt(0).toUpperCase() + removeUnderscore.slice(1)
}