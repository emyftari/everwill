import { useState, useContext } from 'react'
import { useHistory } from 'react-router-dom'
import SiteContext from '../../context/site/siteContext'
import ModalContext from '../../context/modal/modalContext'
import FieldContext from '../../context/field/fieldContext'

import { Dots, Edit, Share, Delete } from '../../assets/Icons'
import { uppercase } from './uppercase'
import { dataSites } from '../../assets/images/siteImages'

import {
  card__container,
  inner_container,
  card__button,
  image__container,
  text__action,
  button__container,
  site__modal,
  modal__content
} from './SiteCard.module.css'

const SiteCard = (props) => {
  const history = useHistory()
  const [showModal, setShowModal] = useState(false)
  const { deleteSite, setCurrentSite } = useContext(SiteContext)
  const { toggleNewSite } = useContext(FieldContext)
  const { openModal, setId } = useContext(ModalContext)

  const current = {
    id: props.id,
    category: props.category,
    happen_next: props.happen_next,
    name: props.name,
    profile_link: props.profile_link,
    username: props.username,
    notes: props.notes,
    site_url: props.site_url,
    site_title: props.site_title,
    attach: props.attachment
  }

  const handleCurrent = () => {
    if (current.site_url !== undefined) {
      toggleNewSite()
      console.log(current)
    }
    setCurrentSite(current)

    history.push('/dashboard/add-new-site')
  }

  const res = dataSites.filter(
    (site) => site.site.toLowerCase() === props.name.toLowerCase()
  )

  return (
    <div className={card__container}>
      <div className={inner_container}>
        <button className={card__button}>
          <div className={image__container}>
            <span>
              <img src={props.name !== 'New Site' && res[0].img} alt="" />
            </span>
            <p>{props.name}</p>
          </div>
          <p className={text__action}>{uppercase(props.happen_next)}</p>
          <div className={button__container}>
            <span onClick={() => {
              openModal('Share directly')
              setId({ id: props.id, item: 'site' })
            }}>Share</span>
            <span onClick={() => setShowModal(!showModal)}>
              <Dots />
            </span>
          </div>
        </button>
      </div>
      {showModal && (
        <div className={site__modal}>
          <div className={modal__content}>
            {' '}
            <button onClick={() => {
              openModal('Share directly')
              setId({ id: props.id, item: 'site' })
            }}>
              <span>
                <Share />
              </span>
              <label>Share site</label>
            </button>
            <button onClick={handleCurrent}>
              <span>
                <Edit />
              </span>
              <label>Edit site</label>
            </button>
            <button onClick={() => deleteSite(props.id)}>
              <span>
                <Delete />
              </span>
              <label>Delete site</label>
            </button>
          </div>
        </div>
      )
      }
    </div >
  )
}

export default SiteCard
