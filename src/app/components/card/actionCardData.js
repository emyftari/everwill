import {
  TrashBin,
  CheckShield,
  CrossArrow,
  DownloadSquare,
  CreditCard,
  AvatarRound,
  QuestionMarkRound
} from '../../assets/Icons'

export const data = [
  {
    icon: <TrashBin />,
    title: 'Delete',
    description: 'Delete  page to protect your loved ones.',
    happen_next: 'delete'
  },
  {
    icon: <CheckShield />,
    title: 'Memorialize',
    description: 'Memorialize  page to protect your loved ones.',
    happen_next: 'memorialize'
  },
  {
    icon: <CrossArrow />,
    title: 'Full access',
    description: 'Get full access to  page to protect your loved ones.',
    happen_next: 'full_access'
  },
  {
    icon: <DownloadSquare />,
    title: 'Extract data',
    description: 'Extract data from  page to protect your loved ones.',
    happen_next: 'extract_data'
  },
  {
    icon: <CreditCard />,
    title: 'Extract Funds',
    description: 'Extract funds from  page to protect your loved ones.',
    happen_next: 'extract_funds'
  },
  {
    icon: <AvatarRound />,
    title: 'Legacy contract',
    description: 'Legacy contract  page to protect your loved ones.',
    happen_next: 'legacy_contract'
  },
  {
    icon: <QuestionMarkRound />,
    title: 'Special request',
    description: 'Special request for  page to protect your loved ones.',
    happen_next: 'special_request'
  }
]