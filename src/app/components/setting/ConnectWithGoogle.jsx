import { useContext } from 'react'

import AuthContext from '../../../context/auth/AuthContext'

import GoogleLogin from 'react-google-login'

import {
  container,
  text__container,
  text__primary,
  text__secondary
} from './Setting.module.css'

import { Google } from '../../assets/Icons'

const Setting = () => {
  const { googleLogin } = useContext(AuthContext)

  return (
    <div className={container}>
      <Google />
      <div className={text__container}>
        <div className={text__primary}>
          <p>Google</p>
          <div>
            <GoogleLogin
              clientId="126571944175-ug751vd0rk493gpg1nget2mnroqjjg0m.apps.googleusercontent.com"
              onSuccess={googleLogin}
              onFailure={googleLogin}
              cookiePolicy={'single_host_origin'}
              render={renderProps => (
                <button onClick={renderProps.onClick}>
                  Connect
                </button>
              )}
            />
          </div>
        </div>
        <div className={text__secondary}>
          <p>Not connected</p>
        </div>
      </div>
    </div>
  )
}

export default Setting
