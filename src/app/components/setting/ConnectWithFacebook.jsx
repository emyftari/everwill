import { useContext } from 'react'

import AuthContext from '../../../context/auth/AuthContext'

import FacebookLogin from 'react-facebook-login'

import {
  container,
  text__container,
  text__primary,
  text__secondary,
  connect__btn
} from './Setting.module.css'

import { Facebook } from '../../assets/Icons'

const Setting = () => {
  const { facebookLogin } = useContext(AuthContext)

  return (
    <div className={container}>
      <Facebook />
      <div className={text__container}>
        <div className={text__primary}>
          <p>Facebook</p>
          <div>
            <FacebookLogin
              appId="1117697788639861"
              fields="name,email,picture"
              textButton='Connect'
              cssClass={connect__btn}
              callback={facebookLogin}
            />
          </div>
        </div>
        <div className={text__secondary}>
          <p>Not connected</p>
        </div>
      </div>
    </div>
  )
}

export default Setting
