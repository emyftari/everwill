import { useContext } from 'react'
import { useHistory } from 'react-router-dom'

import ModalContext from '../../context/modal/modalContext'
import UserContext from '../../context/user/userContext'
import AlertContext from '../../context/alert/alertContext'

import {
  container,
  text__container,
  text__primary,
  text__secondary
} from './Setting.module.css'

const Setting = ({ icon, primaryText, secondaryText, btnText, path }) => {
  const { openModal } = useContext(ModalContext)
  const { setAlert } = useContext(AlertContext)
  const { deadman, disableDeadManSwitch, deleteCreditCard } = useContext(UserContext)
  const history = useHistory()

  const handleClick = () => {
    if (btnText === 'Delete') {
      history.push('/dashboard/delete')
    } else if (btnText === 'Delete Card') {
      deleteCreditCard()
    } else if (deadman !== null) {
      disableDeadManSwitch()
      setAlert(`Dead man's switch is disabled.`, 'info')
    } else {
      if (path) {
        history.push('/dashboard/dead-man-switch')
      } else {
        openModal(primaryText)
      }
    }
  }

  return (
    <div className={container}>
      {icon}
      <div className={text__container}>
        <div className={text__primary}>
          <p>{primaryText}</p>
          <div>
            <button onClick={handleClick}>{btnText}</button>
          </div>
        </div>
        <div className={text__secondary}>
          <p>{secondaryText}</p>
        </div>
      </div>
    </div>
  )
}

export default Setting
