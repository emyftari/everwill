import { useContext, useState } from 'react'
import PatternLock from 'react-pattern-lock'

import DeviceContext from '../../context/device/deviceContext'

import ShareField from '../field/ShareField'
import TextInput from '../inputs/TextInput'
import PasscodeType from '../inputs/PasscodeType'
import TextArea from '../inputs/TextArea'

import './pattern.css'

import {
  field__grid,
  grid__side,
  side__container,
  field__inner,
  swipe,
  swipe__container,
  swipper,
  swipe__content
} from './Form.module.css'
import { useEffect } from 'react'

const AddNewDeviceForm = () => {
  const [type, setType] = useState('passcode')
  const { currentDevice, onFinish } = useContext(DeviceContext)

  const [path, setPath] = useState([])

  const onChange = (path) => setPath(path)

  useEffect(() => {
    if (currentDevice) {
      setType(currentDevice.password_type)
      console.log(type)
      if (currentDevice.password_type === 'swipeing_gesture') {
        setPath(Array.from(currentDevice.password.toString()).map(Number))
      }
    }
    // eslint-disable-next-line
  }, [])

  return (
    <div className={field__grid}>
      <div className={grid__side}>
        <div className={side__container}>
          <p>DEVICE INFORMATION</p>
          <div className={field__inner}>
            <TextInput fullWidth label="Device name" name="device_name" required value={currentDevice && currentDevice.name} />
            <PasscodeType setType={setType} />
            {type === 'passcode' && (
              <TextInput
                required
                fullWidth
                label='Password'
                name='password'
                type='password'
                value={currentDevice && currentDevice.password}
              />
            )}
            {type === 'swipeing_gesture' && (
              <fieldset className={swipe}>
                <div className={swipe__container}>
                  <div className={swipe__content}>
                    <PatternLock
                      size={3}
                      onChange={onChange}
                      path={path}
                      onFinish={() => onFinish(path)}
                      connectorThickness={5}
                      className={swipper}
                    />
                  </div>
                  <p>Click and hold to start drawing your gesture</p>
                </div>
              </fieldset>
            )}
            <p><svg width="17" height="18" viewBox="0 0 17 18" fill="none" xmlns="http://www.w3.org/2000/svg"><g clipPath="url(#IconShield_svg__clip0)" stroke="#657795" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"><path d="M4.781 8.33l1.736 2.465a.743.743 0 001.209.036L13.28 3.8"></path><path d="M2.125 1.03a1.063 1.063 0 00-1.063 1.063v6.375c0 3.409 5.125 7.203 6.866 8.33a1.046 1.046 0 001.144 0c1.741-1.127 6.866-4.921 6.866-8.33V2.093a1.062 1.062 0 00-1.063-1.063H2.125z"></path></g><defs><clipPath id="IconShield_svg__clip0"><path fill="#fff" transform="translate(0 .5)" d="M0 0h17v17H0z"></path></clipPath></defs></svg> Your data is safe with us. We use server-side AES-256 encryption.</p>
          </div>
        </div>
      </div>
      <div className={grid__side}>
        <div className={side__container}>
          <p>Instructions</p>
          <div className={field__inner}>
            <ShareField />
            <TextArea fullWidth value={currentDevice && currentDevice.example} />
          </div>
        </div>
      </div>
    </div>
  )
}

export default AddNewDeviceForm