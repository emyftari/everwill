import { useContext } from 'react'
import ModalContext from '../../context/modal/modalContext'
import FieldContext from '../../context/field/fieldContext'
import NoteContext from '../../context/note/noteContext'

import Field from '../field/Field'
import ShareField from '../field/ShareField'
import TextInput from '../inputs/TextInput'
import TextArea from '../inputs/TextArea'
import FileInput from '../inputs/FileInput'

import {
  field__grid,
  grid__side,
  side__container,
  field__inner
} from './Form.module.css'

const AddNewSiteForm = () => {
  const { openCategory } = useContext(ModalContext)
  const { category } = useContext(FieldContext)
  const { currentNote } = useContext(NoteContext)

  return (
    <div className={field__grid}>
      <div className={grid__side}>
        <div className={side__container}>
          <p>Document Info</p>
          <div className={field__inner}>
            <TextInput fullWidth label="Title" name="title" required value={currentNote && currentNote.title} />
            <FileInput file={currentNote && currentNote.attach} />
            {
              category ? (
                <Field
                  labelOne="Category"
                  labelTwo={category}
                  toggleModal={openCategory}
                  v2
                />
              ) : (
                <Field strong="Choose category" toggleModal={openCategory} />
              )
            }
          </div>
        </div>
      </div>
      <div className={grid__side}>
        <div className={side__container}>
          <p>Instructions</p>
          <div className={field__inner}>
            <ShareField />
            <TextArea fullWidth value={currentNote && currentNote.notes} />
          </div>
        </div>
      </div>
    </div>
  )
}

export default AddNewSiteForm
