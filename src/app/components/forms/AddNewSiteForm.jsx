import { useContext } from 'react'
import ModalContext from '../../context/modal/modalContext'
import FieldContext from '../../context/field/fieldContext'
import SiteContext from '../../context/site/siteContext'

import Field from '../field/Field'
import ShareField from '../field/ShareField'
import TextInput from '../inputs/TextInput'
import TextArea from '../inputs/TextArea'
import FileInput from '../inputs/FileInput'
import PopError from '../popError/PopError'
import DisabledInput from '../inputs/DisabledInput'

import {
  field__grid,
  grid__side,
  side__container,
  field__inner,
  see__last__post
} from './Form.module.css'

const AddNewSiteForm = () => {
  const { openAction, openSite } = useContext(ModalContext)
  const { action, actionError, site, siteLabel, siteError, newSite } = useContext(FieldContext)
  const { currentSite } = useContext(SiteContext)

  return (
    <div className={field__grid}>
      <div className={grid__side}>
        <div className={side__container}>
          <p>Site info</p>
          <div className={field__inner}>
            {site ? (
              <Field
                labelOne={siteLabel}
                labelTwo={site}
                toggleModal={openSite}
                v2
              />
            ) : (
              <Field
                strong="Choose a site"
                toggleModal={openSite}
                error={siteError && <PopError />}
              />
            )}
            {newSite && <TextInput fullWidth label="Site title" name="site_title" required value={currentSite && currentSite.site_title} />}
            {newSite && <TextInput fullWidth label="Site URL" name="site_url" required value={currentSite && currentSite.site_url} />}
            {action ? (
              <Field
                labelOne="What should happen after passing away"
                labelTwo={action}
                toggleModal={openAction}
                v2
              />
            ) : (
              <Field
                strong="What should happen after passing away"
                toggleModal={openAction}
                error={actionError && <PopError />}
              />
            )}
            <TextInput fullWidth label="Profile link" name="profile_link" required value={currentSite && currentSite.profile_link} />
            <TextInput fullWidth label="User name" name="username" required value={currentSite && currentSite.username} />
            <DisabledInput fullWidth />
            <div className={see__last__post}>
              <div>
                <p>Set last post</p>
              </div>
              <button>
                <svg width="17" height="21" xmlns="http://www.w3.org/2000/svg"><g stroke="currentColor" strokeLinejoin="round" strokeWidth="2" fill="none" fillRule="evenodd"><path d="M2.36 18.67H14.6V8.59H2.36zM4.52 6.07a3.96 3.96 0 117.92 0v2.52H4.52V6.07h0z"></path></g></svg>
                Upgrade
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className={grid__side}>
        <div className={side__container}>
          <p>Instructions</p>
          <div className={field__inner}>
            <ShareField />
            <TextArea fullWidth value={currentSite && currentSite.notes} />
            <FileInput file={currentSite && currentSite.attach} />
          </div>
        </div>
      </div>
    </div>
  )
}

export default AddNewSiteForm
