import { container } from './EndContent.module.css'

const EndContent = () => {
  return (
    <div className={container}>
      <span >
        <p >This is the end of your content</p>
      </span>
      <hr />
    </div>
  )
}

export default EndContent