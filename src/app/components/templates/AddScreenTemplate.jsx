import { useContext, useEffect } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useHistory, useLocation } from 'react-router-dom'

import FieldContext from '../../context/field/fieldContext'
import SiteContext from '../../context/site/siteContext'
import NoteContext from '../../context/note/noteContext'
import DeviceContext from '../../context/device/deviceContext'
import MediaContext from '../../context/media/mediaContext'
import ShareContext from '../../context/share/shareContext'
import AlertContext from '../../context/alert/alertContext'

import PageContainer from './PageContainer'

import { BackArrow } from '../../assets/Icons'

import {
  page__main,
  form__inner,
  back__btn__container,
  save__btn__container,
  field__container
} from './AddScreenTemplate.module.css'

const AddScreenTemplate = ({ children, title }) => {
  const { clear, action, site, siteLabel, setActionError, setSiteError, happen_next, setSiteLabel, setActionLabel, category, setCategoryLabel, newSite
  } = useContext(FieldContext)
  const { addSite, currentSite, updateSite, clearCurrent } = useContext(SiteContext)
  const { addNote, currentNote, updateNote, clearCurrentNote } = useContext(NoteContext)
  const { addDevice, currentDevice, updateDevice, clearCurrentDevice, path } = useContext(DeviceContext)
  const { media } = useContext(MediaContext)
  const { deputiesToShareWithIDs } = useContext(ShareContext)
  const { setAlert } = useContext(AlertContext)

  const history = useHistory()
  const location = useLocation()
  const methods = useForm()

  useEffect(() => {
    if (currentSite) {
      setSiteLabel(currentSite.name, currentSite.category)
      setActionLabel({
        title: currentSite.happen_next,
        happen_next: currentSite.happen_next
      })
    }

    if (currentNote) {
      setCategoryLabel(currentNote.category)
    }
    // eslint-disable-next-line
  }, [])

  const handleClick = () => {
    if (location.pathname === '/dashboard/add-new-site') {
      if (!action) setActionError()
      if (!site) setSiteError()
    }
  }

  const onSubmit = async (data) => {
    if (location.pathname === '/dashboard/add-new-site') {
      if (currentSite) {
        await updateSite({
          ...data,
          happen_next,
          site,
          siteLabel,
          id: currentSite.id,
          media,
          deputiesToShareWithIDs
        })
        setAlert('Successfully saved site', 'info')
        clear()
        clearCurrent()
        history.push('/dashboard')
      } else if (newSite) {
        await addSite({ ...data, happen_next, site, siteLabel, media, deputiesToShareWithIDs, type: 'new' })
        setAlert('Successfully saved site', 'info')
        clear()
        history.push('/dashboard')
      } else {
        await addSite({ ...data, happen_next, site, siteLabel, media, deputiesToShareWithIDs, type: 'normal' })
        setAlert('Successfully saved site', 'info')
        clear()
        history.push('/dashboard')
      }
    }
    if (location.pathname === '/dashboard/add-new-note') {
      if (currentNote) {
        await updateNote({ ...data, category, id: currentNote.id, media, deputiesToShareWithIDs })
        setAlert('Successfully saved document', 'info')
        clearCurrentNote()
        clear()
        history.push('/dashboard')
      } else {
        await addNote({ ...data, category, media, deputiesToShareWithIDs })
        setAlert('Successfully saved document', 'info')
        clear()
        history.push('/dashboard')
      }
    }
    if (location.pathname === '/dashboard/add-new-device') {
      if (currentDevice) {
        await updateDevice({ ...data, id: currentDevice.id, deputiesToShareWithIDs })
        setAlert('Successfully saved device', 'info')
        clearCurrentDevice()
        history.push('/dashboard')
      } else {
        if (data.password_type === 'passcode') {
          await addDevice({ ...data, deputiesToShareWithIDs })
          setAlert('Successfully saved device', 'info')
          history.push('/dashboard')
        } else {
          await addDevice({
            ...data,
            password: path,
            deputiesToShareWithIDs
          })
          setAlert('Successfully saved device', 'info')
          history.push('/dashboard')
        }
      }
    }
  }

  const handleBack = () => {
    history.goBack()
    clearCurrent()
    clearCurrentNote()
    clearCurrentDevice()
  }

  return (
    <PageContainer>
      <div className={back__btn__container} onClick={handleBack}>
        <div>
          <BackArrow />
        </div>
      </div>
      <main className={page__main}>
        <h1>{title}</h1>
        <FormProvider {...methods}>
          <form onSubmit={methods.handleSubmit(onSubmit)}>
            <div className={form__inner}>
              <div className={field__container}>{children}</div>
              <div className={save__btn__container}>
                <button onClick={handleClick} type='submit'>Save</button>
              </div>
            </div>
          </form>
        </FormProvider>
      </main>
    </PageContainer>
  )
}

export default AddScreenTemplate
