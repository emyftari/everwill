import { useContext } from 'react'
import ModalContext from '../../context/modal/modalContext'

import clsx from 'clsx'
import {
  modal__open,
  modal__closed,
  modal__overlay,
  modal__content,
  modal__inner,
  modal__inner__content,
  container,
  content,
  back__container
} from './ModalTemplate.module.css'

import { Close, BackArrow } from '../../assets/Icons'

const ModalTemplate = ({ children, open }) => {
  const { closeModal } = useContext(ModalContext)

  return (
    <div
      className={clsx(
        modal__overlay,
        open ? modal__open : modal__closed
      )}
    >
      <div className={modal__content}>
        <div className={modal__inner}>
          <div className={modal__inner__content}>
            <button onClick={closeModal}>
              <Close />
            </button>
            <div className={container}>
              <div className={content}>
                <div className={back__container} onClick={closeModal}>
                  <div>
                    <BackArrow />
                  </div>
                </div>
                {children}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ModalTemplate
