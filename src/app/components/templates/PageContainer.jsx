import {
  page__container,
  page__content,
  page__inner
} from './PageContainer.module.css'

const PageContainer = ({ children }) => {
  return (
    <div className={page__container}>
      <div className={page__content}>
        <div className={page__inner}>{children}</div>
      </div>
    </div>
  )
}

export default PageContainer
