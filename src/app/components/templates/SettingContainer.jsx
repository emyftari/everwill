import {
  setting__container,
  container__title
} from './SettingContainer.module.css'

const SettingContainer = ({ children, title }) => {
  return (
    <div className={setting__container}>
      <p className={container__title}>{title}</p>
      {children}
    </div>
  )
}

export default SettingContainer
