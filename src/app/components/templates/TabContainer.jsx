import { tab__container, tab__title } from './TabContainer.module.css'

const TabContainer = ({ children, title }) => {
  return (
    <div className={tab__container}>
      <p className={tab__title}>{title}</p>
      {children}
    </div>
  )
}

export default TabContainer