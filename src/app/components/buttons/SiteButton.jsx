import { useContext } from 'react'
import ModalContext from '../../context/modal/modalContext'
import FieldContext from '../../context/field/fieldContext'

import { button } from './SiteButton.module.css'

const SiteButton = ({ name, cat, img }) => {
  const { closeModal } = useContext(ModalContext)
  const { setSiteLabel } = useContext(FieldContext)

  const handleClick = () => {
    setSiteLabel(name, cat)
    closeModal()
  }

  return (
    <button className={button} onClick={handleClick}>
      <img src={img} alt="Netflix" />
      <span>
        <span>
          {name}
        </span>
      </span>
    </button>
  )
}

export default SiteButton