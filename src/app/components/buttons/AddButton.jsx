import { Plus } from '../../assets/Icons'
import { button } from './AddButton.module.css'

import { useContext } from 'react'
import ModalContext from '../../context/modal/modalContext'

const AddButton = ({ btnText }) => {
  const { openModal } = useContext(ModalContext)

  return (
    <button type="button" className={button} onClick={() => openModal('Deputy')}>
      <span>
        <Plus />
      </span>
      {btnText}
    </button>
  )
}

export default AddButton
