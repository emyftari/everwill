import { useHistory } from 'react-router-dom'


import {
  showcase__container,
  showcase__header,
  showcase__content,
  showcase__icons,
  showcase__title,
  shocase__description,
  showcase__button
} from './Showcase.module.css'

const Showcase = ({ icon, header, title, description, button, path }) => {
  const history = useHistory()


  return (
    <div className={showcase__container} >
      <div className={showcase__header}>
        <p>{header}</p>
      </div>
      <div className={showcase__content}>
        <div className={showcase__icons}>
          {icon.map((el) => (
            <span key={Math.random()}>{el}</span>
          ))}
        </div>
        <p className={showcase__title}>{title}</p>
        <p className={shocase__description}>{description}</p>
        <button className={showcase__button} onClick={() => history.push(path)}>{button}</button>
      </div>
    </div>
  )
}

export default Showcase
