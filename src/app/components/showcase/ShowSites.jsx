import { Link } from 'react-router-dom'

import AddButton from '../card/AddButton'

import {
  showcase__container,
  showcase__header,
  showsite__content,
} from './Showcase.module.css'

const Showcase = ({ icon, header, path, children, length }) => {

  return (
    <div className={showcase__container}>
      <div className={showcase__header}>
        <p>{header}</p>
        <Link to={path}>See all ({length})</Link>
      </div>
      <div className={showsite__content}>
        {children}
        <AddButton path={path} />
      </div>
    </div>
  )
}

export default Showcase
