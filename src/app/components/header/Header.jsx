import { useState, useContext } from 'react'
import { Link, useHistory } from 'react-router-dom'

import AuthContext from '../../../context/auth/AuthContext'

import Navigation from './Navigation'
import Alerts from '../alerts/Alerts'

import {
  header__container,
  header__content,
  header__rightSide,
  header__res__bar,
  header__bar__right,
  header__link,
} from './Header.module.css'

import { Bell, Logo, Menu } from '../../assets/Icons'

const Header = () => {
  const [openMenu, setOpenMenu] = useState(false)
  const { user } = useContext(AuthContext)
  const history = useHistory()

  return (
    <div className={header__container}>
      <Alerts />
      <div className={header__content}>
        <div className={header__res__bar}>
          <Link to='/dashboard' className={header__link}>
            <Logo />
          </Link>
          <div className={header__bar__right}>
            <button onClick={() => setOpenMenu(!openMenu)}>
              <Menu />
            </button>
            <Link to='/dashboard/profile'>
              <div>
                DJ
              </div>
            </Link>
          </div>
        </div>
        <Navigation open={openMenu} />
        <div className={header__rightSide}>
          <button onClick={() => history.push('/plan')}>Upgrade</button>
          <button onClick={() => history.push('/dashboard/profile')}>
            <span>
              {user.attachment
                ? (
                  <img style={styles} src={user.attachment.file} alt="" />
                )
                : (
                  <div>{user.first_name && user.first_name.charAt(0)}{user.last_name && user.last_name.charAt(0)}</div>
                )}
            </span>
            <label>{user.first_name ? `${user.first_name} ${user.last_name}` : user.email}</label>
          </button>
          <button>
            <Bell />
          </button>
        </div>
      </div>
    </div >
  )
}

const styles = {
  width: '2.25rem',
  height: '2.25rem',
  objectFit: 'cover',
  borderRadius: '50%',
  pointerEvents: 'none'
}

export default Header
