import {
  Home,
  Emblem,
  Note,
  Device,
  Book,
  People
} from '../../assets/Icons'

export const linkDataWithIcons = [
  {
    pathname: '/dashboard',
    name: 'Home',
    icon: <Home />
  },
  {
    pathname: '/dashboard/sites',
    name: 'Sites & Social',
    icon: <Emblem />
  },
  {
    pathname: '/dashboard/document',
    name: 'Docs & Notes',
    icon: <Note />
  },
  {
    pathname: '/dashboard/device',
    name: 'Devices',
    icon: <Device />
  },
  {
    pathname: '/dashboard/directive',
    name: 'Will & Directives',
    icon: <Book />
  },
  {
    pathname: '/dashboard/order',
    name: 'For Loved Ones',
    icon: <People />
  }
]

export const linkDataWithoutIcons = [
  {
    pathname: '/dashboard/deputy',
    name: 'My deputies'
  },
  {
    pathname: '/dashboard/shared',
    name: 'Shared with me'
  },
  {
    pathname: '/dashboard/articles',
    name: 'Articles'
  }
]
