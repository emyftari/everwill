import clsx from 'clsx'

import { NavLink } from 'react-router-dom'
import { linkDataWithIcons, linkDataWithoutIcons } from './linkData'
import {
  nav__container,
  nav__container__open,
  nav__link,
  nav__separator,
  selected
} from './Navigation.module.css'

const Navigation = ({ open }) => {
  return (
    <nav className={clsx(nav__container, open && nav__container__open)}>
      {linkDataWithIcons.map((data) => (
        <NavLink
          exact
          key={data.name}
          to={data.pathname}
          className={nav__link}
          activeClassName={selected}
        >
          {data.icon}
          {data.name}
        </NavLink>
      ))}
      <hr className={nav__separator} />
      {linkDataWithoutIcons.map((data) => (
        <NavLink
          exact
          key={data.name}
          to={data.pathname}
          className={nav__link}
          activeClassName={selected}
        >
          {data.name}
        </NavLink>
      ))}
    </nav>
  )
}

export default Navigation
