import { Link, useHistory } from 'react-router-dom'
import Navigation from '../navigation/Navigation'

import {
  sidebar__container,
  sidebar__content,
  sidebar__logo,
  sidebar__button
} from './Sidebar.module.css'

import { PhoneIcon } from '../../assets/Icons'
import logo from '../../assets/images/logo.png'

const Section = () => {
  const history = useHistory()

  return (
    <div className={sidebar__container}>
      <div className={sidebar__content}>
        <Link to="/" className={sidebar__logo}>
          <img src={logo} alt="" />
        </Link>
        <Navigation />
        <button className={sidebar__button} onClick={() => history.push('/dashboard/vip')}>
          <PhoneIcon />
          Book an expert
        </button>
        <div></div>
      </div>
    </div>
  )
}

export default Section
