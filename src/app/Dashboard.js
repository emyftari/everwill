import { useContext, useEffect } from "react";
import { Switch, Route } from "react-router-dom";
import AuthContext from "../context/auth/AuthContext";

import Sidebar from "./components/sidebar/Sidebar";
import Header from "./components/header/Header";
import Home from "./screens/Home/Home";
import MySites from "./screens/MySites/MySites";
import MyDocsNotes from "./screens/MyDocsNotes/MyDocsNotes";
import Devices from "./screens/MyDevices/Devices";
import Deputy from "./screens/MyDeputy/Deputy";
import WillDirectives from "./screens/WillDirectives/WillDirectives";
import Profile from "./screens/Profile/Profile";
import DeadMan from "./screens/DeadMan/DeadMan";
import DeleteAccount from "./screens/DeleteAccount/DeleteAccount";
import EditDeputy from "./components/deputy/EditDeputy";
import SharedWithMe from "./screens/SharedWithMe/SharedWithMe";
import BookAnExpert from "./screens/BookAnExpert/BookAnExpert";
import ScheduleACall from "./screens/ScheduleACall/ScheduleACall";
import FutureMessages from "./screens/FutureMessages/FutureMessages";
import AddFutureMessage from "./screens/FutureMessages/AddFutureMessage";
import ProfileChecklist from "./components/profileChecklist/ProfileChecklist";
import EndContent from "./components/endContent/EndContent";
import Invite from "./components/invites/Invite";

import AddNewSite from "./screens/MySites/AddNewSite";
import AddNewNote from "./screens/MyDocsNotes/AddNewNote";
import AddNewDevice from "./screens/MyDevices/AddNewDevice";

import ModalState from "./context/modal/ModalState";
import FieldState from "./context/field/FieldState";
import SiteState from "./context/site/SiteState";
import NoteState from "./context/note/NoteState";
import DeviceState from "./context/device/DeviceState";
import MediaState from "./context/media/MediaState";
import DeputyState from "./context/deputy/DeputyState";
import UserState from "./context/user/UserState";
import ShareState from "./context/share/ShareState";
import AlertState from "./context/alert/AlertState";
import FutureMessagesState from "./context/futureMessages/FutureMessagesState";

import ActionModal from "./components/modal/ActionModal";
import SiteModal from "./components/modal/SiteModal";
import CategoryModal from "./components/modal/CategoryModal";
import ProfilePhotoModal from "./components/modal/profileModal/ProfilePhotoModal";
import ChangeNameModal from "./components/modal/profileModal/ChangeNameModal";
import ChangeInformationModal from "./components/modal/profileModal/ChangeInformationModal";
import ChangeEmailModal from "./components/modal/profileModal/ChangeEmailModal";
import ChangeAddressModal from "./components/modal/profileModal/ChangeAddressModal";
import DeputyModal from "./components/modal/profileModal/DeputyModal";
import ShareWithModal from "./components/modal/profileModal/ShareWithModal";
import ShareDirectlyModal from "./components/modal/profileModal/ShareDirectlyModal";
import TwoFaAuthModal from "./components/modal/profileModal/TwoFaAuthModal";
import AddNewCrediCardModal from "./components/modal/profileModal/AddNewCrediCardModal";
import ChangePasswordModal from "./components/modal/profileModal/ChangePasswordModal";

import LoadingScreen from "./components/loading/LoadingScreen";
import DeputyProfile from "./components/deputy/DeputyProfile";

import { container, main } from "./Dashboard.module.css";
import ForLovedOnes from "./screens/ForLovedOnes/ForLovedOnes";

const Dashboard = () => {
  const { user, loadUser } = useContext(AuthContext);

  useEffect(() => {
    loadUser();
    // eslint-disable-next-line
  }, []);

  if (!user) {
    return <LoadingScreen />;
  }

  return (
    <SiteState>
      <NoteState>
        <DeviceState>
          <ModalState>
            <FieldState>
              <MediaState>
                <DeputyState>
                  <UserState>
                    <ShareState>
                      <AlertState>
                        <FutureMessagesState>
                          <div className={container}>
                            <Sidebar />
                            <div className={main}>
                              <Header />
                              <Switch>
                                <Route
                                  exact
                                  path="/dashboard"
                                  component={Home}
                                />
                                <Route
                                  path="/dashboard/sites"
                                  component={MySites}
                                />
                                <Route
                                  path="/dashboard/document"
                                  component={MyDocsNotes}
                                />
                                <Route
                                  path="/dashboard/device"
                                  component={Devices}
                                />
                                <Route
                                  path="/dashboard/deputy"
                                  component={Deputy}
                                />
                                <Route
                                  path="/dashboard/deputy:deputyId"
                                  component={DeputyProfile}
                                />
                                <Route
                                  path="/dashboard/edit-deputy:deputyId"
                                  component={EditDeputy}
                                />
                                <Route
                                  path="/dashboard/directive"
                                  component={WillDirectives}
                                />
                                <Route
                                  path="/dashboard/order"
                                  component={ForLovedOnes}
                                />
                                <Route
                                  path="/dashboard/profile"
                                  component={Profile}
                                />
                                <Route
                                  path="/dashboard/add-new-site"
                                  component={AddNewSite}
                                />
                                <Route
                                  path="/dashboard/add-new-note"
                                  component={AddNewNote}
                                />
                                <Route
                                  path="/dashboard/add-new-device"
                                  component={AddNewDevice}
                                />
                                <Route
                                  path="/dashboard/dead-man-switch"
                                  component={DeadMan}
                                />
                                <Route
                                  path="/dashboard/delete"
                                  component={DeleteAccount}
                                />
                                <Route
                                  path="/dashboard/shared"
                                  component={SharedWithMe}
                                />
                                <Route
                                  path="/dashboard/vip"
                                  component={BookAnExpert}
                                />
                                <Route
                                  path="/dashboard/schedule-a-call"
                                  component={ScheduleACall}
                                />
                                <Route
                                  path="/dashboard/goodbye"
                                  component={FutureMessages}
                                />
                                <Route
                                  path="/dashboard/goodbye-add"
                                  component={AddFutureMessage}
                                />
                              </Switch>
                              <Route
                                exact
                                path="/dashboard"
                                component={EndContent}
                              />
                              <Route
                                exact
                                path="/dashboard"
                                component={Invite}
                              />
                              <Route
                                exact
                                path="/dashboard"
                                component={ProfileChecklist}
                              />
                            </div>
                          </div>
                          <ActionModal />
                          <SiteModal />
                          <CategoryModal />
                          <ProfilePhotoModal />
                          <ChangeAddressModal />
                          <ChangeNameModal />
                          <ChangeInformationModal />
                          <ChangeEmailModal />
                          <ShareWithModal />
                          <ShareDirectlyModal />
                          <DeputyModal />
                          <TwoFaAuthModal />
                          <AddNewCrediCardModal />
                          <ChangePasswordModal />
                        </FutureMessagesState>
                      </AlertState>
                    </ShareState>
                  </UserState>
                </DeputyState>
              </MediaState>
            </FieldState>
          </ModalState>
        </DeviceState>
      </NoteState>
    </SiteState>
  );
};

export default Dashboard;
