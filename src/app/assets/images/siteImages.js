import onepassword from '../images/sites/1password.png'
import andme from '../images/sites/23andme.png'
import aaa from '../images/sites/aaa.png'
import aboutme from '../images/sites/about.me.png'
import adobe from '../images/sites/adobe.png'
import aig from '../images/sites/aig.png'
import alaskaairlines from '../images/sites/alaskaairlines.png'
import alipay from '../images/sites/alipay.png'
import amazon from '../images/sites/amazon.png'
import americanairlines from '../images/sites/americanairline.jpg'
import ancestry from '../images/sites/ancestry.png'
import aol from '../images/sites/aol.png'
import apple from '../images/sites/apple.png'
import appletv from '../images/sites/appletv.png'
import ashleymadison from '../images/sites/ashleymadison.png'
import askfm from '../images/sites/ask.fm.png'
import att from '../images/sites/att.png'
import badoo from '../images/sites/badoo.png'
import baidu from '../images/sites/baidu.png'
import bankofamerica from '../images/sites/bankofamerica.png'
import biance from '../images/sites/biance.png'
import bitstamp from '../images/sites/bitstamp.png'
import bittrex from '../images/sites/bittrex.png'
import booking from '../images/sites/booking.png'
import box from '../images/sites/box.jpg'

import bumble from '../images/sites/bumble.png'
import chase from '../images/sites/chase.png'
import citibank from '../images/sites/citibank.png'
import coinbase from '../images/sites/coinbase.png'
import dailymotion from '../images/sites/dailymotion.png'
import deltaairlines from '../images/sites/deltaairlines.jpg'
import disney from '../images/sites/disney.png'
import docusign from '../images/sites/docusing.jpg'
import domain from '../images/sites/domain.com.png'
import dropbox from '../images/sites/dropbox.png'
import ebay from '../images/sites/ebay.png'
import economist from '../images/sites/economist.png'
import etoro from '../images/sites/etoro.png'
import etrade from '../images/sites/etrade.png'
import etsy from '../images/sites/etsy.png'
import facebook from '../images/sites/facebook.png'
import familysearch from '../images/sites/familysearch.png'
import familytreedna from '../images/sites/familytreedna.png'
import fidelity from '../images/sites/fidelity.png'
import financialtimes from '../images/sites/financialtimes.png'
import freewill from '../images/sites/freewill.png'
import geico from '../images/sites/geico.png'
import godaddy from '../images/sites/godaddy.png'
import google from '../images/sites/google.png'

import hbo from '../images/sites/hbo.png'
import hellosign from '../images/sites/hellosign.png'
import hinge from '../images/sites/hinge.png'
import hulu from '../images/sites/hulu.png'
import instagram from '../images/sites/instagram.png'
import iqiyi from '../images/sites/iqiyi.png'
import ivmu from '../images/sites/ivmu.png'
import kraken from '../images/sites/kraken.png'
import lastfm from '../images/sites/lastfm.png'
import lastpass from '../images/sites/lastpass.png'
import latimes from '../images/sites/latimes.png'
import legalzoom from '../images/sites/legalzoom.jpg'
import lime from '../images/sites/lime.png'
import linkedin from '../images/sites/linkedin.png'
import lyft from '../images/sites/lyft.png'
import matchcom from '../images/sites/match.com.png'
import mcafee from '../images/sites/mcafee.png'
import medium from '../images/sites/medium.png'
import memories from '../images/sites/memories.jpg'
import microsoft from '../images/sites/microsoft.png'
import mint from '../images/sites/mint.png'
import myheritage from '../images/sites/myheritage.png'
import myspace from '../images/sites/myspace.png'
import netflix from '../images/sites/netflix.png'
import newyorktimes from '../images/sites/newyorktimes.png'
import okcupid from '../images/sites/okcupid.png'

import onedrive from '../images/sites/onedrive.jpg'
import paypal from '../images/sites/paypal.png'
import photobucket from '../images/sites/photobucket.png'
import pinterest from '../images/sites/pinterest.png'
// import prime from '../images/sites/prime.png'
import prudential from '../images/sites/prudential.png'
import ps from '../images/sites/ps.png'
import qq from '../images/sites/qq.png'
import reddit from '../images/sites/reddit.png'
import robinhood from '../images/sites/robinhood.png'
import rocketlawyer from '../images/sites/rocketlawyer.png'
import shutterfly from '../images/sites/shutterfly.png'
import signrequest from '../images/sites/signrequest.png'
import skype from '../images/sites/skype.png'
import snapchat from '../images/sites/snapchat.png'
import soundcloud from '../images/sites/soundcloud.png'
import southwestairlines from '../images/sites/southwestairlines.png'
import spotify from '../images/sites/spotify.png'
import sprint from '../images/sites/sprint.png'
import statefarm from '../images/sites/statefarm.png'
import steam from '../images/sites/steam.png'
import target from '../images/sites/target.png'
import tdameritrade from '../images/sites/tdameritrade.png'
import tencent from '../images/sites/tencent.png'
import tiktok from '../images/sites/tiktok.png'
import tinder from '../images/sites/tinder.png'

import tmobile from '../images/sites/tmobile.png'
import trustwill from '../images/sites/trustwill.png'
import tumblr from '../images/sites/tumblr.png'
import turo from '../images/sites/turo.png'
import twitch from '../images/sites/twitch.png'
import twitter from '../images/sites/twitter.png'
import uber from '../images/sites/uber.png'
import unitedairlines from '../images/sites/unitedairlines.jpg'
import verizon from '../images/sites/verizon.png'
import vimeo from '../images/sites/vimeo.png'
import virgin from '../images/sites/virgin.jpg'
import walgreens from '../images/sites/walgreens.png'
import walmart from '../images/sites/walmart.png'
import washington from '../images/sites/washington.png'
import wechat from '../images/sites/wechat.png'
import wellsfargo from '../images/sites/wellsfargo.png'
import wsj from '../images/sites/wsj.png'
import yahoo from '../images/sites/yahoo.png'
import youtube from '../images/sites/youtube.png'
import zoom from '../images/sites/zoom.png'
import zoosk from '../images/sites/zoosk.png'

import {
  Delete,
  Card,
  Download,
  CheckShield,
  CrossArrow
} from '../../../store/assets/Icons'

const actions = {
  delete: {
    type: 'Delete',
    price: '39.99',
    icon: <Delete />,
    desc: 'Delete page to protect your loved ones.'
  },
  extract_funds: {
    type: 'Extract funds',
    price: '99.99',
    icon: <Card />,
    desc: 'Extract funds from page to protect your loved ones.'
  },
  extract_data: {
    type: 'Extract data',
    price: '99.99',
    icon: <Download />,
    desc: 'Extract data from page to protect your loved ones.'
  },
  memorialize: {
    type: 'Memorialize',
    price: '49.99',
    icon: <CheckShield />,
    desc: 'Memorialize page to protect your loved ones.'
  },
  full_access: {
    type: 'Full access',
    price: '99.99',
    icon: <CrossArrow />,
    desc: 'Get full access to page to protect your loved ones.'
  }
}

export const top = [
  {
    id: 1,
    site: 'Netflix',
    category: 'Top',
    img: netflix,
    actions: [actions.delete]
  },
  {
    id: 2,
    site: 'CoinBase',
    category: 'Top',
    img: coinbase,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 3,
    site: 'Twitter',
    category: 'Top',
    img: twitter,
    actions: [actions.delete]
  },
  {
    id: 4,
    site: 'LinkedIn',
    category: 'Top',
    img: linkedin,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 5,
    site: 'Snapchat',
    category: 'Top',
    img: snapchat,
    actions: [actions.delete]
  },
  {
    id: 6,
    site: 'Microsoft',
    category: 'Top',
    img: microsoft,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 7,
    site: 'Spotify',
    category: 'Top',
    img: spotify,
    actions: [actions.delete]
  },
  {
    id: 8,
    site: 'PayPal',
    category: 'Top',
    img: paypal,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 9,
    site: 'Facebook',
    category: 'Top',
    img: facebook,
    actions: [actions.delete, actions.memorialize, actions.extract_data]
  },
  {
    id: 10,
    site: 'Instagram',
    category: 'Top',
    img: instagram,
    actions: [actions.delete, actions.memorialize]
  },
  {
    id: 11,
    site: 'GoDaddy',
    category: 'Top',
    img: godaddy,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 12,
    site: 'Google',
    category: 'Top',
    img: google,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 13,
    site: 'Amazon',
    category: 'Top',
    img: amazon,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 14,
    site: 'Apple',
    category: 'Top',
    img: apple,
    actions: [actions.delete, actions.extract_data]
  }
]

export const social = [
  {
    id: 1,
    site: 'IMVU',
    category: 'Social',
    img: ivmu,
    actions: [actions.delete]
  },
  {
    id: 2,
    site: 'About.me',
    category: 'Social',
    img: aboutme,
    actions: [actions.delete]
  },
  {
    id: 3,
    site: 'Twitter',
    category: 'Social',
    img: twitter,
    actions: [actions.delete]
  },
  {
    id: 4,
    site: 'LinkedIn',
    category: 'Social',
    img: linkedin,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 5,
    site: 'Snapchat',
    category: 'Social',
    img: snapchat,
    actions: [actions.delete]
  },
  {
    id: 6,
    site: 'Ancestry',
    category: 'Social',
    img: ancestry,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 7,
    site: 'MySpace',
    category: 'Social',
    img: myspace,
    actions: [actions.delete, actions.memorialize]
  },
  {
    id: 8,
    site: 'Myheritage',
    category: 'Social',
    img: myheritage,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 9,
    site: 'Pinterest',
    category: 'Social',
    img: pinterest,
    actions: [actions.delete]
  },
  {
    id: 10,
    site: 'Memories',
    category: 'Social',
    img: memories,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 11,
    site: 'Yahoo',
    category: 'Social',
    img: yahoo,
    actions: [actions.delete]
  },
  {
    id: 12,
    site: 'FamilyTreeDNA',
    category: 'Social',
    img: familytreedna,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 13,
    site: 'Aol',
    category: 'Social',
    img: aol,
    actions: [actions.delete]
  },
  {
    id: 14,
    site: '23andMe',
    category: 'Social',
    img: andme,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 15,
    site: 'Reddit',
    category: 'Social',
    img: reddit,
    actions: [actions.delete]
  },
  {
    id: 16,
    site: 'FamilySearch',
    category: 'Social',
    img: familysearch,
    actions: [actions.delete]
  },
  {
    id: 17,
    site: 'Tumblr',
    category: 'Social',
    img: tumblr,
    actions: [actions.delete]
  },
  {
    id: 18,
    site: 'Ask.FM',
    category: 'Social',
    img: askfm,
    actions: [actions.delete]
  },
  {
    id: 19,
    site: 'TikTok',
    category: 'Social',
    img: tiktok,
    actions: [actions.delete]
  },
  {
    id: 20,
    site: 'Facebook',
    category: 'Social',
    img: facebook,
    actions: [actions.delete, actions.memorialize, actions.extract_data]
  },
  {
    id: 21,
    site: 'Instagram',
    category: 'Social',
    img: instagram,
    actions: [actions.delete, actions.memorialize]
  }
]

export const shopping = [
  {
    id: 1,
    site: 'Etsy',
    category: 'Shopping',
    img: etsy,
    actions: [actions.delete]
  },
  {
    id: 2,
    site: 'Ebay',
    category: 'Shopping',
    img: ebay,
    actions: [actions.delete]
  },
  {
    id: 3,
    site: 'Booking.com',
    category: 'Shopping',
    img: booking,
    actions: [actions.delete]
  },
  {
    id: 4,
    site: 'American Airline',
    category: 'Shopping',
    img: americanairlines,
    actions: [actions.extract_funds]
  },
  {
    id: 5,
    site: 'Target',
    category: 'Shopping',
    img: target,
    actions: [actions.delete]
  },
  {
    id: 6,
    site: 'Walgreens',
    category: 'Shopping',
    img: walgreens,
    actions: [actions.delete]
  },
  {
    id: 7,
    site: 'Walmart',
    category: 'Shopping',
    img: walmart,
    actions: [actions.delete]
  },
  {
    id: 8,
    site: 'Uber',
    category: 'Shopping',
    img: uber,
    actions: [actions.delete]
  },
  {
    id: 9,
    site: 'Lyft',
    category: 'Shopping',
    img: lyft,
    actions: [actions.delete]
  },
  {
    id: 10,
    site: 'United Airlines',
    category: 'Shopping',
    img: unitedairlines,
    actions: [actions.extract_funds]
  },
  {
    id: 11,
    site: 'Delta Airlines',
    category: 'Shopping',
    img: deltaairlines,
    actions: [actions.extract_funds]
  },
  {
    id: 12,
    site: 'Alaska Airlines',
    category: 'Shopping',
    img: alaskaairlines,
    actions: [actions.extract_funds]
  },
  {
    id: 13,
    site: 'Southwest Airlines',
    category: 'Shopping',
    img: southwestairlines,
    actions: [actions.extract_funds]
  },
  {
    id: 14,
    site: 'Virgin Airlines',
    category: 'Shopping',
    img: virgin,
    actions: [actions.extract_funds]
  },
  {
    id: 15,
    site: 'Amazon',
    category: 'Shopping',
    img: amazon,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 16,
    site: 'Lime',
    category: 'Shopping',
    img: lime,
    actions: [actions.delete]
  },
  {
    id: 17,
    site: 'Prime',
    category: 'Shopping',
    img: amazon,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 18,
    site: 'Turo',
    category: 'Shopping',
    img: turo,
    actions: [actions.delete]
  }
]

export const payments = [
  {
    id: 1,
    site: 'Robinhood',
    category: 'Payment, Banks And Trading',
    img: robinhood,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 2,
    site: 'eTrade',
    category: 'Payment, Banks And Trading',
    img: etrade,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 3,
    site: 'Chase',
    category: 'Payment, Banks And Trading',
    img: chase,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 4,
    site: 'TD Ameritrade',
    category: 'Payment, Banks And Trading',
    img: tdameritrade,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 5,
    site: 'Bank of America',
    category: 'Payment, Banks And Trading',
    img: bankofamerica,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 6,
    site: 'Fidelity',
    category: 'Payment, Banks And Trading',
    img: fidelity,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 7,
    site: 'Wells Fargo',
    category: 'Payment, Banks And Trading',
    img: wellsfargo,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 8,
    site: 'Citibank',
    category: 'Payment, Banks And Trading',
    img: citibank,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 9,
    site: 'TD Bank',
    category: 'Payment, Banks And Trading',
    img: tdameritrade,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 10,
    site: 'Coinbase',
    category: 'Payment, Banks And Trading',
    img: coinbase,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 11,
    site: 'Paypal',
    category: 'Payment, Banks And Trading',
    img: paypal,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 12,
    site: 'Venmo',
    category: 'Payment, Banks And Trading',
    img: vimeo,
    actions: [actions.delete, actions.extract_funds]
  }
]

export const software = [
  {
    id: 1,
    site: 'DocuSing',
    category: 'Software And Publishing',
    img: docusign,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 2,
    site: 'Microsoft',
    category: 'Software And Publishing',
    img: microsoft,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 3,
    site: 'HelloSign',
    category: 'Software And Publishing',
    img: hellosign,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 4,
    site: 'Zoom',
    category: 'Software And Publishing',
    img: zoom,
    actions: [actions.delete]
  },
  {
    id: 5,
    site: 'SignRequest',
    category: 'Software And Publishing',
    img: signrequest,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 6,
    site: 'Adobe',
    category: 'Software And Publishing',
    img: adobe,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 7,
    site: 'DropBox',
    category: 'Software And Publishing',
    img: dropbox,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 8,
    site: 'Skype',
    category: 'Software And Publishing',
    img: skype,
    actions: [actions.delete]
  },
  {
    id: 9,
    site: 'Box',
    category: 'Software And Publishing',
    img: box,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 10,
    site: 'McAfee',
    category: 'Software And Publishing',
    img: mcafee,
    actions: [actions.delete]
  },
  {
    id: 11,
    site: 'OneDrive',
    category: 'Software And Publishing',
    img: onedrive,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 12,
    site: 'Lastpass',
    category: 'Software And Publishing',
    img: lastpass,
    actions: [actions.delete]
  },
  {
    id: 13,
    site: 'Rocketlawyer',
    category: 'Software And Publishing',
    img: rocketlawyer,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 14,
    site: '1password',
    category: 'Software And Publishing',
    img: onepassword,
    actions: [actions.delete]
  },
  {
    id: 15,
    site: 'LegalZoom',
    category: 'Software And Publishing',
    img: legalzoom,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 16,
    site: 'Mint',
    category: 'Software And Publishing',
    img: mint,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 17,
    site: 'Trust&Will',
    category: 'Software And Publishing',
    img: trustwill,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 18,
    site: 'domain.com',
    category: 'Software And Publishing',
    img: domain,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 19,
    site: 'FreeWill',
    category: 'Software And Publishing',
    img: freewill,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 20,
    site: 'Medium',
    category: 'Software And Publishing',
    img: medium,
    actions: [actions.delete]
  },
  {
    id: 21,
    site: 'GoDaddy',
    category: 'Software And Publishing',
    img: godaddy,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 22,
    site: 'Google',
    category: 'Software And Publishing',
    img: google,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 23,
    site: 'Apple',
    category: 'Software And Publishing',
    img: apple,
    actions: [actions.delete, actions.extract_data]
  }
]

export const telecom = [
  {
    id: 1,
    site: 'AT&T',
    category: 'Telecom',
    img: att,
    actions: [actions.delete]
  },
  {
    id: 2,
    site: 'Verizon',
    category: 'Telecom',
    img: verizon,
    actions: [actions.delete]
  },
  {
    id: 3,
    site: 'T-Mobile',
    category: 'Telecom',
    img: tmobile,
    actions: [actions.delete]
  },
  {
    id: 4,
    site: 'Sprint',
    category: 'Telecom',
    img: sprint,
    actions: [actions.delete]
  }
]

export const dating = [
  {
    id: 1,
    site: 'Bumble',
    category: 'Dating',
    img: bumble,
    actions: [actions.delete]
  },
  {
    id: 2,
    site: 'Badoo',
    category: 'Dating',
    img: badoo,
    actions: [actions.delete]
  },
  {
    id: 3,
    site: 'Ashley Madison',
    category: 'Dating',
    img: ashleymadison,
    actions: [actions.delete]
  },
  {
    id: 4,
    site: 'Match.com',
    category: 'Dating',
    img: matchcom,
    actions: [actions.delete]
  },
  {
    id: 5,
    site: 'OKCupid',
    category: 'Dating',
    img: okcupid,
    actions: [actions.delete]
  },
  {
    id: 6,
    site: 'Zoosk',
    category: 'Dating',
    img: zoosk,
    actions: [actions.delete]
  },
  {
    id: 7,
    site: 'Tinder',
    category: 'Dating',
    img: tinder,
    actions: [actions.delete]
  },
  {
    id: 8,
    site: 'Hinge',
    category: 'Dating',
    img: hinge,
    actions: [actions.delete]
  }
]

export const media = [
  {
    id: 1,
    site: 'Youtube',
    category: 'Photo & Video',
    img: youtube,
    actions: [actions.delete, actions.extract_data, actions.extract_funds]
  },
  {
    id: 2,
    site: 'Photo Bucket',
    category: 'Photo & Video',
    img: photobucket,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 3,
    site: 'Twitch',
    category: 'Photo & Video',
    img: twitch,
    actions: [actions.delete]
  },
  {
    id: 4,
    site: 'Shutterfly',
    category: 'Photo & Video',
    img: shutterfly,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 5,
    site: 'Vimeo',
    category: 'Photo & Video',
    img: vimeo,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 6,
    site: 'DailyMotion',
    category: 'Photo & Video',
    img: dailymotion,
    actions: [actions.delete]
  },
  {
    id: 7,
    site: 'Google',
    category: 'Photo & Video',
    img: google,
    actions: [actions.delete, actions.full_access]
  }
]

export const crypto = [
  {
    id: 1,
    site: 'Bittrex',
    category: 'Crypto',
    img: bittrex,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 2,
    site: 'Coinbase',
    category: 'Crypto',
    img: coinbase,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 3,
    site: 'Bitstamp',
    category: 'Crypto',
    img: bitstamp,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 4,
    site: 'Kraken',
    category: 'Crypto',
    img: kraken,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 5,
    site: 'Etoro',
    category: 'Crypto',
    img: etoro,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 6,
    site: 'Biance',
    category: 'Crypto',
    img: biance,
    actions: [actions.delete, actions.extract_funds]
  }
]

export const china = [
  {
    id: 1,
    site: 'Alipay',
    category: 'China',
    img: alipay,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 2,
    site: 'QQ',
    category: 'China',
    img: qq,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 3,
    site: 'Baidu',
    category: 'China',
    img: baidu,
    actions: [actions.delete]
  },
  {
    id: 4,
    site: 'iQiyi',
    category: 'China',
    img: iqiyi,
    actions: [actions.delete]
  },
  {
    id: 5,
    site: 'Tencent',
    category: 'China',
    img: tencent,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 6,
    site: 'Wechat',
    category: 'China',
    img: wechat,
    actions: [actions.delete, actions.extract_funds, actions.extract_data]
  }
]

export const music = [
  {
    id: 1,
    site: 'The Economist',
    category: 'Music, News And Gaming',
    img: economist,
    actions: [actions.delete]
  },
  {
    id: 2,
    site: 'PlayStation',
    category: 'Music, News And Gaming',
    img: ps,
    actions: [actions.delete]
  },
  {
    id: 3,
    site: 'Steam',
    category: 'Music, News And Gaming',
    img: steam,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 4,
    site: 'Spotify',
    category: 'Music, News And Gaming',
    img: spotify,
    actions: [actions.delete]
  },
  {
    id: 5,
    site: 'SoundCloud',
    category: 'Music, News And Gaming',
    img: soundcloud,
    actions: [actions.delete]
  },
  {
    id: 6,
    site: 'Last.FM',
    category: 'Music, News And Gaming',
    img: lastfm,
    actions: [actions.delete]
  },
  {
    id: 7,
    site: 'Financial Times',
    category: 'Music, News And Gaming',
    img: financialtimes,
    actions: [actions.delete]
  },
  {
    id: 8,
    site: 'The New York Times',
    category: 'Music, News And Gaming',
    img: newyorktimes,
    actions: [actions.delete]
  },
  {
    id: 9,
    site: 'Los Angeles Times',
    category: 'Music, News And Gaming',
    img: latimes,
    actions: [actions.delete]
  },
  {
    id: 10,
    site: 'The Wall Street Journal',
    category: 'Music, News And Gaming',
    img: wsj,
    actions: [actions.delete]
  },
  {
    id: 11,
    site: 'The Washington Post',
    category: 'Music, News And Gaming',
    img: washington,
    actions: [actions.delete]
  }
]

export const tv = [
  {
    id: 1,
    site: 'Netflix',
    category: 'TV',
    img: netflix,
    actions: [actions.delete]
  },
  {
    id: 2,
    site: 'HBO Max',
    category: 'TV',
    img: hbo,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 3,
    site: 'Disney+',
    category: 'TV',
    img: disney,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 4,
    site: 'Hulu',
    category: 'TV',
    img: hulu,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 5,
    site: 'Apple Tv',
    category: 'TV',
    img: appletv,
    actions: [actions.delete]
  }
]

export const insurance = [
  {
    id: 1,
    site: 'Geico',
    category: 'Insurance',
    img: geico,
    actions: [actions.delete]
  },
  {
    id: 2,
    site: 'StateFarm',
    category: 'Insurance',
    img: statefarm,
    actions: [actions.delete]
  },
  {
    id: 3,
    site: 'AAA',
    category: 'Insurance',
    img: aaa,
    actions: [actions.delete]
  },
  {
    id: 4,
    site: 'Prudential Financial',
    category: 'Insurance',
    img: prudential,
    actions: [actions.delete]
  },
  {
    id: 5,
    site: 'AIG',
    category: 'Insurance',
    img: aig,
    actions: [actions.delete]
  }
]

export const dataSites = [
  {
    id: 1,
    site: 'Netflix',
    category: 'Top',
    img: netflix,
    actions: [actions.delete]
  },
  {
    id: 2,
    site: 'CoinBase',
    category: 'Top',
    img: coinbase,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 3,
    site: 'Twitter',
    category: 'Top',
    img: twitter,
    actions: [actions.delete]
  },
  {
    id: 4,
    site: 'LinkedIn',
    category: 'Top',
    img: linkedin,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 5,
    site: 'Snapchat',
    category: 'Top',
    img: snapchat,
    actions: [actions.delete]
  },
  {
    id: 6,
    site: 'Microsoft',
    category: 'Top',
    img: microsoft,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 7,
    site: 'Spotify',
    category: 'Top',
    img: spotify,
    actions: [actions.delete]
  },
  {
    id: 8,
    site: 'PayPal',
    category: 'Top',
    img: paypal,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 9,
    site: 'Facebook',
    category: 'Top',
    img: facebook,
    actions: [actions.delete, actions.memorialize, actions.extract_data]
  },
  {
    id: 10,
    site: 'Instagram',
    category: 'Top',
    img: instagram,
    actions: [actions.delete, actions.memorialize]
  },
  {
    id: 11,
    site: 'GoDaddy',
    category: 'Top',
    img: godaddy,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 12,
    site: 'Google',
    category: 'Top',
    img: google,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 13,
    site: 'Amazon',
    category: 'Top',
    img: amazon,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 14,
    site: 'Apple',
    category: 'Top',
    img: apple,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 15,
    site: 'IMVU',
    category: 'Social',
    img: ivmu,
    actions: [actions.delete]
  },
  {
    id: 16,
    site: 'About.me',
    category: 'Social',
    img: aboutme,
    actions: [actions.delete]
  },
  {
    id: 17,
    site: 'Twitter',
    category: 'Social',
    img: twitter,
    actions: [actions.delete]
  },
  {
    id: 18,
    site: 'LinkedIn',
    category: 'Social',
    img: linkedin,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 19,
    site: 'Snapchat',
    category: 'Social',
    img: snapchat,
    actions: [actions.delete]
  },
  {
    id: 20,
    site: 'Ancestry',
    category: 'Social',
    img: ancestry,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 21,
    site: 'MySpace',
    category: 'Social',
    img: myspace,
    actions: [actions.delete, actions.memorialize]
  },
  {
    id: 22,
    site: 'Myheritage',
    category: 'Social',
    img: myheritage,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 23,
    site: 'Pinterest',
    category: 'Social',
    img: pinterest,
    actions: [actions.delete]
  },
  {
    id: 24,
    site: 'Memories',
    category: 'Social',
    img: memories,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 25,
    site: 'Yahoo',
    category: 'Social',
    img: yahoo,
    actions: [actions.delete]
  },
  {
    id: 26,
    site: 'FamilyTreeDNA',
    category: 'Social',
    img: familytreedna,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 27,
    site: 'Aol',
    category: 'Social',
    img: aol,
    actions: [actions.delete]
  },
  {
    id: 28,
    site: '23andMe',
    category: 'Social',
    img: andme,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 29,
    site: 'Reddit',
    category: 'Social',
    img: reddit,
    actions: [actions.delete]
  },
  {
    id: 30,
    site: 'FamilySearch',
    category: 'Social',
    img: familysearch,
    actions: [actions.delete]
  },
  {
    id: 31,
    site: 'Tumblr',
    category: 'Social',
    img: tumblr,
    actions: [actions.delete]
  },
  {
    id: 32,
    site: 'Ask.FM',
    category: 'Social',
    img: askfm,
    actions: [actions.delete]
  },
  {
    id: 33,
    site: 'TikTok',
    category: 'Social',
    img: tiktok,
    actions: [actions.delete]
  },
  {
    id: 34,
    site: 'Facebook',
    category: 'Social',
    img: facebook,
    actions: [actions.delete, actions.memorialize, actions.extract_data]
  },
  {
    id: 35,
    site: 'Instagram',
    category: 'Social',
    img: instagram,
    actions: [actions.delete, actions.memorialize]
  },
  {
    id: 36,
    site: 'Etsy',
    category: 'Shopping',
    img: etsy,
    actions: [actions.delete]
  },
  {
    id: 37,
    site: 'Ebay',
    category: 'Shopping',
    img: ebay,
    actions: [actions.delete]
  },
  {
    id: 38,
    site: 'Booking.com',
    category: 'Shopping',
    img: booking,
    actions: [actions.delete]
  },
  {
    id: 39,
    site: 'American Airline',
    category: 'Shopping',
    img: americanairlines,
    actions: [actions.extract_funds]
  },
  {
    id: 40,
    site: 'Target',
    category: 'Shopping',
    img: target,
    actions: [actions.delete]
  },
  {
    id: 41,
    site: 'Walgreens',
    category: 'Shopping',
    img: walgreens,
    actions: [actions.delete]
  },
  {
    id: 42,
    site: 'Walmart',
    category: 'Shopping',
    img: walmart,
    actions: [actions.delete]
  },
  {
    id: 43,
    site: 'Uber',
    category: 'Shopping',
    img: uber,
    actions: [actions.delete]
  },
  {
    id: 44,
    site: 'Lyft',
    category: 'Shopping',
    img: lyft,
    actions: [actions.delete]
  },
  {
    id: 45,
    site: 'United Airlines',
    category: 'Shopping',
    img: unitedairlines,
    actions: [actions.extract_funds]
  },
  {
    id: 46,
    site: 'Delta Airlines',
    category: 'Shopping',
    img: deltaairlines,
    actions: [actions.extract_funds]
  },
  {
    id: 47,
    site: 'Alaska Airlines',
    category: 'Shopping',
    img: alaskaairlines,
    actions: [actions.extract_funds]
  },
  {
    id: 48,
    site: 'Southwest Airlines',
    category: 'Shopping',
    img: southwestairlines,
    actions: [actions.extract_funds]
  },
  {
    id: 49,
    site: 'Virgin Airlines',
    category: 'Shopping',
    img: virgin,
    actions: [actions.extract_funds]
  },
  {
    id: 50,
    site: 'Amazon',
    category: 'Shopping',
    img: amazon,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 51,
    site: 'Lime',
    category: 'Shopping',
    img: lime,
    actions: [actions.delete]
  },
  {
    id: 52,
    site: 'Prime',
    category: 'Shopping',
    img: amazon,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 53,
    site: 'Turo',
    category: 'Shopping',
    img: turo,
    actions: [actions.delete]
  },
  {
    id: 54,
    site: 'Robinhood',
    category: 'Payment, Banks And Trading',
    img: robinhood,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 55,
    site: 'eTrade',
    category: 'Payment, Banks And Trading',
    img: etrade,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 56,
    site: 'Chase',
    category: 'Payment, Banks And Trading',
    img: chase,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 57,
    site: 'TD Ameritrade',
    category: 'Payment, Banks And Trading',
    img: tdameritrade,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 58,
    site: 'Bank of America',
    category: 'Payment, Banks And Trading',
    img: bankofamerica,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 59,
    site: 'Fidelity',
    category: 'Payment, Banks And Trading',
    img: fidelity,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 60,
    site: 'Wells Fargo',
    category: 'Payment, Banks And Trading',
    img: wellsfargo,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 61,
    site: 'Citibank',
    category: 'Payment, Banks And Trading',
    img: citibank,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 62,
    site: 'TD Bank',
    category: 'Payment, Banks And Trading',
    img: tdameritrade,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 63,
    site: 'Coinbase',
    category: 'Payment, Banks And Trading',
    img: coinbase,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 64,
    site: 'Paypal',
    category: 'Payment, Banks And Trading',
    img: paypal,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 65,
    site: 'Venmo',
    category: 'Payment, Banks And Trading',
    img: vimeo,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 66,
    site: 'DocuSing',
    category: 'Software And Publishing',
    img: docusign,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 67,
    site: 'Microsoft',
    category: 'Software And Publishing',
    img: microsoft,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 68,
    site: 'HelloSign',
    category: 'Software And Publishing',
    img: hellosign,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 69,
    site: 'Zoom',
    category: 'Software And Publishing',
    img: zoom,
    actions: [actions.delete]
  },
  {
    id: 70,
    site: 'SignRequest',
    category: 'Software And Publishing',
    img: signrequest,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 71,
    site: 'Adobe',
    category: 'Software And Publishing',
    img: adobe,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 72,
    site: 'DropBox',
    category: 'Software And Publishing',
    img: dropbox,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 73,
    site: 'Skype',
    category: 'Software And Publishing',
    img: skype,
    actions: [actions.delete]
  },
  {
    id: 74,
    site: 'Box',
    category: 'Software And Publishing',
    img: box,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 75,
    site: 'McAfee',
    category: 'Software And Publishing',
    img: mcafee,
    actions: [actions.delete]
  },
  {
    id: 76,
    site: 'OneDrive',
    category: 'Software And Publishing',
    img: onedrive,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 77,
    site: 'Lastpass',
    category: 'Software And Publishing',
    img: lastpass,
    actions: [actions.delete]
  },
  {
    id: 78,
    site: 'Rocketlawyer',
    category: 'Software And Publishing',
    img: rocketlawyer,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 79,
    site: '1password',
    category: 'Software And Publishing',
    img: onepassword,
    actions: [actions.delete]
  },
  {
    id: 80,
    site: 'LegalZoom',
    category: 'Software And Publishing',
    img: legalzoom,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 81,
    site: 'Mint',
    category: 'Software And Publishing',
    img: mint,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 82,
    site: 'Trust&Will',
    category: 'Software And Publishing',
    img: trustwill,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 83,
    site: 'domain.com',
    category: 'Software And Publishing',
    img: domain,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 84,
    site: 'FreeWill',
    category: 'Software And Publishing',
    img: freewill,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 85,
    site: 'Medium',
    category: 'Software And Publishing',
    img: medium,
    actions: [actions.delete]
  },
  {
    id: 86,
    site: 'GoDaddy',
    category: 'Software And Publishing',
    img: godaddy,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 87,
    site: 'Google',
    category: 'Software And Publishing',
    img: google,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 88,
    site: 'Apple',
    category: 'Software And Publishing',
    img: apple,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 89,
    site: 'AT&T',
    category: 'Telecom',
    img: att,
    actions: [actions.delete]
  },
  {
    id: 90,
    site: 'Verizon',
    category: 'Telecom',
    img: verizon,
    actions: [actions.delete]
  },
  {
    id: 91,
    site: 'T-Mobile',
    category: 'Telecom',
    img: tmobile,
    actions: [actions.delete]
  },
  {
    id: 92,
    site: 'Sprint',
    category: 'Telecom',
    img: sprint,
    actions: [actions.delete]
  },
  {
    id: 93,
    site: 'Bumble',
    category: 'Dating',
    img: bumble,
    actions: [actions.delete]
  },
  {
    id: 94,
    site: 'Badoo',
    category: 'Dating',
    img: badoo,
    actions: [actions.delete]
  },
  {
    id: 95,
    site: 'Ashley Madison',
    category: 'Dating',
    img: ashleymadison,
    actions: [actions.delete]
  },
  {
    id: 96,
    site: 'Match.com',
    category: 'Dating',
    img: matchcom,
    actions: [actions.delete]
  },
  {
    id: 97,
    site: 'OKCupid',
    category: 'Dating',
    img: okcupid,
    actions: [actions.delete]
  },
  {
    id: 98,
    site: 'Zoosk',
    category: 'Dating',
    img: zoosk,
    actions: [actions.delete]
  },
  {
    id: 99,
    site: 'Tinder',
    category: 'Dating',
    img: tinder,
    actions: [actions.delete]
  },
  {
    id: 100,
    site: 'Hinge',
    category: 'Dating',
    img: hinge,
    actions: [actions.delete]
  },
  {
    id: 101,
    site: 'Youtube',
    category: 'Photo & Video',
    img: youtube,
    actions: [actions.delete, actions.extract_data, actions.extract_funds]
  },
  {
    id: 102,
    site: 'Photo Bucket',
    category: 'Photo & Video',
    img: photobucket,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 103,
    site: 'Twitch',
    category: 'Photo & Video',
    img: twitch,
    actions: [actions.delete]
  },
  {
    id: 104,
    site: 'Shutterfly',
    category: 'Photo & Video',
    img: shutterfly,
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 105,
    site: 'Vimeo',
    category: 'Photo & Video',
    img: vimeo,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 106,
    site: 'DailyMotion',
    category: 'Photo & Video',
    img: dailymotion,
    actions: [actions.delete]
  },
  {
    id: 107,
    site: 'Google',
    category: 'Photo & Video',
    img: google,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 108,
    site: 'Bittrex',
    category: 'Crypto',
    img: bittrex,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 109,
    site: 'Coinbase',
    category: 'Crypto',
    img: coinbase,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 110,
    site: 'Bitstamp',
    category: 'Crypto',
    img: bitstamp,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 111,
    site: 'Kraken',
    category: 'Crypto',
    img: kraken,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 112,
    site: 'Etoro',
    category: 'Crypto',
    img: etoro,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 113,
    site: 'Biance',
    category: 'Crypto',
    img: biance,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 114,
    site: 'Alipay',
    category: 'China',
    img: alipay,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 115,
    site: 'QQ',
    category: 'China',
    img: qq,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 116,
    site: 'Baidu',
    category: 'China',
    img: baidu,
    actions: [actions.delete]
  },
  {
    id: 117,
    site: 'iQiyi',
    category: 'China',
    img: iqiyi,
    actions: [actions.delete]
  },
  {
    id: 118,
    site: 'Tencent',
    category: 'China',
    img: tencent,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 119,
    site: 'Wechat',
    category: 'China',
    img: wechat,
    actions: [actions.delete, actions.extract_funds, actions.extract_data]
  },
  {
    id: 120,
    site: 'The Economist',
    category: 'Music, News And Gaming',
    img: economist,
    actions: [actions.delete]
  },
  {
    id: 121,
    site: 'PlayStation',
    category: 'Music, News And Gaming',
    img: ps,
    actions: [actions.delete]
  },
  {
    id: 122,
    site: 'Steam',
    category: 'Music, News And Gaming',
    img: steam,
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 123,
    site: 'Spotify',
    category: 'Music, News And Gaming',
    img: spotify,
    actions: [actions.delete]
  },
  {
    id: 124,
    site: 'SoundCloud',
    category: 'Music, News And Gaming',
    img: soundcloud,
    actions: [actions.delete]
  },
  {
    id: 125,
    site: 'Last.FM',
    category: 'Music, News And Gaming',
    img: lastfm,
    actions: [actions.delete]
  },
  {
    id: 126,
    site: 'Financial Times',
    category: 'Music, News And Gaming',
    img: financialtimes,
    actions: [actions.delete]
  },
  {
    id: 127,
    site: 'The New York Times',
    category: 'Music, News And Gaming',
    img: newyorktimes,
    actions: [actions.delete]
  },
  {
    id: 128,
    site: 'Los Angeles Times',
    category: 'Music, News And Gaming',
    img: latimes,
    actions: [actions.delete]
  },
  {
    id: 129,
    site: 'The Wall Street Journal',
    category: 'Music, News And Gaming',
    img: wsj,
    actions: [actions.delete]
  },
  {
    id: 130,
    site: 'The Washington Post',
    category: 'Music, News And Gaming',
    img: washington,
    actions: [actions.delete]
  },
  {
    id: 131,
    site: 'Netflix',
    category: 'TV',
    img: netflix,
    actions: [actions.delete]
  },
  {
    id: 132,
    site: 'HBO Max',
    category: 'TV',
    img: hbo,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 133,
    site: 'Disney+',
    category: 'TV',
    img: disney,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 134,
    site: 'Hulu',
    category: 'TV',
    img: hulu,
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 135,
    site: 'Apple Tv',
    category: 'TV',
    img: appletv,
    actions: [actions.delete]
  },
  {
    id: 136,
    site: 'Geico',
    category: 'Insurance',
    img: geico,
    actions: [actions.delete]
  },
  {
    id: 137,
    site: 'StateFarm',
    category: 'Insurance',
    img: statefarm,
    actions: [actions.delete]
  },
  {
    id: 138,
    site: 'AAA',
    category: 'Insurance',
    img: aaa,
    actions: [actions.delete]
  },
  {
    id: 139,
    site: 'Prudential Financial',
    category: 'Insurance',
    img: prudential,
    actions: [actions.delete]
  },
  {
    id: 140,
    site: 'AIG',
    category: 'Insurance',
    img: aig,
    actions: [actions.delete]
  }
]