import { useState, useContext, useEffect } from 'react'

import AuthContext from '../../../context/auth/AuthContext'
import UserContext from '../../context/user/userContext'

import PageContainer from '../../components/templates/PageContainer'
import General from '../../components/profileSections/General'
import Plan from '../../components/profileSections/Plan'
import Billing from '../../components/profileSections/Biling'
import Security from '../../components/profileSections/Security'
import Death from '../../components/profileSections/Death'


// import AddNewCreditCard from '../../components/modal/profileModal/AddNewCrediCardModal'


import { ArrowOut } from '../../assets/Icons'

import {
  header,
  tab__container,
  tab__list,
  tab__element,
  desktop__only,
  active
} from './Profile.module.css'

const Profile = () => {
  const { logout } = useContext(AuthContext)
  const { getDeadManSwitch } = useContext(UserContext)

  const [showGeneral, setShowGeneral] = useState(true)
  const [showPlan, setShowPlan] = useState(false)
  const [showBilling, setShowBilling] = useState(false)
  const [showSecurity, setShowSecurity] = useState(false)
  const [showDeath, setShowDeath] = useState(false)

  const handleClick = (tab) => {
    switch (tab) {
      case 'general':
        setShowGeneral(true)
        setShowPlan(false)
        setShowBilling(false)
        setShowSecurity(false)
        setShowDeath(false)
        break
      case 'plan':
        setShowGeneral(false)
        setShowPlan(true)
        setShowBilling(false)
        setShowSecurity(false)
        setShowDeath(false)
        break
      case 'billing':
        setShowGeneral(false)
        setShowPlan(false)
        setShowBilling(true)
        setShowSecurity(false)
        setShowDeath(false)
        break
      case 'security':
        setShowGeneral(false)
        setShowPlan(false)
        setShowBilling(false)
        setShowSecurity(true)
        setShowDeath(false)
        break
      case 'death':
        setShowGeneral(false)
        setShowPlan(false)
        setShowBilling(false)
        setShowSecurity(false)
        setShowDeath(true)
        break
      default:
        break
    }
  }

  useEffect(() => {
    getDeadManSwitch()
    // eslint-disable-next-line
  }, [])

  return (
    <PageContainer>
      <main>
        <div className={header}>
          <h1>My account.</h1>
          <button type="button" onClick={() => logout()}>
            <span>
              <ArrowOut />
            </span>
            <label>Log out</label>
          </button>
        </div>
        <div className={tab__container}>
          <div className={tab__list}>
            <div
              className={showGeneral ? active : tab__element}
              onClick={() => handleClick('general')}
            >
              <p>General</p>
            </div>
            <div
              className={showPlan ? active : tab__element}
              onClick={() => handleClick('plan')}
            >
              <p>Plan</p>
            </div>
            <div
              className={showBilling ? active : tab__element}
              onClick={() => handleClick('billing')}
            >
              <p>Billing</p>
            </div>
            <div
              className={showSecurity ? active : tab__element}
              onClick={() => handleClick('security')}
            >
              <p>Security</p>
            </div>
            <div
              className={showDeath ? active : tab__element}
              onClick={() => handleClick('death')}
            >
              <p>Death verification</p>
            </div>
            <button type="button" className={desktop__only} onClick={() => logout()}>
              <span>
                <ArrowOut />
              </span>
              <label>Log out</label>
            </button>
          </div>
          <section>
            {showGeneral && <General />}
            {showPlan && <Plan />}
            {showBilling && <Billing />}
            {showSecurity && <Security />}
            {showDeath && <Death />}
          </section>
        </div>
      </main>
    </PageContainer>
  )
}

export default Profile
