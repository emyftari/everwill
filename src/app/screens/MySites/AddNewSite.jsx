import AddScreenTemplate from '../../components/templates/AddScreenTemplate'
import AddNewSiteForm from '../../components/forms/AddNewSiteForm'

const AddNewSite = () => {
  return (
    <AddScreenTemplate title='Add new site.'>
      <AddNewSiteForm />
    </AddScreenTemplate>
  )
}

export default AddNewSite
