import { useContext } from 'react'

import SiteContext from '../../context/site/siteContext'

import PageTemplate from '../../components/pageTemplate/PageTemplate'
import SiteCard from '../../components/card/SiteCard'

import { data } from '../data'

const MySites = () => {
  const { sites } = useContext(SiteContext)
  return (
    <PageTemplate
      page="Sites"
      title="Protect your sites."
      desc="Save your sites, assign a deputy and set what should happen with your legacy when your day comes."
      buttonName='Add site'
      path='/dashboard/add-new-site'
      data={data.sitesSocial}
      cards={sites.map(site => <SiteCard key={site.id}  {...site} />)}
    />
  )
}

export default MySites
