import { InlineWidget } from "react-calendly"

const ScheduleACall = () => {
  return (
    <div>
      <InlineWidget url="https://calendly.com/jrdavyjones/goodtrust-expert-call?month=2021-06" />
    </div>
  )
}

export default ScheduleACall