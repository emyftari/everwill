import { useState, useContext, useEffect } from "react"
import { Link, useHistory } from "react-router-dom"
import { useForm, FormProvider } from "react-hook-form"

import FutureMessageContext from "../../context/futureMessages/futureMessagesContext"
import MediaContext from '../../context/media/mediaContext'

import PageContainer from "../../components/templates/PageContainer"
import TextInput from "../../components/inputs/TextInput"
import TimingSelect from "../../components/inputs/TimingSelect"
import FileInput from "../../components/inputs/FileInput"
import DateInput from "../../../pages/customInputs/DateInput"
import TextAreaToolbar from "../../components/inputs/TextAreaToolbar"

import {
  header__title,
  form__container,
  form__content,
  form__inner,
  section,
  section__title,
  section__content,
  desc,
  button__container
} from "./AddFutureMessage.module.css"

const AddFutureMessage = () => {
  const [value, setValue] = useState("")
  const [timing, setTiming] = useState('death_reported')
  const methods = useForm()
  const history = useHistory()

  const { addFutureMessage, updateFutureMessage, current, clearCurrent } = useContext(FutureMessageContext)
  const { media } = useContext(MediaContext)

  useEffect(() => {
    if (current) {
      setTiming(current.timing)
      setValue(current.message)
    }
    // eslint-disable-next-line
  }, [])

  const onSubmit = async (data) => {
    if (current) {
      await updateFutureMessage({ ...data, id: current.id, media, date: data.birthday, message: value })
      clearCurrent()
      history.push('/dashboard/goodbye')
    } else if (media === null) {
      await addFutureMessage({ ...data, date: data.birthday, message: value })
      history.push('/dashboard/goodbye')
    } else {
      await addFutureMessage({ ...data, media, date: data.birthday, message: value })
      history.push('/dashboard/goodbye')
    }

  }

  return (
    <PageContainer>
      <div></div>
      <main>
        <h1 className={header__title}>{current ? 'Edit New Note' : 'Create a future message.'}</h1>
        <FormProvider {...methods}>
          <form onSubmit={methods.handleSubmit(onSubmit)}>
            <div className={form__container}>
              <div className={form__content}>
                <div className={form__inner}>
                  <div className={section}>
                    <p className={section__title}>Message recipient</p>
                    <div className={section__content}>
                      <TextInput
                        label="First name"
                        name="first_name"
                        required
                        value={current && current.first_name}
                      />
                      <TextInput label="Last name" name="last_name" required value={current && current.last_name} />
                      <TextInput
                        label="Email address"
                        name="email"
                        required
                        fullWidth
                        value={current && current.email}
                      />
                      <TextInput
                        label="Phone number"
                        name="phone_number"
                        required
                        fullWidth
                        value={current && current.phone_number}
                      />
                    </div>
                  </div>
                  <div className={section}>
                    <p className={section__title}>
                      When to deliver your message
                    </p>
                    <div className={section__content}>
                      <TimingSelect setValue={setTiming} value={current && current.timing} />
                      {timing === 'specific_day' && <DateInput value={current && current.date} />}
                      <p className={desc}>
                        We will send out this message once we receive
                        confirmation of your passing. Either by{" "}
                        <Link to="/dashboard/dead-man-switch">
                          dead man’s switch
                        </Link>{" "}
                        or an official document submitted by one of your
                        deputies.
                      </p>
                    </div>
                  </div>
                  <div className={section}>
                    <p className={section__title}>Message</p>
                    <div className={section__content}>
                      <TextInput label="Title" name="title" fullWidth />
                      <TextAreaToolbar
                        value={value}
                        setValue={setValue}
                        fullWidth
                      />
                      <FileInput />
                    </div>
                  </div>
                </div>
              </div>
              <div className={button__container}>
                <button>Continue</button>
              </div>
            </div>
          </form>
        </FormProvider>
      </main>
    </PageContainer>
  )
}

export default AddFutureMessage
