import { useContext, useEffect } from "react"
import { useHistory } from "react-router-dom"

import FutureMessagesContext from "../../context/futureMessages/futureMessagesContext"

import {
  container,
  content,
  inner,
  main,
  inner__content,
  cards,
  card__container,
  card__content,
  card__header,
  card__title,
  card__description,
  section__head,
  section__container,
  message__card,
  message__content,
  message__button,
  message__head,
  message__menu,
  modal,
  modal__content
} from "./FutureMessages.module.css"

import { Dots, Edit, Delete } from "../../assets/Icons"
import { useState } from "react"

const FutureMessages = () => {
  const { getFutureMessages, futureMessages } = useContext(FutureMessagesContext)

  useEffect(() => {
    getFutureMessages()
    // eslint-disable-next-line
  }, [])

  return (
    <div className={container}>
      <div className={content}>
        <div className={inner}>
          <main className={main}>
            <p>Future Messages</p>
            <h1>Set up your future messages.</h1>
            <p>Write yourself or a loved one a letter from the future.</p>
            <div className={inner__content}>
              <div className={cards}>
                <FutureMessageCard
                  time="Takes 1 min."
                  title="Future Email Message"
                  desc="Set send out time and add attachments to your future message."
                />
                <FutureMessageCard
                  time="Takes 3 min."
                  title="Future Video Message"
                  desc="Personal video message to your friends and loved ones. Record video
          from your webcam or upload a file."
                />
                <FutureMessageCard
                  time="Takes 2-3 min."
                  title="Future Social Post"
                  desc="Send out a message on a social site of your choosing to your friends and loved ones."
                />
                <BookAnExpertCard />
              </div>
              <section>
                <div className={section__head}>
                  <p>Future Messages</p>
                </div>
                <div className={section__container}>
                  {futureMessages.map(message => <Card key={message.id} {...message} />)}
                </div>
              </section>
            </div>
          </main>
        </div>
      </div>
    </div>
  )
}

const Card = (props) => {
  const [showModal, setShowModal] = useState(false)
  const { setCurrentFutureMessage, deleteFutureMessage } = useContext(
    FutureMessagesContext
  )

  const { first_name, last_name, timing, title, id } = props

  const history = useHistory()

  return (
    <div className={message__card}>
      <div className={message__content}>
        <button className={message__button}>
          <div className={message__head}>
            <span>
              <VideoIcon />
            </span>
            <p>{title}</p>
          </div>
          <p>
            <span>
              {first_name} {last_name}
              <br />
              on {timing}
            </span>
          </p>
          <div className={message__menu}>
            <div></div>
            <span onClick={() => setShowModal(!showModal)}>
              <Dots />
            </span>
          </div>
        </button>
      </div>
      {showModal && (
        <div className={modal}>
          <div className={modal__content}>
            <button
              onClick={() => {
                setCurrentFutureMessage(props)
                history.push('/dashboard/goodbye-add')
              }}
            >
              <span>
                <Edit />
              </span>
              <label>Edit future message</label>
            </button>
            <button onClick={() => deleteFutureMessage(id)}>
              <span>
                <Delete />
              </span>
              <label>Delete future message</label>
            </button>
          </div>
        </div>
      )}
    </div>
  )
}

const FutureMessageCard = ({ time, title, desc }) => {
  const history = useHistory()

  return (
    <div
      className={card__container}
      onClick={() => history.push("/dashboard/goodbye-add")}
    >
      <div className={card__content}>
        <div className={card__header}>
          <Plus />
          <span>{time}</span>
        </div>
        <p className={card__title}>{title}</p>
        <p className={card__description}>{desc}</p>
      </div>
    </div>
  )
}

const BookAnExpertCard = () => {
  const history = useHistory()

  return (
    <div
      className={card__container}
      onClick={() => history.push("/dashboard/vip")}
    >
      <div className={card__content}>
        <div className={card__header}>
          <Phone />
          <span>Free</span>
        </div>
        <p className={card__title}>Book an expert</p>
        <p className={card__description}>
          Not sure what to do? Book an expert consultation and let us help you
          guide through process.
        </p>
      </div>
    </div>
  )
}

const Plus = () => (
  <svg
    width="26"
    height="26"
    viewBox="0 0 26 26"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M13.931 12.069V.897h-1.862v11.172H.897v1.862h11.172v11.172h1.862V13.931h11.172v-1.862H13.931z"
      fill="currentColor"
    ></path>
  </svg>
)

const Phone = () => (
  <svg
    width="22"
    height="22"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      clipRule="evenodd"
      d="M7.977 6.732a1.711 1.711 0 000-2.418L6.163 2.501a1.71 1.71 0 00-2.419 0l-.993.994a2.56 2.56 0 00-.324 3.227 39.761 39.761 0 0011.112 11.113 2.564 2.564 0 003.227-.324l.994-.995a1.708 1.708 0 000-2.417l-1.813-1.813a1.707 1.707 0 00-2.418 0l-.605.604a40.522 40.522 0 01-5.552-5.554l.605-.604v0z"
      stroke="currentColor"
      strokeLinecap="round"
      strokeLinejoin="round"
    ></path>
  </svg>
)

const VideoIcon = () => (
  <svg
    width="36"
    height="36"
    viewBox="0 0 36 36"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      clipRule="evenodd"
      d="M5.542 14.208h24.916v13c0 1.197-.97 2.167-2.166 2.167H7.707a2.167 2.167 0 01-2.167-2.167v-13zM7.708 6.625h20.584c1.196 0 2.166.97 2.166 2.167v3.25H5.542v-3.25c0-1.197.97-2.167 2.166-2.167z"
      stroke="#657795"
      strokeWidth="1.083"
      strokeLinecap="round"
      strokeLinejoin="round"
    ></path>
    <path
      clipRule="evenodd"
      d="M15.2 17.892a.708.708 0 00-.992.65v6.5a.709.709 0 00.992.65l7.178-3.142a.828.828 0 000-1.517L15.2 17.892z"
      stroke="#657795"
      strokeWidth="1.083"
      strokeLinecap="round"
      strokeLinejoin="round"
    ></path>
    <path
      d="M9.875 12.042l5.417-5.417M15.291 12.042l5.417-5.417M20.708 12.042l5.417-5.417M26.125 12.042L30.267 7.9M5.542 10.958l4.333-4.333"
      stroke="#657795"
      strokeWidth="1.083"
      strokeLinecap="round"
      strokeLinejoin="round"
    ></path>
  </svg>
)
export default FutureMessages
