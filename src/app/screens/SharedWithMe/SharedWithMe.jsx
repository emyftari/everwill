import { useEffect, useContext } from 'react'

import UserContext from '../../context/user/userContext'

import { BackArrow, LogoSimple } from '../../assets/Icons'
import { dataSites } from '../../assets/images/siteImages'

import {
  container,
  content,
  inner,
  arrow,
  main__desc,
  main__title,
  main__paragraph,
  shared__container,
  section,
  section__title,
  shared__people,
  deputy,
  deputy__icon,
  deputy__info,
  deputy__shared,
  section__head,
  card
} from './SharedWithMe.module.css'

const SharedWithMe = () => {
  const { sharedWithMe, getPeopleSharingWithMe, peopleSharingWithMe, itemsSharedWithMe } = useContext(UserContext)

  useEffect(() => {
    getPeopleSharingWithMe()
    sharedWithMe()
    // eslint-disable-next-line
  }, [])

  return (
    <div className={container}>
      <div className={content}>
        <div className={inner}>
          <div className={arrow}>
            <div>
              <BackArrow />
            </div>
            <div>
              <LogoSimple />
            </div>
          </div>
          <main>
            <p className={main__desc}>Shared with me</p>
            <h1 className={main__title}>
              Help protect the digital legacy of others.
            </h1>
            <p className={main__paragraph}>
              This is the place where you can find what other people are sharing
              with you when you are their Everwill Deputy.When you invite
              Deputies, they will see this section on their Everwill account.
              Ask your family and loved ones to make you their Deputy and help
              protect their digital legacy as well.
            </p>
            <div className={shared__container}>
              <section className={section}>
                <p className={section__title}>People sharing with you</p>
                <div className={shared__people}>
                  {peopleSharingWithMe?.map(deputy => <Deputy key={deputy.email} {...deputy} />)}
                </div>
              </section>
              <section className={section}>
                <div className={section__head}>
                  <p>Sites shared with me</p>
                </div>
                {itemsSharedWithMe?.sites.map(site => <SiteCard {...site} />)}
              </section>
              <section className={section}>
                <div className={section__head}>
                  <p >Documents shared with me</p>
                </div>
                {itemsSharedWithMe?.documents.map(({ title }) => (
                  <div className={card}>
                    <button>
                      <span>{title}</span>
                    </button>
                  </div>
                ))}
              </section>
              <section className={section}>
                <div className={section__head}>
                  <p>Devices shared with me</p>
                </div>
                {itemsSharedWithMe?.devices.map(({ name }) => (
                  <div className={card}>
                    <button>
                      <span>{name}</span>
                    </button>
                  </div>
                ))}
              </section>
            </div>
          </main>
        </div>
      </div>
    </div>
  )
}

const Deputy = ({ email, shared_items }) => {
  return (
    <div className={deputy}>
      <div className={deputy__icon}>
        <div></div>
      </div>
      <div className={deputy__info}>
        <p>{email}</p>
      </div>
      <div className={deputy__shared}>
        <p>{shared_items} shared items</p>
      </div>
    </div>
  )
}

const SiteCard = ({ name }) => {
  const res = dataSites.filter(
    (site) => site.site.toLowerCase() === name.toLowerCase()
  )

  return (
    <div className={card}>
      <button>
        <img src={res[0].img} alt="" />
        <span>{name}</span>
      </button>
    </div>
  )
}

export default SharedWithMe
