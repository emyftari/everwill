import AddCard from '../card/AddCard'
import AddButton from '../card/AddButton'

import {
  page__container,
  page__content,
  page__inner,
  page__main,
  page__center,
  page__center__grid,
  add__section,
  add__section__title,
  add__section__button,
  add__section__grid,
  page__bar,
  card__desc
} from './PageTemplate.module.css'


import { LogoSimple, BackArrow } from '../../assets/Icons'

const Section = ({ page, title, desc, data, path, cards, cardDesc }) => {

  return (
    <div className={page__container}>
      <div className={page__content}>
        <div className={page__inner}>
          <div className={page__bar}>
            <div>
              <BackArrow />
            </div>
            <div>
              <LogoSimple />
            </div>
          </div>
          <main className={page__main}>
            <p>{page}</p>
            <h1>{title}</h1>
            <p>{desc}</p>
            <div className={card__desc}>
              <p>{cardDesc}</p>
            </div>
            <div className={page__center}>
              <div className={page__center__grid}>
                {
                  data.map(el => <AddCard key={el.title} title={el.title} desc={el.desc} to={path} />)
                }
              </div>
              <section className={add__section}>
                <div className={add__section__title}>
                  <p>My {page}</p>
                </div>
                <div className={add__section__button}>
                  <div className={add__section__grid}>
                    {cards}
                    <AddButton path={path} />
                  </div>
                </div>
              </section>
            </div>
          </main>
        </div>
      </div>
    </div>
  )
}

export default Section
