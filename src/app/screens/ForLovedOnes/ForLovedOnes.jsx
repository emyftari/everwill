import { useHistory } from 'react-router-dom'

import PageContainer from '../../components/templates/PageContainer'

import {
  small__title,
  title,
  content,
  article__container,
  card__container,
  card,
  top,
  bottom,
  button
} from './ForLovedOnes.module.css'

import { PlusSquare, TickNote, Touch, Facebook, Google, Paypal, LinkedIn, Netflix } from '../../assets/Icons'

const ForLovedOnes = () => {
  const history = useHistory()

  return (
    <PageContainer>
      <main >
        <p className={small__title}>For Loved Ones</p>
        <h1 className={title}>Protect the memory <br />of your loved ones.</h1>
        <div className={content}>
          <p>Everwill helps families who lost someone to protect and preserve their digital presence and memory. Let us help you!</p>
          <div className={article__container}>
            <div></div>
            <article>
              <div><Touch /></div>
              <p>Select sites &amp; actions</p>
              <p>Select the websites you need help with and the action you want taken. Each website has a&nbsp;unique set of options.</p>
            </article>
            <article>
              <div><PlusSquare /></div>
              <p>Upload & sign documents</p>
              <p>Provide information about the account, and upload documents like proof that the person has passed away and create necessary online Power of Attorneys.</p>
            </article>
            <article>
              <div><TickNote /></div>
              <p>Everwill makes it happen</p>
              <p>The Everwill team reaches out to all the global websites you select to ensure that your requested actions are safely and diligently completed in a timely manner.</p>
            </article>
          </div>
        </div>
        <div className={card__container}>
          <div className={card} onClick={() => history.push('/store')}>
            <div className={top}>
              <div>
                <Facebook />
              </div>
            </div>
            <div className={bottom}>
              <strong>Memoralize Facebook Page</strong>
              <span>Choose how a Facebook account is preserved.</span>
            </div>
          </div>
          <div className={card} onClick={() => history.push('/store')}>
            <div className={top}>
              <div>
                <Paypal />
              </div>
            </div>
            <div className={bottom}>
              <strong>Extract remaining PayPal funds</strong>
              <span>Transfer any remaining funds from PayPal.</span>
            </div>
          </div>
          <div className={card} onClick={() => history.push('/store')}>
            <div className={top}>
              <div>
                <Netflix />
              </div>
            </div>
            <div className={bottom}>
              <strong>Stop Netflix Subscription</strong>
              <span>Annoyed by recurring payments of Netflix?</span>
            </div>
          </div>
          <div className={card} onClick={() => history.push('/store')}>
            <div className={top}>
              <div>
                <Google />
              </div>
            </div>
            <div className={bottom}>
              <strong>Extract Google Photos</strong>
              <span>Keep memories of your loved one safe.</span>
            </div>
          </div>
          <div className={card} onClick={() => history.push('/store')}>
            <div className={top}>
              <div>
                <LinkedIn />
              </div>
            </div>
            <div className={bottom}>
              <strong>LinkedIn</strong>
              <span>Say goodbye to followers of your loved one.</span>
            </div>
          </div>
        </div>
        <div className={button}>
          <button onClick={() => history.push('/store')}>
            Get started
          </button>
        </div>
      </main>
    </PageContainer>
  )
}

export default ForLovedOnes