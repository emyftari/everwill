import PageContainer from '../../components/templates/PageContainer'

import { header, container, side, locked, locked__content, card, overlay, card__content, card__head } from './WillDirectives.module.css'

import { Plus } from '../../assets/Icons'

const WillDirectives = () => {
  return (
    <PageContainer>
      <main>
        <p className={header}>Will &amp; directives</p>
        <div className={container}>
          <div className={side}>
            <h1 className="css-1djoent eltiyan0">
              Don't leave <br />
              your legacy to <br />
              destiny
            </h1>
          </div>
          <div className={side}>
            <div>
              <p>
                Letting your loved ones know your will and last wishes is an
                important task. It will give you peace of mind and reduce the
                pain for your loves ones if they know your wishes. Make your
                wishes clear so it is not the government that makes the
                decisions for you.
              </p>
              <p>
                If you do not have a will try our “create a will” tool, that
                will help you. Once done, then print, sign it with witnesses to
                make it legal.
              </p>
              <p>
                Do you have medical and funeral directives with instructions? If
                not, then take a few minutes and create them and share with your
                family.
              </p>
            </div>
          </div>
        </div>
        <div className={locked}>
          <div className={locked__content}>
            <div className={card}>
              <div className={overlay}>
                <div>
                  <svg width="17" height="21" xmlns="http://www.w3.org/2000/svg"><g stroke="currentColor" strokeLinejoin="round" strokeWidth="2" fill="none" fillRule="evenodd"><path d="M2.36 18.67H14.6V8.59H2.36zM4.52 6.07a3.96 3.96 0 117.92 0v2.52H4.52V6.07h0z"></path></g></svg>
                  <p>Unlock</p>
                </div>
              </div>
              <div className={card__content}>
                <div className={card__head}>
                  <Plus />
                  <span>Takes 9 minutes</span>
                </div>
                <p>Create a funeral directive</p>
                <p>List your instructions for any funeral arrangements, including who should be in charge, and what to do with your body.</p>
              </div>
            </div>
            <div className={card}>
              <div className={overlay}></div>
              <div className={card__content}>
                <div className={card__head}>
                  <Plus />
                  <span>Coming soon</span>
                </div>
                <p>Create a will</p>
                <p>If you do not have a will try our easy “create a will” tool, that will help you make one in minutes. Print, sign it with witnesses to make it legal.</p>
              </div>
            </div>
            <div className={card}>
              <div className={overlay}></div>
              <div className={card__content}>
                <div className={card__head}>
                  <Plus />
                  <span>Coming soon</span>
                </div>
                <p>Create a medical directive</p>
                <p>List your instructions for any health care arrangements, including wishes about key health care decisions that could become necessary.</p>
              </div>
            </div>
          </div>
        </div>
      </main>
    </PageContainer>
  )
}

export default WillDirectives