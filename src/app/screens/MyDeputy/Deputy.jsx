import { useContext, useEffect } from 'react'
import DeputyContext from '../../context/deputy/deputyContext'

import PageContainer from '../../components/templates/PageContainer'
import AddButton from '../../components/buttons/AddButton'
import DeputyCard from '../../components/deputyCard/DeputyCard'

import {
  page__main,
  page__center__grid,
  center__grid__item
} from './Deputy.module.css'

const Deputy = () => {
  const { deputies, loadDeputies } = useContext(DeputyContext)

  useEffect(() => {
    loadDeputies()
    // eslint-disable-next-line
  }, [])

  return (
    <PageContainer>
      <main className={page__main}>
        <p>My deputies</p>
        <h1>
          Share your digital legacy <br />
          with family and friends today.
        </h1>
        <div className={page__center__grid}>
          <div className={center__grid__item}>
            <p>Why are deputies important?</p>
            <p>
              Your Deputies are your family and friends that will access your
              Everwill Plan incl. your sites and documents already today or
              when you pass away. It is easy to control who, will get what,
              when. You can choose your partner, children, siblings, parents,
              trusted friends, the executor of your will i.e. people you trust
              with your digital legacy.
            </p>
          </div>
          <div className={center__grid__item}>
            <p>How do deputies gain access?</p>
            <p>
              Your Deputies will receive an email invitation with instructions
              to create a free Everwill Deputy account. They will then be able
              to see the content you chose to share with them today or when you
              passed away. Take a minute and invite two deputies that are
              important to you now.
            </p>
          </div>
        </div>
        <AddButton btnText="Add a deputy" />
        <DeputyCard deputies={deputies} />
      </main>
    </PageContainer>
  )
}

export default Deputy
