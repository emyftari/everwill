export const data = {
  sitesSocial: [
    { title: 'Add social media', desc: 'Share how you want to be remembered on Facebook etc.' },
    { title: 'Add financial sites', desc: 'Make sure your bank and financial sites can be found.' },
    { title: 'Add photo sites', desc: 'Ensure your priceless photo albums do not get lost.' },
    { title: 'Add work sites', desc: 'List your subscription, domain names and other work sites.' },
    { title: 'Add your email', desc: 'Make sure loved ones can access your email account!' }
  ],
  docsNotes: [
    { title: 'Add life insurance', desc: 'Help loved ones find all your insurance policies in time.' },
    { title: 'Add your passport ', desc: 'Share copy of passport and id documentation with family.' },
    { title: 'Add valuable contracts', desc: 'Make is easy to find all your important contracts  today' },
    { title: 'Add your will', desc: 'Make sure loved ones find your will and last wishes.' },
    { title: 'Add a Power of Attorney', desc: 'From paying bills to filling taxes. A POA manages your accounts when you’re unable.' }
  ],
  devices: [
    { title: 'Add a computer password', desc: 'This could include both your login and password and may apply to multiple computers.' },
    { title: 'Add a phone code', desc: "Without a phone code your loved ones won't be able to take the initial first steps on your behalf." },
    { title: 'Add router password', desc: 'Accessing a router can sometimes be important when you need to adjust special settings.' },
    {
      title: 'Add smart home keycode', desc: "Your home can be really smart but also leave others feeling quite annoyed if they don't have access."
    },
    { title: 'Add alarm code', desc: 'Prevents your Deputies from being locked out or alarming the authorities.' }
  ]
}