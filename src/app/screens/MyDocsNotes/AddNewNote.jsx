import AddScreenTemplate from '../../components/templates/AddScreenTemplate'
import AddNewNoteForm from '../../components/forms/AddNewNoteForm'

const AddNewNote = () => {
  return (
    <AddScreenTemplate title='Add new document or note.'>
      <AddNewNoteForm />
    </AddScreenTemplate>
  )
}

export default AddNewNote