import { useContext } from 'react'
import NoteContext from '../../context/note/noteContext'

import PageTemplate from '../../components/pageTemplate/PageTemplate'
import NoteCard from '../../components/card/NoteCard'

import { data } from '../data'

const MyDocsNotes = () => {
  const { notes } = useContext(NoteContext)
  return (
    <PageTemplate
      page="DOCS & NOTES"
      title="Save your documents and notes."
      desc="Drop documents or your notes here, assign a deputy and set what should happen with your legacy when your day comes."
      buttonName='Add document or note'
      path='/dashboard/add-new-note'
      data={data.docsNotes}
      cards={notes.map(note => <NoteCard key={note.id} {...note} />)}
    />
  )
}

export default MyDocsNotes
