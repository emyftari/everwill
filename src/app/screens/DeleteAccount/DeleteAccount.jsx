import { useContext } from 'react'
import { useHistory } from 'react-router-dom'

import AuthContext from '../../../context/auth/AuthContext'
import UserContext from '../../context/user/userContext'

import {
  container,
  content,
  inner__container,
  arrow__container
} from '../../components/deputy/DeputyProfile.module.css'

import {
  header,
  side,
  info,
  list__container,
  buttons
} from './DeleteAccount.module.css'

import { BackArrow, Warning } from '../../assets/Icons'

const DeleteAccount = () => {
  const history = useHistory()
  const { deleteAccount } = useContext(UserContext)
  const { logout } = useContext(AuthContext)

  const handleClick = () => {
    deleteAccount()
    logout()
  }

  return (
    <div className={container}>
      <div className={content}>
        <div className={inner__container}>
          <div className={arrow__container}>
            <div onClick={() => history.goBack()}>
              <BackArrow />
            </div>
          </div>
          <main>
            <div className={header}>
              <h1>Are you sure to delete your Everwill account?</h1>
              <p>
                Deleting your account will delete all data forever. You won’t be
                able to access your account anymore.{' '}
              </p>
            </div>
            <div className={info}>
              <div className={side}>
                <p>You'll lose all of the following</p>
                <ul className={list__container}>
                  <li>
                    <p>
                      <Warning />
                      Save null sites &amp; passwords
                    </p>
                  </li>
                  <li>
                    <p>
                      <Warning />
                      Store null documents
                    </p>
                  </li>
                  <li>
                    <p>
                      <Warning />
                      Create null future messages
                    </p>
                  </li>
                  <li>
                    <p>
                      <Warning />
                      Last posts on socials
                    </p>
                  </li>
                  <li>
                    <p>
                      <Warning />
                      Add trusted contacts
                    </p>
                  </li>
                  <li>
                    <p>
                      <Warning />
                      Videos in future messages
                    </p>
                  </li>
                  <li>
                    <p>
                      <Warning />
                      Animated photos
                    </p>
                  </li>
                </ul>
              </div>
              <div className={side}>
                <p>
                  Before going consider what you'll lose
                </p>
                <p>
                  Once you delete your account, you’ll lose access to all your
                  data.
                </p>
              </div>
            </div>
            <div className={buttons}>
              <button onClick={handleClick}>Delete my account</button>
              <button onClick={() => history.push('/dashboard')}>I changed my mind, keep my account active</button>
            </div>
          </main>
        </div>
      </div>
    </div>
  )
}

export default DeleteAccount
