import { useContext } from 'react'
import { useHistory } from 'react-router-dom'
import { useForm, FormProvider } from 'react-hook-form'

import UserContext from '../../context/user/userContext'
import AlertContext from '../../context/alert/alertContext'

import {
  container,
  content,
  inner__container,
  arrow__container
} from '../../components/deputy/DeputyProfile.module.css'

import {
  form__container,
  list,
  list__item,
  title,
  contents,
  contents__title,
  desc,
  buttonContainer,
  fields
} from './DeadMen.module.css'

import FrequencySelect from '../../components/inputs/FrequencySelect'
import TimesSelect from '../../components/inputs/TimesSelect'

import { BackArrow, Warning } from '../../assets/Icons'

const DeadMen = () => {
  const { enableDeadManSwitch } = useContext(UserContext)
  const { setAlert } = useContext(AlertContext)
  const methods = useForm()
  const history = useHistory()

  const onSubmit = async data => {
    setAlert('Dead man’s switch was successfully enabled.', 'info')
    enableDeadManSwitch(data)
    history.push('/dashboard/profile')
  }

  return (
    <div className={container}>
      <div className={content}>
        <div className={inner__container}>
          <div className={arrow__container}>
            <div onClick={() => history.goBack()}>
              <BackArrow />
            </div>
          </div>
          <main>
            <h1 className={title}>Dead man's switch.</h1>
            <FormProvider {...methods}>
              <form onSubmit={methods.handleSubmit(onSubmit)}>
                <div className={form__container}>
                  <div className={contents}>
                    <p className={contents__title}>What is it?</p>
                    <p className={desc}>
                      Sometimes it just happens, you feel your time's coming and
                      no one knows exactly when. Maybe you don’t want to assign
                      someone close to you to trigger events on GoodTrust
                      following your death. That’s why we have a dead man’s
                      switch.
                    <br />
                      <br />
                    Set an interval how often we should contact you, and if you
                    reply, we know you’re alive. If you don’t reply for 3 times,
                    we’ll trigger events on GoodTrust following your death.
                  </p>
                  </div>
                  <div className={contents}>
                    <p className={contents__title}>
                      How often should we contact you?
                  </p>
                    <div className={fields}>
                      <FrequencySelect />
                      <TimesSelect />
                    </div>
                  </div>
                  <div className={contents}>
                    <p className={contents__title}>What will happen next</p>
                    <ul className={list}>
                      <li className={list__item}>
                        <p>
                          <Warning />
                        Set the interval of how often we should contact you via
                        email to verify your reply
                      </p>
                      </li>
                      <li className={list__item}>
                        <p>
                          <Warning />
                        Missing to reply 3 times in a row will result in us
                        considering you as deceased
                      </p>
                      </li>
                      <li className={list__item}>
                        <p>
                          <Warning />
                        Your Sites &amp; Social will be managed and shared with
                        your assigned deputies
                      </p>
                      </li>
                      <li className={list__item}>
                        <p>
                          <Warning />
                        Your Documents will be shared with your assigned
                        deputies
                      </p>
                      </li>
                      <li className={list__item}>
                        <p>
                          <Warning />
                        Your Future Messages will be sent
                      </p>
                      </li>
                      <li className={list__item}>
                        <p>
                          <Warning />
                        Your Will &amp; Directives will be shared with your
                        assigned deputies
                      </p>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className={buttonContainer}>
                  <button>Set dead man’s switch</button>
                </div>
              </form>
            </FormProvider>
          </main>
        </div>
      </div>
    </div>
  )
}

export default DeadMen
