import { useHistory } from 'react-router-dom'

import PageContainer from '../../components/templates/PageContainer'

import {
  container,
  header,
  section,
  section__header,
  section__description,
  section__bottom,
  bottom,
  info,
  button
} from './BookAnExpert.module.css'

import image from '../../assets/images/vipService.jpg'

const BookAnExpert = () => {
  const history = useHistory()

  return (
    <PageContainer>
      <main>
        <div className={container}>
          <div>
            <h1 className={header}>Need help? Book a call with expert.</h1>
            <section className={section}>
              <p className={section__header}>We're here for you</p>
              <p className={section__description}>Need help with online account for someone that passed away? Have questions how to delete or memorialize social media? Looking to extract photos or funds from accounts. Need help with fake account?</p>
              <p className={section__bottom}>Book an expert consultation and let us help you.</p>
              <div className={bottom}>
                <div className={info}>
                  <span >24/7 support</span>
                  <span >20 experts</span>
                  <span >Free (30 min)</span>
                </div>
                <div className={button}>
                  <button onClick={() => history.push('/dashboard/schedule-a-call')}>Schedule a call</button>
                </div>
              </div>
            </section>
          </div>
          <div className={image}>
            <img src={image} alt="" />
          </div>
        </div>
      </main>
    </PageContainer>
  )
}

export default BookAnExpert