import AddScreenTemplate from '../../components/templates/AddScreenTemplate'
import AddNewDeviceForm from '../../components/forms/AddNewDeviceForm'

import { useContext } from 'react'
import DeviceContext from '../../context/device/deviceContext'

const AddNewDevice = () => {
  const { currentDevice } = useContext(DeviceContext)

  return (
    <AddScreenTemplate title={currentDevice ? 'Edit device' : 'Add new device.'} >
      <AddNewDeviceForm />
    </AddScreenTemplate >
  )
}

export default AddNewDevice