import { useContext } from 'react'
import DeviceContext from '../../context/device/deviceContext'

import PageTemplate from '../../components/pageTemplate/PageTemplate'
import DeviceCard from '../../components/card/DeviceCard'
import { data } from '../data'

const Devices = () => {
  const { devices } = useContext(DeviceContext)
  return (
    <PageTemplate
      page='DEVICES'
      title='Unlock your devices.'
      desc='Ensure that someone you trust (e.g. a Deputy) is able to gain access to the right devices at the right time. You decide how and when to share your phone codes and computer passwords. Keep in mind that without the physical device no one can do anything with this information. But just in case and we will keep it safe in the meantime.'
      buttonName='Add devices'
      path='/dashboard/add-new-device'
      data={data.devices}
      cardDesc='Things you shoud do'
      cards={devices.map(device => <DeviceCard key={device.id} {...device} />)}
    />
  )
}

export default Devices
