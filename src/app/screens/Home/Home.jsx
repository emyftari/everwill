import { useEffect, useContext, useState } from 'react'

import AuthContext from '../../../context/auth/AuthContext'
import SiteContext from '../../context/site/siteContext'
import NoteContext from '../../context/note/noteContext'
import DeviceContext from '../../context/device/deviceContext'
import DeputyContext from '../../context/deputy/deputyContext'
import UserContext from '../../context/user/userContext'

import Showcase from '../../components/showcase/Showcase'
import ShowSites from '../../components/showcase/ShowSites'
import SiteCard from '../../components/card/SiteCard'
import NoteCard from '../../components/card/NoteCard'
import DeviceCard from '../../components/card/DeviceCard'
import Todo from '../../components/todo/Todo'
import DeputyCard from '../../components/deputyCard/DeputyCard'

import {
  page__container,
  page__content,
  page__inner,
  page__main,
  page__bar,
  todo__section,
  todo__container,
  todo__left,
  todo__grid,
  todo__grid__left,
  todo__grid__right,
  todo__grid__center
} from './Home.module.css'

import { BackArrow, LogoSimple, CircleCheck, Hide } from '../../assets/Icons'

import { todoData } from './todoData'
import { showcaseData } from './showcaseData'

const Home = () => {
  const [showCompleted, setShowCompleted] = useState(true)
  const { loading, user } = useContext(AuthContext)
  const { loadSites, sites } = useContext(SiteContext)
  const { loadNotes, notes } = useContext(NoteContext)
  const { loadDevices, devices } = useContext(DeviceContext)
  const { loadDeputies, deputies } = useContext(DeputyContext)
  const { deadman, getDeadManSwitch } = useContext(UserContext)

  useEffect(() => {
    if (!loading && user) {
      loadSites()
      loadNotes()
      loadDevices()
      loadDeputies()
      getDeadManSwitch()
    }

    // eslint-disable-next-line
  }, [])

  let totalTodos = 5
  let renderDeadman
  let renderDevice
  let renderDeputy
  let renderNotes
  let renderSites

  if (sites.length > 0) {
    if (showCompleted) {
      renderSites = true
    } else {
      renderSites = false
    }
    totalTodos--
  } else {
    renderSites = true
  }

  if (notes.length > 0) {
    if (showCompleted) {
      renderNotes = true
    } else {
      renderNotes = false
    }
    totalTodos--
  } else {
    renderNotes = true
  }

  if (devices.length > 0) {
    if (showCompleted) {
      renderDevice = true
    } else {
      renderDevice = false
    }
    totalTodos--
  } else {
    renderDevice = true
  }

  if (deputies.length > 0) {
    if (showCompleted) {
      renderDeputy = true
    } else {
      renderDeputy = false
    }
    totalTodos--
  } else {
    renderDeputy = true
  }

  if (deadman !== null) {
    if (showCompleted) {
      renderDeadman = true
    } else {
      renderDeadman = false
    }
    totalTodos--
  } else {
    renderDeadman = true
  }

  const handleClick = () => setShowCompleted(!showCompleted)

  return (
    <div className={page__container}>
      <div className={page__content}>
        <div className={page__inner}>
          <div className={page__bar}>
            <div>
              <BackArrow />
            </div>
            <div>
              <LogoSimple />
            </div>
          </div>
          <main className={page__main}>
            <div>
              <p>DASHBOARD</p>
            </div>
            <h1>Welcome, {user.first_name ? user.first_name : user.email}</h1>
            <div className={todo__section}>
              <div className={todo__container}>
                <div className={todo__left}>
                  <p>{totalTodos} TO-DO’s LEFT</p>
                  {showCompleted && (
                    <div onClick={handleClick}>
                      <p>
                        <Hide />
                        Hide completed
                      </p>
                    </div>
                  )}
                  {!showCompleted && (
                    <div onClick={handleClick}>
                      <p>
                        <CircleCheck />
                        Show completed
                      </p>
                    </div>
                  )}
                </div>
                <div className={todo__grid}>
                  <div className={todo__grid__left}></div>
                  <div className={todo__grid__center}>
                    {renderDeadman && <Todo {...todoData[0]} />}
                    {renderDevice && <Todo {...todoData[1]} />}
                    {renderDeputy && <Todo {...todoData[2]} />}
                    {renderSites && <Todo {...todoData[3]} />}
                    {renderNotes && <Todo {...todoData[4]} />}
                  </div>
                  <div className={todo__grid__right}></div>
                </div>
              </div>
              {deputies.length === 0 ? (
                <Showcase {...showcaseData[0]} />
              ) : (
                <DeputyCard deputies={deputies} />
              )}

              {sites.length === 0 ? (
                <Showcase {...showcaseData[1]} />
              ) : (
                <ShowSites
                  header="my sites & social"
                  length={sites.length}
                  path="/dashboard/sites"
                >
                  {sites.slice(0, 3).map((site) => (
                    <SiteCard key={site.id} {...site} />
                  ))}
                </ShowSites>
              )}
              {notes.length === 0 ? (
                <Showcase {...showcaseData[2]} />
              ) : (
                <ShowSites
                  header="my documents and notes"
                  length={notes.length}
                  path="/dashboard/document"
                >
                  {notes.slice(0, 3).map((note) => (
                    <NoteCard key={note.id} {...note} />
                  ))}
                </ShowSites>
              )}
              {devices.length === 0 ? (
                <Showcase {...showcaseData[3]} />
              ) : (
                <ShowSites
                  header="my devices"
                  length={devices.length}
                  path="/dashboard/device"
                >
                  {devices.slice(0, 3).map((device) => (
                    <DeviceCard key={device.id} {...device} />
                  ))}
                </ShowSites>
              )}
            </div>
          </main>
        </div>
      </div>
    </div>
  )
}

export default Home
