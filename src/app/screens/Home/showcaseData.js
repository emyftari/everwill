import {
  AvatarDouble,
  Note,
  Device,
  Facebook,
  Spotify,
  Instagram,
  Google
} from '../../assets/Icons'

export const showcaseData = [
  {
    icon: [<AvatarDouble />],
    header: 'Your deputies',
    title: 'Invite your deputy',
    description:
      'Family member, partner, friend or a legal advocate that can access your account.',
    button: 'Add deputy',
    path: '/dashboard/deputy'
  },
  {
    icon: [<Facebook />, <Spotify />, <Instagram />, <Google />],
    header: 'Your sites and social',
    title: 'Protect your accounts',
    description: 'Specify what should be done after you pass away',
    button: 'Add first Site',
    path: '/dashboard/sites'
  },
  {
    icon: [<Note />],
    header: 'Your documents and notes',
    title: 'Upload important documents',
    description:
      'Will, Power of Attorney or important letter? Upload now to secure storage.',
    button: 'Upload a document',
    path: '/dashboard/document'
  },
  {
    icon: [<Device />],
    header: 'Your devices',
    title: 'Enable access to your devices',
    description:
      'Add your phone, tablet or a computer and specify who has the access in case something happens.',
    button: 'Add device',
    path: '/dashboard/device'
  }
]
