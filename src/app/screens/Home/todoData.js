import { Switch, Pattern, AvatarPlus, Note, Helmet } from '../../assets/Icons'

export const todoData = [
  {
    icon: <Switch />,
    title: 'Setup Deadman’s switch',
    description:
      'GoodTrust will reach out to you reguarly to make sure you are ok.',
    button: 'Start now',
    path: '/dashboard/dead-man-switch'
  },
  {
    icon: <Pattern />,
    title: 'Add your phone passcode',
    description: 'Store your passcode here as a backup when someone needs it.',
    button: 'Add device',
    path: '/dashboard/add-new-device'
  },
  {
    icon: <AvatarPlus />,
    title: 'Add a Deputy',
    description:
      'Family member, partner, friend or a legal advocate that can access your account.',
    button: 'Add deputy',
    path: '/dashboard/deputy'
  },
  {
    icon: <Helmet />,
    title: 'Add a site',
    description:
      'You might want to memorialize or transfer certain sites in your name.',
    button: 'Add site',
    path: '/dashboard/add-new-site'
  },
  {
    icon: <Note />,
    title: 'Add a document',
    description:
      'The best way to minimize clutter in your life by uploading valuable documents.',
    button: 'Create POA',
    path: '/dashboard/add-new-note'
  }
]
