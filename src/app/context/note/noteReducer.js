import { SET_CURRENT, NOTES_LOADED, NOTE_ADDED, NOTE_DELETED, NOTE_UPDATED, CLEAR_CURRENT } from '../types'

const siteReducer = (state, action) => {
  switch (action.type) {
    case NOTES_LOADED:
      return {
        ...state,
        notes: [...action.payload]
      }
    case NOTE_ADDED:
      return {
        ...state,
        notes: [...state.notes, action.payload]
      }
    case NOTE_DELETED:
      return {
        ...state,
        notes: state.notes.filter(note => note.id !== action.payload)
      }
    case NOTE_UPDATED:
      return {
        ...state,
        notes: state.notes.map(note => note.id === action.payload.id ? action.payload : note)
      }
    case SET_CURRENT:
      return {
        ...state,
        currentNote: action.payload
      }
    case CLEAR_CURRENT:
      return {
        ...state,
        currentNote: null
      }
    default:
      return state
  }
}

export default siteReducer
