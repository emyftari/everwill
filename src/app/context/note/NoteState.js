import { useReducer } from 'react'

import axios from 'axios'

import NoteContext from './noteContext'
import noteReducer from './noteReducer'

import { SET_CURRENT, NOTES_LOADED, NOTE_ADDED, NOTE_DELETED, NOTE_UPDATED, CLEAR_CURRENT } from '../types'

const NoteState = ({ children }) => {
  axios.defaults.baseURL = 'https://mighty-peak-18791.herokuapp.com'
  axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.token

  const initalState = {
    notes: [],
    currentNote: null
  }

  const [state, dispatch] = useReducer(noteReducer, initalState)

  const loadNotes = async () => {
    try {
      const res = await axios.get('/api/my-documents')
      dispatch({
        type: NOTES_LOADED,
        payload: res.data.data
      })
    } catch (error) {
      console.error(error)
    }
  }

  const addNote = async (data) => {
    const formData = {
      category: data.category.toLowerCase(),
      title: data.title,
      notes: data.notes,
    }

    try {
      if (!data.media) {
        const res = await axios.post('/api/documents', formData)

        dispatch({
          type: NOTE_ADDED,
          payload: res.data.data
        })
      } else {
        const attach = new FormData()
        attach.append('media', data.media)

        const response = await axios.post('/api/documents', formData)
        const resWithAttach = await axios.post(`/api/documents/add-media/${response.data.data.id}`, attach)

        dispatch({
          type: NOTE_ADDED,
          payload: resWithAttach.data.data
        })
      }
    } catch (error) {
      console.log(error.response)
    }
  }

  const deleteNote = async (id) => {
    try {
      await axios.delete(`api/documents/${id}`)
      dispatch({ type: NOTE_DELETED, payload: id })
    } catch (error) {
      console.log(error.response)
    }
  }

  const updateNote = async (data) => {
    try {
      const formData = {
        title: data.title,
        category: data.category.toLowerCase(),
        notes: data.notes === '' && 'n',
      }


      const res = await axios.post(`/api/documents/${data.id}/update`, formData)
      dispatch({ type: NOTE_UPDATED, payload: res.data.data })
    } catch (error) {
      console.log(error.response)
    }
  }

  const setCurrentNote = (note) => dispatch({ type: SET_CURRENT, payload: note })

  const clearCurrentNote = () => dispatch({ type: CLEAR_CURRENT })

  return (
    <NoteContext.Provider
      value={{
        notes: state.notes,
        currentNote: state.currentNote,
        loadNotes,
        addNote,
        deleteNote,
        updateNote,
        setCurrentNote,
        clearCurrentNote
      }}
    >
      {children}
    </NoteContext.Provider>
  )
}

export default NoteState

