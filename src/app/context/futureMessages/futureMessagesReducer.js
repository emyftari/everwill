import {
  GET_FUTURE_MESSAGES,
  ADD_FUTURE_MESSAGE,
  DELETE_FUTURE_MESSAGE,
  UPDATE_FUTURE_MESSAGE,
  SET_CURRENT,
  CLEAR_CURRENT
} from "../types"

const futureMessagesReducer = (state, action) => {
  switch (action.type) {
    case GET_FUTURE_MESSAGES:
      return {
        ...state,
        futureMessages: action.payload
      }
    case ADD_FUTURE_MESSAGE:
      return {
        ...state,
        futureMessages: [...state.futureMessages, action.payload]
      }
    case DELETE_FUTURE_MESSAGE:
      return {
        ...state,
        futureMessages: state.futureMessages.filter(
          (futureMessage) => futureMessage.id !== action.payload
        )
      }
    case UPDATE_FUTURE_MESSAGE:
      return {
        ...state,
        futureMessages: state.futureMessages.map((futureMessage) =>
          futureMessage.id === action.payload.id
            ? action.payload
            : futureMessage
        )
      }
    case SET_CURRENT:
      return {
        ...state,
        current: action.payload
      }
    case CLEAR_CURRENT:
      return {
        ...state,
        current: null
      }
    default:
      return state
  }
}

export default futureMessagesReducer
