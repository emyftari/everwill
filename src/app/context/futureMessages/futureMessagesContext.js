import { createContext } from "react";

const futureMessagesContext = createContext();

export default futureMessagesContext;
