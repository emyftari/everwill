import { useReducer } from "react"

import axios from "axios"

import FutureMessagesContext from "./futureMessagesContext"
import futureMessagesReducer from "./futureMessagesReducer"

import {
  GET_FUTURE_MESSAGES,
  ADD_FUTURE_MESSAGE,
  DELETE_FUTURE_MESSAGE,
  UPDATE_FUTURE_MESSAGE,
  SET_CURRENT,
  CLEAR_CURRENT
} from "../types"

const FutureMessagesState = ({ children }) => {
  axios.defaults.baseURL = "https://mighty-peak-18791.herokuapp.com"
  axios.defaults.headers.common["Access-Control-Allow-Origin"] = "*"
  axios.defaults.headers.common["Authorization"] =
    "Bearer " + localStorage.token

  const initialState = {
    futureMessages: [],
    current: null
  }

  const [state, dispatch] = useReducer(futureMessagesReducer, initialState)

  const getFutureMessages = async () => {
    try {
      const res = await axios.get("/api/future-messages")
      dispatch({
        type: GET_FUTURE_MESSAGES,
        payload: res.data.data
      })
    } catch (error) {
      console.log(error.response)
    }
  }

  const addFutureMessage = async (data) => {
    try {
      if (!data.media) {
        const res = await axios.post("/api/future-messages", data)
        dispatch({
          type: ADD_FUTURE_MESSAGE,
          payload: res.data.data
        })
      } else {
        const attach = new FormData()
        attach.append('media', data.media)

        const response = await axios.post('/api/future-messages', data)
        const resWithAttach = await axios.post(`/api/future-messages/add-media/${response.data.data.id}`, attach)
        dispatch({
          type: ADD_FUTURE_MESSAGE,
          payload: resWithAttach.data.data
        })
      }
    } catch (error) {
      console.log(error.response)
    }
  }

  const deleteFutureMessage = async (id) => {
    try {
      await axios.delete(`/api/future-messages/${id}`)
      dispatch({
        type: DELETE_FUTURE_MESSAGE,
        payload: id
      })
    } catch (error) {
      console.log(error.response)
    }
  }

  const updateFutureMessage = async (data) => {
    try {
      if (!data.media) {
        const res = await axios.post(`/api/future-messages/${data.id}/update`, data)
        dispatch({
          type: UPDATE_FUTURE_MESSAGE,
          payload: res.data.data
        })
      } else {
        const attach = new FormData()
        attach.append('media', data.media)

        const res = await axios.post(`/api/future-messages/${data.id}/update`, attach)

        dispatch({
          type: UPDATE_FUTURE_MESSAGE,
          payload: res.data.data
        })
      }

    } catch (error) {
      console.log(error.response)
    }
  }

  const setCurrentFutureMessage = (futureMessage) => {
    dispatch({
      type: SET_CURRENT,
      payload: futureMessage
    })
  }

  const clearCurrentFutureMessage = () => dispatch({ type: CLEAR_CURRENT })

  return (
    <FutureMessagesContext.Provider
      value={{
        current: state.current,
        futureMessages: state.futureMessages,
        getFutureMessages,
        addFutureMessage,
        deleteFutureMessage,
        updateFutureMessage,
        setCurrentFutureMessage,
        clearCurrentFutureMessage
      }}
    >
      {children}
    </FutureMessagesContext.Provider>
  )
}

export default FutureMessagesState
