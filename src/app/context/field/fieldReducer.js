const fieldReducer = (state, action) => {
  switch (action.type) {
    case 'SET_ACTION_LABEL':
      return {
        ...state,
        action: action.payload.title,
        happen_next: action.payload.happen_next
      }
    case 'SET_SITE_LABEL':
      return {
        ...state,
        site: action.payload.name,
        siteLabel: action.payload.cat
      }
    case 'SET_CATEGORY_LABEL':
      return {
        ...state,
        category: action.payload
      }
    case 'SET_ACTION_ERROR':
      return {
        ...state,
        actionError: !state.actionError
      }
    case 'SET_SITE_ERROR':
      return {
        ...state,
        siteError: !state.siteError
      }
    case 'TOOGLE_NEW_SITE':
      return {
        ...state,
        newSite: !state.newSite
      }
    case 'CLEAR':
      return {
        ...state,
        action: null,
        actionError: false,
        site: null,
        siteLabel: null,
        siteError: false,
        category: null,
        happen_next: null
      }
    default:
      return state
  }
}

export default fieldReducer
