import { useReducer } from 'react'

import FieldContext from './fieldContext'

import fieldReducer from './fieldReducer'

const FieldState = ({ children }) => {
  const initalState = {
    action: null,
    actionError: false,
    site: null,
    siteLabel: null,
    siteError: false,
    category: null,
    happen_next: null,
    newSite: false
  }

  const [state, dispatch] = useReducer(fieldReducer, initalState)

  const setActionLabel = payload => dispatch({ type: 'SET_ACTION_LABEL', payload })

  const setSiteLabel = (name, cat) => dispatch({ type: 'SET_SITE_LABEL', payload: { name, cat } })

  const setCategoryLabel = payload => dispatch({ type: 'SET_CATEGORY_LABEL', payload })

  const setActionError = () => dispatch({ type: 'SET_ACTION_ERROR' })

  const setSiteError = () => dispatch({ type: 'SET_SITE_ERROR' })

  const clear = () => dispatch({ type: 'CLEAR' })

  const toggleNewSite = () => dispatch({ type: 'TOOGLE_NEW_SITE' })

  return (
    <FieldContext.Provider
      value={{
        action: state.action,
        actionError: state.actionError,
        site: state.site,
        siteLabel: state.siteLabel,
        siteError: state.siteError,
        category: state.category,
        happen_next: state.happen_next,
        newSite: state.newSite,
        setActionLabel,
        setActionError,
        setSiteLabel,
        setSiteError,
        setCategoryLabel,
        clear,
        toggleNewSite
      }}
    >
      {children}
    </FieldContext.Provider>
  )
}

export default FieldState
