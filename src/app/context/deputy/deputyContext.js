import { createContext } from 'react'

const deputyContext = createContext()

export default deputyContext
