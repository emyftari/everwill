import {
  DEPUTIES_LOADED,
  DEPUTY_ADDED,
  DEPUTY_DELETED,
  DEPUTY_UPDATED,
  GET_DEPUTY,
  INVITATIONS_RECIEVED
} from '../types'

const deputyReducer = (state, action) => {
  switch (action.type) {
    case DEPUTIES_LOADED:
      return {
        ...state,
        deputies: [...action.payload]
      }
    case GET_DEPUTY:
      return {
        ...state,
        deputy: action.payload,
        loading: false,
        sharingSites: action.payload.sharing_sites
      }
    case DEPUTY_ADDED:
      return {
        ...state,
        deputies: [...state.deputies, action.payload]
      }
    case DEPUTY_DELETED:
      return {
        ...state,
        deputies: state.deputies.filter(
          (deputy) => deputy.id !== action.payload
        )
      }
    case DEPUTY_UPDATED:
      return {
        ...state,
        deputies: state.deputies.map((deputy) =>
          deputy.id === action.payload.id ? action.payload : deputy
        )
      }
    case INVITATIONS_RECIEVED:
      return {
        ...state,
        invitations: action.payload
      }
    default:
      return state
  }
}

export default deputyReducer
