import { useReducer } from 'react'

import axios from 'axios'

import DeputyContext from './deputyContext'
import deputyReducer from './deputyReducer'

import {
  DEPUTY_ADDED,
  DEPUTIES_LOADED,
  GET_DEPUTY,
  DEPUTY_DELETED,
  DEPUTY_UPDATED,
  INVITATIONS_RECIEVED
} from '../types'

const DeputyState = ({ children }) => {
  axios.defaults.baseURL = 'https://mighty-peak-18791.herokuapp.com'
  axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
  axios.defaults.headers.common['Authorization'] =
    'Bearer ' + localStorage.token

  const initalState = {
    deputies: [],
    deputy: null,
    loading: true,
    currentDeputy: null,
    invitations: null
  }

  const [state, dispatch] = useReducer(deputyReducer, initalState)

  const loadDeputies = async () => {
    try {
      const res = await axios.get('/api/deputies')
      dispatch({ type: DEPUTIES_LOADED, payload: res.data.data })
    } catch (error) {
      console.error(error)
    }
  }

  const getDeputy = async (id) => {
    try {
      const res = await axios.get(`api/deputies/${id}`)
      dispatch({
        type: GET_DEPUTY,
        payload: res.data.data
      })
    } catch (error) {
      console.log(error.response)
    }
  }

  const addDeputy = async (data) => {
    const formData = {
      name: data.first_name,
      surname: data.last_name,
      email: data.email,
      relationship: data.relationship,
      give_right: data.give_right,
      notify: true
    }

    try {
      const res = await axios.post('/api/deputies', formData)

      dispatch({
        type: DEPUTY_ADDED,
        payload: res.data.data
      })
      loadDeputies()
    } catch (error) {
      console.error(error.response)
    }
  }

  const deleteDeputy = async (id) => {
    try {
      await axios.delete(`/api/deputies/${id}`)
      dispatch({ type: DEPUTY_DELETED, payload: id })
    } catch (error) {
      console.log(error.response)
    }
  }

  const updateDeputy = async (data) => {
    try {
      const res = await axios.post(`api/deputies/${data.id}`, {
        relationship: data.relationship,
        give_right: true
      })
      console.log(res)
      dispatch({ type: DEPUTY_UPDATED, payload: res.data.data })
    } catch (error) {
      console.log(error.response)
    }
  }

  const updateSharingSite = async ({ siteId, deputyId, time }) => {
    try {
      await axios.post(`/api/deputies/sharing-site/${deputyId}/${siteId}`, {
        time
      })
      getDeputy(deputyId)
    } catch (error) {
      console.log(error.response)
    }
  }

  const updateSharingDocuments = async ({ documentId, deputyId, time }) => {
    try {
      await axios.post(
        `/api/deputies/sharing-document/${deputyId}/${documentId}`,
        { time }
      )
    } catch (error) {
      console.log(error.response)
    }
  }

  const updateSharingDevice = async ({ deviceId, deputyId, time }) => {
    try {
      await axios.post(`/api/deputies/sharing-device/${deputyId}/${deviceId}`, {
        time
      })
    } catch (error) {
      console.log(error.response)
    }
  }

  const updateToShareSite = async ({ siteId, deputyId, time }) => {
    try {
      const res = await axios.post(`/api/share-site/${deputyId}/${siteId}`, {
        time
      })
      console.log(res)
    } catch (error) {
      console.log(error.response)
    }
  }

  const updateToShareDocument = async ({ documentId, deputyId, time }) => {
    try {
      const res = await axios.post(
        `/api/share-document/${deputyId}/${documentId}`,
        { time }
      )
      console.log(res)
    } catch (error) {
      console.log(error.response)
    }
  }

  const updateToShareDevice = async ({ deviceId, deputyId, time }) => {
    try {
      const res = await axios.post(
        `/api/share-device/${deputyId}/${deviceId}`,
        { time }
      )
      console.log(res)
    } catch (error) {
      console.log(error.response)
    }
  }

  const invitationsRecieved = async () => {
    try {
      const res = await axios.get('/api/invitations/received')
      dispatch({
        type: INVITATIONS_RECIEVED,
        payload: res.data.data
      })
    } catch (error) {
      console.log(error.response)
    }
  }

  const acceptInvite = async (id) => {
    try {
      await axios.post(`/api/deputies/accept/${id}`)
    } catch (error) {
      console.log(error.response)
    }
  }

  return (
    <DeputyContext.Provider
      value={{
        deputies: state.deputies,
        currentDeputy: state.currentDeputy,
        deputy: state.deputy,
        loading: state.loading,
        invitations: state.invitations,
        loadDeputies,
        addDeputy,
        deleteDeputy,
        updateDeputy,
        getDeputy,
        updateSharingSite,
        updateToShareSite,
        updateToShareDocument,
        updateToShareDevice,
        updateSharingDocuments,
        updateSharingDevice,
        invitationsRecieved,
        acceptInvite
      }}
    >
      {children}
    </DeputyContext.Provider>
  )
}

export default DeputyState
