import { useReducer } from 'react'

import axios from 'axios'

import ShareContext from './shareContext'
import shareReducer from './shareReducer'


const ShareState = ({ children }) => {
  axios.defaults.baseURL = 'https://mighty-peak-18791.herokuapp.com'
  axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
  axios.defaults.headers.common['Authorization'] =
    'Bearer ' + localStorage.token

  const initialState = {
    deputiesToShareWith: [],
    deputiesToShareWithIDs: []
  }

  const [state, dispatch] = useReducer(shareReducer, initialState)

  const addDeputyToShareWith = (deputy) => {
    dispatch({
      type: 'ADD_DEPUTY_TO_SHARE_WITH',
      payload: deputy
    })
  }

  const removeDeputyToShareWith = (id) => {
    dispatch({
      type: 'REMOVE_DEPUTY_TO_SHARE_WITH',
      payload: id
    })
  }

  const shareWith = (deputies) => {
    dispatch({
      type: 'ADD_DEPUTIES_IDS_TO_SHARE_WITH',
      payload: deputies
    })
  }

  const shareHomeSite = async (id, deputies) => {
    try {
      const res = await axios.post(`/api/share-site-home/${id}`, { deputies })
      console.log(res.data.data)
    } catch (error) {
      console.log(error.response)
    }
  }

  const shareHomeNotes = async (id, deputies) => {
    try {
      const res = await axios.post(`/api/share-document-home/${id}`, { deputies })
      console.log(res.data.data)
    } catch (error) {
      console.log(error.response)
    }
  }

  const shareHomeDevices = async (id, deputies) => {
    try {
      await axios.post(`/api/share-device-home/${id}`, { deputies })
    } catch (error) {
      console.log(error.response)
    }
  }

  return (
    <ShareContext.Provider
      value={{
        deputiesToShareWith: state.deputiesToShareWith,
        deputiesToShareWithIDs: state.deputiesToShareWithIDs,
        addDeputyToShareWith,
        removeDeputyToShareWith,
        shareWith,
        shareHomeSite,
        shareHomeNotes,
        shareHomeDevices
      }}
    >
      {children}
    </ShareContext.Provider>
  )
}

export default ShareState
