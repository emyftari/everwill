const shareReducer = (state, action) => {
  switch (action.type) {
    case 'ADD_DEPUTY_TO_SHARE_WITH':
      return {
        ...state,
        deputiesToShareWith: [...state.deputiesToShareWith, action.payload]
      }
    case 'REMOVE_DEPUTY_TO_SHARE_WITH':
      return {
        ...state,
        deputiesToShareWith: state.deputiesToShareWith.filter(deputy => deputy.id !== action.payload)
      }
    case 'ADD_DEPUTIES_IDS_TO_SHARE_WITH':
      return {
        ...state,
        deputiesToShareWithIDs: [...new Set([...state.deputiesToShareWithIDs, ...action.payload])]
      }
    default:
      return state
  }
}

export default shareReducer