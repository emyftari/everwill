import { SET_CURRENT, SITES_LOADED, SITE_ADDED, SITE_DELETED, SITE_UPDATED, CLEAR_CURRENT } from '../types'

const siteReducer = (state, action) => {
  switch (action.type) {
    case SITES_LOADED:
      return {
        ...state,
        sites: [...action.payload]
      }
    case SITE_ADDED:
      return {
        ...state,
        sites: [...state.sites, action.payload]
      }
    case SITE_DELETED:
      return {
        ...state,
        sites: state.sites.filter(site => site.id !== action.payload)
      }
    case SITE_UPDATED:
      return {
        ...state,
        sites: state.sites.map(site => site.id === action.payload.id ? action.payload : site)
      }
    case SET_CURRENT:
      return {
        ...state,
        currentSite: action.payload
      }
    case CLEAR_CURRENT:
      return {
        ...state,
        currentSite: null
      }
    default:
      return state
  }
}

export default siteReducer
