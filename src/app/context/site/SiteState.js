import { useReducer } from 'react'
import axios from 'axios'

import SiteContext from './siteContext'
import siteReducer from './siteReducer'

import {
  SET_CURRENT,
  SITES_LOADED,
  SITE_ADDED,
  SITE_DELETED,
  SITE_UPDATED,
  CLEAR_CURRENT
} from '../types'

const SiteState = ({ children }) => {
  axios.defaults.baseURL = 'https://mighty-peak-18791.herokuapp.com'
  axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.token

  const initalState = {
    sites: [],
    currentSite: null
  }

  const [state, dispatch] = useReducer(siteReducer, initalState)

  const loadSites = async () => {
    try {
      const res = await axios.get('/api/my-sites')
      dispatch({
        type: SITES_LOADED,
        payload: res.data.data
      })
    } catch (error) {
      console.error(error)
    }
  }

  const addSite = async (data) => {
    const formData = {
      name: data.site,
      type: 'new',
      category: data.siteLabel,
      happen_next: data.happen_next,
      profile_link: data.profile_link,
      site_url: data.site_url,
      site_title: data.site_title,
      username: data.username,
      notes: data.notes,
      existing_deputies: data.deputiesToShareWithIDs
    }

    try {
      if (!data.media) {
        const response = await axios.post('/api/sites', formData)

        dispatch({
          type: SITE_ADDED,
          payload: response.data.data
        })
      } else {
        const attach = new FormData()
        attach.append('media', data.media)

        const response = await axios.post('/api/sites', formData)
        const resWithAttach = await axios.post(`/api/sites/add-media/${response.data.data.id}`, attach)

        dispatch({
          type: SITE_ADDED,
          payload: resWithAttach.data.data
        })
      }
    } catch (error) {
      console.error(error.response)
    }
  }

  const deleteSite = async (id) => {
    try {
      await axios.delete(`api/sites/${id}`)
      dispatch({ type: SITE_DELETED, payload: id })
    } catch (error) {
      console.log(error.response)
    }
  }

  const updateSite = async (data) => {
    try {
      const formData = {
        name: data.site,
        type: 'new',
        category: data.siteLabel,
        happen_next: data.happen_next,
        profile_link: data.profile_link,
        username: data.username,
        notes: data.notes,
        site_url: data.site_url,
        site_title: data.site_title
      }

      if (!data.media) {
        const response = await axios.post(`/api/sites/${data.id}/update`, formData)

        dispatch({
          type: SITE_UPDATED,
          payload: response.data.data
        })
      } else {
        const attach = new FormData()
        attach.append('media', data.media)

        const response = await axios.post(`/api/sites/add-media/${data.id}`, attach)

        dispatch({
          type: SITE_UPDATED,
          payload: response.data.data
        })
      }
    } catch (error) {
      console.log(error.response)
    }
  }

  const setCurrentSite = (site) => dispatch({ type: SET_CURRENT, payload: site })

  const clearCurrent = () => dispatch({ type: CLEAR_CURRENT })

  return (
    <SiteContext.Provider
      value={{
        sites: state.sites,
        currentSite: state.currentSite,
        loadSites,
        addSite,
        deleteSite,
        updateSite,
        setCurrentSite,
        clearCurrent
      }}
    >
      {children}
    </SiteContext.Provider>
  )
}

export default SiteState
