import { useState } from 'react'

import MediaContext from './mediaContext'

const MediaState = ({ children }) => {
  const [media, setMedia] = useState(null)

  return (
    <MediaContext.Provider value={{ media, setMedia }}>
      {children}
    </MediaContext.Provider>
  )
}

export default MediaState
