import { useReducer } from 'react'

import axios from 'axios'

import UserContext from './userContext'
import userReducer from './userReducer'

import {
  ENABLE_DEADMAN,
  DISABLE_DEADMAN,
  GET_DEADMAN,
  GET_CARD,
  ADD_CARD,
  DELETE_CARD,
  GET_SHARED_WITH_ME,
  GET_PEOPLE_SHARING_WITH_ME
} from '../types'

const UserState = ({ children }) => {
  axios.defaults.baseURL = 'https://mighty-peak-18791.herokuapp.com'
  axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
  axios.defaults.headers.common['Authorization'] =
    'Bearer ' + localStorage.token

  const initalState = {
    deadman: null,
    creditCard: null,
    itemsSharedWithMe: null,
    peopleSharingWithMe: null
  }

  const [state, dispatch] = useReducer(userReducer, initalState)

  const cardDataFormat = string => parseInt(string.replace(/\s+/g, ''))

  const getDeadManSwitch = async () => {
    try {
      const response = await axios.get('/api/dead-man-switch/personal')
      dispatch({
        type: GET_DEADMAN,
        payload: response.data.data
      })
    } catch (error) {
      console.log(error.response)
    }
  }

  const enableDeadManSwitch = async ({ times, often }) => {
    try {
      const response = await axios.post('/api/dead-man-switch', {
        times,
        often
      })
      dispatch({
        type: ENABLE_DEADMAN,
        payload: response.data.data
      })
    } catch (error) {
      console.log(error.response)
    }
  }

  const disableDeadManSwitch = async () => {
    try {
      await axios.post('/api/dead-man-switch/unset')
      dispatch({
        type: DISABLE_DEADMAN
      })
    } catch (error) {
      console.log(error.response)
    }
  }

  const changeName = async (data) => {
    try {
      await axios.post('/api/users', data)
    } catch (error) {
      console.log(error.response)
    }
  }

  const addPhoneNumber = async (data) => {
    try {
      await axios.post('/api/users', data)
    } catch (error) {
      console.log(error.response)
    }
  }

  const changeEmail = async (data) => {
    try {
      await axios.post('/api/users', data)
    } catch (error) {
      console.log(error.response)
    }
  }

  const changeAddress = async (data) => {
    try {
      await axios.post('/api/users', data)
    } catch (error) {
      console.log(error.response)
    }
  }

  const changeInformation = async (data) => {
    try {
      await axios.post('/api/users', data)
    } catch (error) {
      console.log(error.response)
    }
  }

  const getCreditCard = async () => {
    try {
      const res = await axios.get('/api/users/card-info')
      dispatch({
        type: GET_CARD,
        payload: res.data.data
      })
    } catch (error) {
      console.log(error)
    }
  }

  const addCreditCard = async (data) => {
    const formData = {
      card_number: cardDataFormat(data.card_number),
      zip_code: cardDataFormat(data.zip_code),
      card_date: data.card_date,
      card_cvc: parseInt(data.card_cvc),
      default_method: 1
    }

    try {
      const res = await axios.post('/api/add-card', formData)
      dispatch({
        type: ADD_CARD,
        payload: res.data.data
      })
    } catch (error) {
      console.log(error.response)
    }
  }

  const deleteCreditCard = async () => {
    try {
      await axios.post('api/delete-card')
      dispatch({ type: DELETE_CARD })
    } catch (error) {
      console.log(error)
    }
  }

  const changePassword = async (data) => {
    try {
      await axios.post('/api/auth/change-password', data)
    } catch (error) {
      console.log(error.response)
    }
  }

  const addProfileImage = async (data) => {
    if (data) {
      const formData = new FormData()
      formData.append('media', data)

      try {
        await axios.post('/api/users/add-photo', formData)
      } catch (error) {
        console.log(error.response)
      }
    } else {
      try {
        await axios.post('/api/users/remove-photo')
      } catch (error) {
        console.log(error.response)
      }
    }
  }

  const deleteAccount = async () => {
    try {
      const res = await axios.post('/api/users/delete-account')
      console.log(res.data)
    } catch (error) {
      console.log(error.response)
    }
  }

  const sharedWithMe = async () => {
    try {
      const res = await axios.get('/api/all-shared-grouped')
      dispatch({
        type: GET_SHARED_WITH_ME,
        payload: res.data.data
      })
    } catch (error) {
      console.log(error.response)
    }
  }

  const getPeopleSharingWithMe = async () => {
    try {
      const res = await axios.get('/api/shared-with-me-all')
      dispatch({
        type: GET_PEOPLE_SHARING_WITH_ME,
        payload: res.data.data
      })
    } catch (error) {
      console.log(error.response)
    }
  }

  return (
    <UserContext.Provider
      value={{
        deadman: state.deadman,
        creditCard: state.creditCard,
        peopleSharingWithMe: state.peopleSharingWithMe,
        itemsSharedWithMe: state.itemsSharedWithMe,
        getDeadManSwitch,
        enableDeadManSwitch,
        disableDeadManSwitch,
        changeName,
        changeEmail,
        changeAddress,
        changeInformation,
        getCreditCard,
        addCreditCard,
        deleteCreditCard,
        changePassword,
        addProfileImage,
        deleteAccount,
        sharedWithMe,
        addPhoneNumber,
        getPeopleSharingWithMe
      }}
    >
      {children}
    </UserContext.Provider>
  )
}

export default UserState
