import {
  GET_DEADMAN,
  ENABLE_DEADMAN,
  DISABLE_DEADMAN,
  GET_CARD,
  ADD_CARD,
  DELETE_CARD,
  GET_SHARED_WITH_ME,
  GET_PEOPLE_SHARING_WITH_ME
} from '../types'

const deputyReducer = (state, action) => {
  switch (action.type) {
    case GET_DEADMAN:
      return {
        ...state,
        deadman: action.payload
      }
    case ENABLE_DEADMAN:
      return {
        ...state,
        deadman: action.payload
      }
    case DISABLE_DEADMAN:
      return {
        ...state,
        deadman: null
      }
    case GET_CARD:
      return {
        ...state,
        creditCard: action.payload
      }
    case ADD_CARD:
      return {
        ...state,
        creditCard: action.payload
      }
    case DELETE_CARD:
      return {
        ...state,
        creditCard: null
      }
    case GET_SHARED_WITH_ME:
      return {
        ...state,
        itemsSharedWithMe: action.payload
      }
    case GET_PEOPLE_SHARING_WITH_ME:
      return {
        ...state,
        peopleSharingWithMe: action.payload
      }
    default:
      return state
  }
}

export default deputyReducer
