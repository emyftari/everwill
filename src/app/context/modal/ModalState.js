import { useReducer, useState } from 'react'

import ModalContext from './modalContext'

import modalReducer from './modalReducer'

const ModalState = ({ children }) => {
  const initalState = {
    openActionModal: false,
    openSiteModal: false,
    openCategoryModal: false,
    profileImageModal: false
  }

  const [profileImageModal, setProfileImageModal] = useState(false)
  const [changeAddressModal, setChangeAddressModal] = useState(false)
  const [changeEmailModal, setChangeEmailModal] = useState(false)
  const [changeInformationModal, setChangeInformationModal] = useState(false)
  const [changeNameModal, setChangeNameModal] = useState(false)
  const [openDeputyModal, setOpenDeputyModal] = useState(false)
  const [openShareWithModal, setOpenShareWithModal] = useState(false)
  const [openShareWithDirectlyModal, setOpenShareWithDirectlyModal] = useState(false)
  const [open2fa, setOpen2fa] = useState(false)
  const [openCreditCard, setOpenCreditCard] = useState(false)
  const [openChangePassword, setOpenChangePassword] = useState(false)
  const [sharingItem, setSharingItem] = useState(null)

  const [state, dispatch] = useReducer(modalReducer, initalState)

  const openAction = () => dispatch({ type: 'OPEN_ACTION_MODAL' })

  const openSite = () => dispatch({ type: 'OPEN_SITE_MODAL' })

  const openCategory = () => dispatch({ type: 'OPEN_CATEGORY_MODAL' })

  const setId = ({ id, item }) => setSharingItem({ id, item })

  const openModal = (modal) => {
    switch (modal) {
      case 'Profile image':
        setProfileImageModal(!profileImageModal)
        break
      case 'Name':
        setChangeNameModal(!changeNameModal)
        break
      case 'Personal e-mail':
        setChangeEmailModal(!changeEmailModal)
        break
      case 'Address':
        setChangeAddressModal(!changeAddressModal)
        break
      case 'About you':
        setChangeInformationModal(!changeInformationModal)
        break
      case 'Deputy':
        setOpenDeputyModal(!openDeputyModal)
        setOpenShareWithModal(false)
        break
      case 'Share with':
        setOpenShareWithModal(!openShareWithModal)
        break
      case 'Share directly':
        setOpenShareWithDirectlyModal(!openShareWithDirectlyModal)
        break
      case 'Two step verification':
        setOpen2fa(!open2fa)
        break
      case 'Add credit card':
        setOpenCreditCard(!openCreditCard)
        break
      case 'Your Password':
        setOpenChangePassword(!openChangePassword)
        break
      default:
        break
    }
  }

  const close = () => {
    setProfileImageModal(false)
    setChangeNameModal(false)
    setChangeEmailModal(false)
    setChangeAddressModal(false)
    setChangeInformationModal(false)
    setOpenDeputyModal(false)
    setOpenShareWithModal(false)
    setOpen2fa(false)
    setOpenCreditCard(false)
    setOpenChangePassword(false)
    setOpenShareWithDirectlyModal(false)
  }



  const closeModal = () => dispatch({ type: 'CLOSE_MODAL' })

  return (
    <ModalContext.Provider
      value={{
        openActionModal: state.openActionModal,
        openSiteModal: state.openSiteModal,
        openCategoryModal: state.openCategoryModal,
        sharingItem,
        profileImageModal,
        openAction,
        openSite,
        openCategory,
        closeModal,
        openModal,
        changeNameModal,
        changeEmailModal,
        changeAddressModal,
        changeInformationModal,
        openDeputyModal,
        open2fa,
        openShareWithModal,
        openCreditCard,
        openChangePassword,
        openShareWithDirectlyModal,
        close,
        setId
      }}
    >
      {children}
    </ModalContext.Provider>
  )
}

export default ModalState
