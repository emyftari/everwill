const modalReducer = (state, action) => {
  switch (action.type) {
    case 'OPEN_ACTION_MODAL':
      return {
        ...state,
        openActionModal: true
      }
    case 'OPEN_SITE_MODAL':
      return {
        ...state,
        openSiteModal: true
      }
    case 'OPEN_CATEGORY_MODAL':
      return {
        ...state,
        openCategoryModal: true
      }
    case 'CLOSE_MODAL':
      return {
        ...state,
        openActionModal: false,
        openSiteModal: false,
        openCategoryModal: false,
      }
    default:
      return state
  }
}

export default modalReducer
