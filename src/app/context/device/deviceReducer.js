import { SET_CURRENT, DEVICES_LOADED, DEVICE_ADDED, DEVICE_DELETED, DEVICE_UPDATED, CLEAR_CURRENT, SET_PATH, RESET_PATH } from '../types'

const siteReducer = (state, action) => {
  switch (action.type) {
    case DEVICES_LOADED:
      return {
        ...state,
        devices: [...action.payload]
      }
    case DEVICE_ADDED:
      return {
        ...state,
        devices: [...state.devices, action.payload]
      }
    case DEVICE_DELETED:
      return {
        ...state,
        devices: state.devices.filter(device => device.id !== action.payload)
      }
    case DEVICE_UPDATED:
      return {
        ...state,
        devices: state.devices.map(device => device.id === action.payload.id ? action.payload : device)
      }
    case SET_CURRENT:
      return {
        ...state,
        currentDevice: action.payload
      }
    case CLEAR_CURRENT:
      return {
        ...state,
        currentDevice: null
      }
    case SET_PATH:
      return {
        ...state,
        path: action.payload.join('')
      }
    case RESET_PATH:
      return {
        ...state,
        path: null
      }
    default:
      return state
  }
}

export default siteReducer
