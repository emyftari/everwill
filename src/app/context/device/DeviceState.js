import { useReducer } from 'react'

import axios from 'axios'

import DeviceContext from './deviceContext'
import deviceReducer from './deviceReducer'

import {
  SET_CURRENT,
  DEVICES_LOADED,
  DEVICE_ADDED,
  DEVICE_DELETED,
  DEVICE_UPDATED,
  CLEAR_CURRENT,
  SET_PATH,
  RESET_PATH
} from '../types'

const DeviceState = ({ children }) => {
  axios.defaults.baseURL = 'https://mighty-peak-18791.herokuapp.com'
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.token

  const initalState = {
    devices: [],
    currentDevice: null,
    path: null
  }

  const [state, dispatch] = useReducer(deviceReducer, initalState)

  const loadDevices = async () => {
    try {
      const res = await axios.get('/api/my-devices')
      dispatch({ type: DEVICES_LOADED, payload: res.data.data })
    } catch (error) {
      console.error(error)
    }
  }

  const addDevice = async (data) => {
    const formData = {
      name: data.device_name,
      password: data.password,
      password_type: data.password_type,
      example: data.notes
    }

    try {
      const res = await axios.post('/api/devices', formData)

      dispatch({
        type: DEVICE_ADDED,
        payload: res.data.data
      })

    } catch (error) {
      console.error(error.response)
    }
  }

  const deleteDevice = async (id) => {
    try {
      await axios.delete(`api/devices/${id}`)
      dispatch({ type: DEVICE_DELETED, payload: id })
    } catch (error) {
      console.log(error.response)
    }
  }

  const updateDevice = async (data) => {
    try {
      const formData = {
        name: data.device_name,
        password: data.password,
        password_type: 'passcode',
        example: data.notes
      }
      const res = await axios.post(`/api/devices/${data.id}/update`, formData)
      dispatch({ type: DEVICE_UPDATED, payload: res.data.data })
    } catch (error) {
      console.log(error.response)
    }
  }

  const setCurrentDevice = (note) => dispatch({ type: SET_CURRENT, payload: note })

  const clearCurrentDevice = () => dispatch({ type: CLEAR_CURRENT })

  const onFinish = (path) => dispatch({ type: SET_PATH, payload: path })

  const onReset = () => dispatch({ type: RESET_PATH })

  return (
    <DeviceContext.Provider
      value={{
        devices: state.devices,
        currentDevice: state.currentDevice,
        path: state.path,
        loadDevices,
        addDevice,
        deleteDevice,
        updateDevice,
        setCurrentDevice,
        clearCurrentDevice,
        onFinish,
        onReset
      }}
    >
      {children}
    </DeviceContext.Provider>
  )
}

export default DeviceState

