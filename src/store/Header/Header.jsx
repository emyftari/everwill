import { NavLink } from 'react-router-dom'

import { LogoSimple, ChevronRight } from '../assets/Icons'

import {
  header__container,
  header__link__container,
  logo__link,
  header__link,
  active
} from './Header.module.css'

const Header = () => {
  return (
    <div className={header__container}>
      <NavLink to="" className={logo__link}>
        <LogoSimple />
      </NavLink>
      <div className={header__link__container}>
        <NavLink
          exact
          to="/store/site"
          activeClassName={active}
          className={header__link}
        >
          Select sites &amp; actions
        </NavLink>
        <div>
          <ChevronRight />
        </div>
        <NavLink
          to="/store/checkout"
          activeClassName={active}
          className={header__link}
        >
          Checkout
        </NavLink>
        <div>
          <ChevronRight />
        </div>
        <NavLink
          to="/store/upload"
          activeClassName={active}
          className={header__link}
        >
          Upload &amp; sign documents
        </NavLink>
        <div>
          <ChevronRight />
        </div>
        <NavLink
          to="/store/finish"
          activeClassName={active}
          className={header__link}
        >
          Everwill makes it happen
        </NavLink>
      </div>
    </div>
  )
}

export default Header
