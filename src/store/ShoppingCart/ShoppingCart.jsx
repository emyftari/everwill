import { useContext, useEffect, useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'

import StoreContext from '../context/store/storeContext'

import { ShoppingBag, Cross } from '../assets/Icons'

import {
  cart__container,
  cart__content,
  cart__button,
  cart__content__filled,
  cart__title,
  cart__item__container,
  cart__item,
  cart__item__primary,
  cart__item__secondary,
  cart__footer,
  cart__submit,
  mobile__only
} from './ShoppingCart.module.css'


const ShoppingCart = () => {
  const history = useHistory()
  const location = useLocation()
  const [openCart, setOpenCart] = useState(false)
  const [total, setTotal] = useState(0)
  const { items } = useContext(StoreContext)

  useEffect(() => {
    if (items.length > 0) {
      setOpenCart(true)
      const total = items
        .map((item) => parseFloat(item.price))
        .reduce((acc, curr) => acc + curr)
      setTotal(total)
    } else {
      setOpenCart(false)
    }
  }, [items])

  const handleClick = () => {
    if (location.pathname === '/store') {
      history.push('/store/checkout')
    }
  }

  return (
    <div className={cart__container}>
      {!openCart ? (
        <div className={cart__content}>
          <button className={cart__button}>
            <span>
              <ShoppingBag />
            </span>
          </button>
        </div>
      ) : (
        <div className={cart__content__filled}>
          <p className={cart__title}>
            <ShoppingBag />
            Your order
          </p>
          <div className={cart__item__container}>
            {items.map((item) => (
              <CartItem {...item} key={item.cartId} />
            ))}
          </div>
          <div className={cart__footer}>
            <p className={cart__item__primary} style={{ marginBottom: 15 }}>
              <span>Total</span>
              <span>${total}</span>
            </p>
            <div className={cart__submit}>
              <button type='submit' onClick={handleClick}>Continue to checkout</button>
            </div>
          </div>
        </div>
      )}
      <div className={mobile__only}>
        <button onClick={() => history.push('/store/checkout')}>Continue to checkout</button>
      </div>
    </div>
  )
}

const CartItem = ({ site, price, type, cartId }) => {
  const { removeItem } = useContext(StoreContext)

  return (
    <div className={cart__item}>
      <p className={cart__item__primary}>
        <span>{site}</span>
        <span>${price}</span>
      </p>
      <p className={cart__item__secondary}>
        <span>{type}</span>
        <span onClick={() => removeItem(cartId)}>
          <Cross />
        </span>
      </p>
    </div>
  )
}

export default ShoppingCart
