import { BrowserRouter as Router } from 'react-router-dom'

import StoreState from './context/store/StoreState'

import Main from './Main/Main'
import ShoppingCart from './ShoppingCart/ShoppingCart'

const Store = () => {
  return (
    <Router>
      <StoreState>
        <div style={{ display: 'flex', flexDirection: 'row', height: '100%' }}>
          <Main />
          <ShoppingCart />
        </div>
      </StoreState>
    </Router>
  )
}

export default Store
