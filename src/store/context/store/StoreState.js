import { useReducer } from 'react'

import StoreContext from './storeContext'
import storeReducer from './storeReducer'

import { ADD_ITEM, REMOVE_ITEM } from '../types'
import axios from 'axios'

const StoreState = ({ children }) => {
  const initialState = {
    items: []
  }

  const [state, dispatch] = useReducer(storeReducer, initialState)

  const addItem = (item) => {
    dispatch({
      type: ADD_ITEM,
      payload: item
    })
  }

  const removeItem = (id) => {
    dispatch({
      type: REMOVE_ITEM,
      payload: id
    })
  }

  const buyLovedOne = async (data) => {
    // const total = state.items
    //   .map((item) => parseFloat(item.price))
    //   .reduce((acc, curr) => acc + curr)

    const formData = {
      card_number: data.card_number,
      card_date: '12/31',
      card_cvc: data.card_cvc,
      remember_card: false,
      zip_code: data.zip_code,
      token: 'token',
      action: state.items[0].type,
      site: state.items[0].site,
      price: state.items[0].price
    }

    try {
      const res = await axios.post('/api/loved-ones/buy', formData)
      console.log(res)
    } catch (error) {
      console.log(error.response)
    }
  }

  return (
    <StoreContext.Provider value={{ items: state.items, addItem, removeItem, buyLovedOne }}>
      {children}
    </StoreContext.Provider>
  )
}

export default StoreState
