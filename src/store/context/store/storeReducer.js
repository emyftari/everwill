import { ADD_ITEM, REMOVE_ITEM } from '../types'

const storeReducer = (state, action) => {
  switch (action.type) {
    case ADD_ITEM:
      const conditionName = state.items.some(item => item.site === action.payload.site)
      const conditionType = state.items.some(item => item.type === action.payload.type)
      if (!conditionType || !conditionName) {
        return {
          ...state,
          items: [...state.items, action.payload]
        }
      }
      return state
    case REMOVE_ITEM:
      return {
        ...state,
        items: state.items.filter((item) => item.cartId !== action.payload)
      }
    default:
      return state
  }
}

export default storeReducer
