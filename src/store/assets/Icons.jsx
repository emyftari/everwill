export const ShoppingBag = () => (
  <svg
    width="16"
    height="17"
    viewBox="0 0 16 17"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect
      x="1"
      y="4.706"
      width="14"
      height="11.294"
      rx="2"
      stroke="#000"
      strokeWidth="2"
    ></rect>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M8 0C5.79 0 4 1.686 4 3.765h2c0-1.04.895-1.883 2-1.883s2 .843 2 1.883h2C12 1.685 10.21 0 8 0z"
      fill="#000"
    ></path>
  </svg>
)
export const Cross = () => (
  <svg
    width="14"
    height="14"
    viewBox="0 0 14 14"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M11 3l-8 8M11 11L3 3" stroke="#657795" strokeWidth="1.5"></path>
  </svg>
)
export const LogoSimple = () => (
  <svg
    width="42"
    height="37"
    viewBox="0 0 42 37"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M19.984 34.435l-.228.26c-1.258 1.26-3.422.367-3.422-1.416 0-.516.196-1.027.586-1.416l8.491-8.491c.61-.614-.345-1.57-.955-.956l-8.492 8.492c-.793.79-1.938.721-2.679.121-.774-.735-1.08-2.047-.214-2.913l6.585-6.585 1.448-1.448c.61-.61-.347-1.566-.956-.956L18.7 20.576l-4.822 4.822-1.762 1.762a1.995 1.995 0 01-1.416.585c-1.782 0-2.675-2.16-1.42-3.418l8.032-8.033c.613-.609-.343-1.565-.956-.955l-8.032 8.033a1.995 1.995 0 01-1.416.584c-1.782 0-2.675-2.162-1.416-3.42 3.372-3.373 2.945-2.934 6.048-6.017.662-.662 2.57-2.502 3.232-3.164.613-.609-.343-1.565-.956-.955l-2.833 2.833-6.447 6.346-.098.107s-.46.451-.713 1.204c-.786 2.351 1.177 4.732 3.651 4.386a3.356 3.356 0 003.792 3.79c-.312 2.23 1.698 4.137 3.846 3.783-.317 2.457 2.048 4.396 4.388 3.613.752-.254 1.204-.713 1.204-.713l.106-.098.228-.26c.61-.613-.346-1.569-.956-.956zM22.355 3.763l1.168-.817c4.16-2.544 9.665-2.04 13.275 1.573 4.22 4.22 4.22 11.064 0 15.285l-.094.09c-.614.61.342 1.566.955.957l.094-.091c4.748-4.748 4.748-12.45 0-17.197-4.386-4.386-11.286-4.72-16.06-.998l.004.005c-.039.029-.078.054-.117.086-.669.547.065 1.6.775 1.107z"
      fill="#007DBC"
    ></path>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M3.835 19.058l-.276-.412c-.017-.024-.031-.049-.048-.072l-.56-.835C.437 13.58.969 8.104 4.552 4.52 8.746.326 15.522.294 19.755 4.432l.92.92 3.065 3.06a2.643 2.643 0 01-.105 3.83c-1.058.954-2.718.804-3.726-.204-1.013-1.013-.87-.867-1.51-1.504-.661-.662-1.335-1.347-1.997-2.01-.61-.612-1.565.344-.955.957l1.93 1.931h.002l1.677 1.685a3.988 3.988 0 005.88-.262c1.353-1.61 1.1-4.033-.386-5.52L21.63 4.397l-.947-.943-.007-.01h-.002C15.17-1.913 5.854-1.016 1.622 6.117-.64 9.925-.47 14.595 1.716 18.299l-.007.003c.06.102.127.197.19.296.08.127.159.255.244.379.19.28.383.555.569.834.479.72 1.602-.032 1.123-.753zM36.179 25.425l-.008.008c-.641.64-1.76.64-2.401 0l-2.42-2.42a1.702 1.702 0 01.008-2.408 1.682 1.682 0 011.2-.497c.454 0 .88.176 1.202.497l2.419 2.42c.32.32.497.747.497 1.2 0 .453-.177.88-.497 1.2zm-3.236 3.236l-.008.007c-.641.642-1.76.642-2.401 0l-2.42-2.42a1.702 1.702 0 01.008-2.407 1.687 1.687 0 011.2-.498c.455 0 .88.177 1.202.498l2.419 2.419c.32.32.497.747.497 1.2 0 .454-.177.88-.497 1.2zm-3.236 3.236l-.007.007c-.641.642-1.76.642-2.401 0l-2.42-2.42a1.704 1.704 0 01.008-2.408c.32-.321.747-.497 1.2-.497.454 0 .88.176 1.2.497l2.42 2.419c.32.322.498.748.498 1.2 0 .455-.177.88-.498 1.202zm-3.236 3.236l-.007.007c-.641.64-1.76.64-2.401 0l-2.42-2.42a1.704 1.704 0 01.008-2.408c.32-.321.747-.497 1.2-.497.454 0 .88.176 1.2.497l2.42 2.42c.32.32.498.747.498 1.2 0 .454-.177.88-.498 1.2zm8.114-15.354c-1.083-1.083-2.963-1.09-4.06.007a2.853 2.853 0 00-.797 2.45c-.87-.116-1.79.137-2.44.786a2.859 2.859 0 00-.796 2.452c-.87-.117-1.79.135-2.44.784a2.859 2.859 0 00-.796 2.45c-.87-.116-1.79.137-2.438.785a2.868 2.868 0 000 4.053l2.419 2.42c.54.54 1.26.84 2.026.84a2.84 2.84 0 002.02-.835l.014-.013c.542-.541.84-1.261.84-2.026 0-.136-.021-.27-.04-.402.133.019.266.04.402.04a2.84 2.84 0 002.02-.834l.014-.013c.542-.541.84-1.261.84-2.027 0-.136-.021-.27-.04-.401.133.019.266.039.402.039.765 0 1.486-.3 2.02-.835l.014-.012c.54-.541.84-1.261.84-2.026 0-.137-.021-.27-.04-.402.133.018.266.039.402.039.765 0 1.485-.298 2.02-.834l.014-.013c.54-.541.84-1.261.84-2.026 0-.766-.3-1.486-.84-2.027l-2.42-2.42z"
      fill="#007DBC"
    ></path>
  </svg>
)
export const ChevronRight = () => (
  <svg
    width="36"
    height="36"
    viewBox="0 0 36 36"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M14 26l8-8-8-8" stroke="#B2BBCA" strokeWidth="2"></path>
  </svg>
)
export const LeftArrow = () => (
  <svg
    width="22"
    height="15"
    viewBox="0 0 22 15"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M8.621 1.414L7.207 0 0 7.207l7.207 7.207L8.621 13 3.828 8.207h17.586v-2H3.83L8.62 1.414z"
      fill="#007DBC"
    ></path>
  </svg>
)
export const Delete = () => (
  <svg
    width="36"
    height="36"
    viewBox="0 0 36 36"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      clipRule="evenodd"
      d="M9.5 29.5h16v-20h-16v20zM13.5 9.5h8v-3h-8v3z"
      stroke="currentColor"
      strokeLinecap="round"
      strokeLinejoin="round"
    ></path>
    <path
      d="M7 9.5h22M13.5 13v12M17.5 13v12M21.5 13v12"
      stroke="currentColor"
      strokeLinecap="round"
      strokeLinejoin="round"
    ></path>
  </svg>
)
export const Tick = () => (
  <svg
    width="15"
    height="11"
    viewBox="0 0 15 11"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M14.694.28a1 1 0 01.026 1.414l-8.666 9a1 1 0 01-1.441 0L.28 6.194a1 1 0 111.44-1.388l3.613 3.752L13.28.306A1 1 0 0114.694.28z"
      fill="currentColor"
    ></path>
  </svg>
)

export const Card = () => (
  <svg
    width="36"
    height="36"
    viewBox="0 0 36 36"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect
      x="7.5"
      y="11.5"
      width="20"
      height="13"
      rx="1.9"
      stroke="currentColor"
    ></rect>
    <rect
      x="12"
      y="15"
      width="16"
      height="1"
      rx="0.5"
      fill="currentColor"
    ></rect>
  </svg>
)

export const Download = () => (
  <svg
    width="36"
    height="36"
    viewBox="0 0 36 36"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M17.5 25.5V11M23 20l-5.5 5.5L12 20M23.5 6.5h6v23h-6"
      stroke="currentColor"
      strokeLinecap="round"
      strokeLinejoin="round"
    ></path>
    <path
      d="M12.5 6.5h-6v23h6"
      stroke="currentColor"
      strokeLinecap="round"
      strokeLinejoin="round"
    ></path>
  </svg>
)

export const CheckShield = () => (
  <svg
    width="36"
    height="36"
    viewBox="0 0 36 36"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M23.501 13l-7.5 7-2.5-2.5"
      stroke="currentColor"
      strokeLinecap="round"
      strokeLinejoin="round"
    ></path>
    <path
      clipRule="evenodd"
      d="M8.5 6.5h19v7.264A17.784 17.784 0 0118 29.5a17.778 17.778 0 01-9.5-15.736V6.5v0z"
      stroke="currentColor"
      strokeLinejoin="round"
    ></path>
  </svg>
)

export const CrossArrow = () => (
  <svg
    width="36"
    height="36"
    viewBox="0 0 36 36"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M18.5 6.5v22M29.5 17.5h-22M10.369 14.63L7.5 17.5l2.869 2.87M15.63 25.63l2.87 2.87 2.869-2.87M26.63 20.37l2.87-2.87-2.87-2.87M21.369 9.37l-2.87-2.87-2.868 2.87"
      stroke="currentColor"
      strokeLinecap="round"
      strokeLinejoin="round"
    ></path>
  </svg>
)
