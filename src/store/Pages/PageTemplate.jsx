import { useHistory } from 'react-router-dom'

import { LeftArrow } from '../assets/Icons'

import {
  template__container,
  template__content,
  template__arrow
} from './PageTemplate.module.css'

const PageTemplate = ({ children }) => {
  const history = useHistory()

  return (
    <div className={template__container}>
      <div className={template__content}>
        <div className={template__arrow}>
          <div onClick={() => history.goBack()}>
            <LeftArrow />
          </div>
        </div>
        {children}
      </div>
    </div>
  )
}

export default PageTemplate
