import { useState } from 'react'
import { useHistory } from 'react-router-dom'

import PageTemplate from '../PageTemplate'
import SearchBar from '../../../app/components/searchBar/SearchBar'

import {
  main__container,
  main__header,
  main__content,
  site__category,
  site__button__container,
  site__button,
  site__title
} from './Sites.module.css'


import { dataSites } from '../../../app/assets/images/siteImages'

const Sites = () => {
  const [keyword, setKeyword] = useState('')
  const [data, setData] = useState(dataSites)

  const updateInput = async (keyword) => {
    const filteredData = dataSites.filter(site => site.site.toLowerCase().includes(keyword.toLowerCase()))

    setKeyword(keyword)
    setData(filteredData)
  }

  return (
    <PageTemplate>
      <main className={main__container}>
        <div className={main__header}>
          <div>
            <h1>Select all the sites your loved one needs help with.</h1>
          </div>
          <SearchBar keyword={keyword} setKeyword={updateInput} />
        </div>
        <div className={main__content}>
          {data.filter(site => site.category === 'Top').length > 0 && (
            <SiteContainer category="Top">
              {data
                .filter(site => site.category === 'Top')
                .map(({ id, site, img }) => (
                  <SiteButton
                    key={id}
                    id={id}
                    name={site}
                    img={img}
                  />
                ))}
            </SiteContainer>
          )}
          {data.filter(site => site.category === 'Social').length > 0 && (
            <SiteContainer category="Social">
              {data
                .filter(site => site.category === 'Social')
                .map(({ id, site, img }) => (
                  <SiteButton
                    key={id}
                    id={id}
                    name={site}
                    img={img}
                  />
                ))}
            </SiteContainer>
          )}
          {data.filter(site => site.category === 'Shopping').length > 0 && (
            <SiteContainer category="Shopping">
              {data
                .filter(site => site.category === 'Shopping')
                .map(({ id, site, img }) => (
                  <SiteButton
                    key={id}
                    id={id}
                    name={site}
                    img={img}
                  />
                ))}
            </SiteContainer>
          )}
          {data.filter(site => site.category === 'Payment, Banks And Trading').length > 0 && (
            <SiteContainer category='Payment, Banks And Trading'>
              {data
                .filter(site => site.category === 'Payment, Banks And Trading')
                .map(({ id, site, img }) => (
                  <SiteButton
                    key={id}
                    id={id}
                    name={site}
                    img={img}
                  />
                ))}
            </SiteContainer>
          )}
          {data.filter(site => site.category === 'Software And Publishing').length > 0 && (
            <SiteContainer category="Software And Publishing">
              {data
                .filter(site => site.category === 'Software And Publishing')
                .map(({ id, site, img }) => (
                  <SiteButton
                    key={id}
                    id={id}
                    name={site}
                    img={img}
                  />
                ))}
            </SiteContainer>
          )}
          {data.filter(site => site.category === 'Telecom').length > 0 && (
            <SiteContainer category="Telecom">
              {data
                .filter(site => site.category === 'Telecom')
                .map(({ id, site, img }) => (
                  <SiteButton
                    key={id}
                    id={id}
                    name={site}
                    img={img}
                  />
                ))}
            </SiteContainer>
          )}
          {data.filter(site => site.category === 'Dating').length > 0 && (
            <SiteContainer category="Dating">
              {data
                .filter(site => site.category === 'Dating')
                .map(({ id, site, img }) => (
                  <SiteButton
                    key={id}
                    id={id}
                    name={site}
                    img={img}
                  />
                ))}
            </SiteContainer>
          )}
          {data.filter(site => site.category === 'Photo & Video').length > 0 && (
            <SiteContainer category="Photo & Video">
              {data
                .filter(site => site.category === 'Photo & Video')
                .map(({ id, site, img }) => (
                  <SiteButton
                    key={id}
                    id={id}
                    name={site}
                    img={img}
                  />
                ))}
            </SiteContainer>
          )}
          {data.filter(site => site.category === 'Crypto').length > 0 && (
            <SiteContainer category="Crypto">
              {data
                .filter(site => site.category === 'Crypto')
                .map(({ id, site, img }) => (
                  <SiteButton
                    key={id}
                    id={id}
                    name={site}
                    img={img}
                  />
                ))}
            </SiteContainer>
          )}
          {data.filter(site => site.category === 'China').length > 0 && (
            <SiteContainer category="China">
              {data
                .filter(site => site.category === 'China')
                .map(({ id, site, img }) => (
                  <SiteButton
                    key={id}
                    id={id}
                    name={site}
                    img={img}
                  />
                ))}
            </SiteContainer>
          )}
          {data.filter(site => site.category === 'Music, News And Gaming').length > 0 && (
            <SiteContainer category="Music, News And Gaming">
              {data
                .filter(site => site.category === 'Music, News And Gaming')
                .map(({ id, site, img }) => (
                  <SiteButton
                    key={id}
                    id={id}
                    name={site}
                    img={img}
                  />
                ))}
            </SiteContainer>
          )}
          {data.filter(site => site.category === 'TV').length > 0 && (
            <SiteContainer category="TV">
              {data
                .filter(site => site.category === 'TV')
                .map(({ id, site, img }) => (
                  <SiteButton
                    key={id}
                    id={id}
                    name={site}
                    img={img}
                  />
                ))}
            </SiteContainer>
          )}
          {data.filter(site => site.category === 'Insurance').length > 0 && (
            <SiteContainer category="Insurance">
              {data
                .filter(site => site.category === 'Insurance')
                .map(({ id, site, img }) => (
                  <SiteButton
                    key={id}
                    id={id}
                    name={site}
                    img={img}
                  />
                ))}
            </SiteContainer>
          )}
        </div>
      </main>
    </PageTemplate>
  )
}

const SiteContainer = ({ children, category }) => {
  return (
    <div>
      <p className={site__category}>{category}</p>
      <div className={site__button__container}>{children}</div>
    </div>
  )
}

const SiteButton = ({ id, name, img }) => {
  const history = useHistory()

  const handleClick = () => {
    history.push(`/store/${id}`)
  }

  return (
    <button className={site__button} onClick={handleClick}>
      <img alt="" src={img} />
      <span className={site__title}>
        <span>{name}</span>
      </span>
    </button>
  )
}

export default Sites
