import {
  Delete,
  Card,
  Download,
  CheckShield,
  CrossArrow
} from '../../assets/Icons'

const actions = {
  delete: {
    type: 'Delete',
    price: '39.99',
    icon: <Delete />,
    desc: 'Delete page to protect your loved ones.'
  },
  extract_funds: {
    type: 'Extract funds',
    price: '99.99',
    icon: <Card />,
    desc: 'Extract funds from page to protect your loved ones.'
  },
  extract_data: {
    type: 'Extract data',
    price: '99.99',
    icon: <Download />,
    desc: 'Extract data from page to protect your loved ones.'
  },
  memorialize: {
    type: 'Memorialize',
    price: '49.99',
    icon: <CheckShield />,
    desc: 'Memorialize page to protect your loved ones.'
  },
  full_access: {
    type: 'Full access',
    price: '99.99',
    icon: <CrossArrow />,
    desc: 'Get full access to page to protect your loved ones.'
  }
}

export const data = [
  {
    id: 1,
    site: 'Netflix',
    category: 'Top',
    img: '',
    actions: [actions.delete]
  },
  {
    id: 2,
    site: 'CoinBase',
    category: 'Top',
    img: '',
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 3,
    site: 'Twitter',
    category: 'Top',
    img: '',
    actions: [actions.delete]
  },
  {
    id: 4,
    site: 'LinkedIn',
    category: 'Top',
    img: '',
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 5,
    site: 'Snapchat',
    category: 'Top',
    img: '',
    actions: [actions.delete]
  },
  {
    id: 6,
    site: 'Microsoft',
    category: 'Top',
    img: '',
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 7,
    site: 'Spotify',
    category: 'Top',
    img: '',
    actions: [actions.delete]
  },
  {
    id: 8,
    site: 'PayPal',
    category: 'Top',
    img: '',
    actions: [actions.delete, actions.extract_funds]
  },
  {
    id: 9,
    site: 'Facebook',
    category: 'Top',
    img: '',
    actions: [actions.delete, actions.memorialize, actions.extract_data]
  },
  {
    id: 10,
    site: 'Instagram',
    category: 'Top',
    img: '',
    actions: [actions.delete, actions.memorialize]
  },
  {
    id: 11,
    site: 'GoDaddy',
    category: 'Top',
    img: '',
    actions: [actions.delete, actions.full_access]
  },
  {
    id: 12,
    site: 'Google',
    category: 'Top',
    img: '',
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 13,
    site: 'Amazon',
    category: 'Top',
    img: '',
    actions: [actions.delete, actions.extract_data]
  },
  {
    id: 14,
    site: 'Apple',
    category: 'Top',
    img: '',
    actions: [actions.delete, actions.extract_data]
  }
]
