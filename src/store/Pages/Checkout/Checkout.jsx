import { useForm, FormProvider } from 'react-hook-form'

import PageTemplate from '../PageTemplate'
import TextInput from '../../../app/components/inputs/TextInput'

import { Tick } from '../../assets/Icons'
import { Visa, Master, JCB, AmericanExpress } from '../../../app/assets/Icons'

import {
  content,
  title,
  right__side,
  left__side,
  checkbox,
  tick,
  left__content,
  form,
  credit__cards,
  pay
} from './Checkout.module.css'
import { useContext } from 'react'
import StoreContext from '../../context/store/storeContext'

const Checkout = () => {
  const methods = useForm()
  const { buyLovedOne } = useContext(StoreContext)

  const onSubmit = data => {
    buyLovedOne(data)
  }

  return (
    <PageTemplate>
      <main style={{ marginRight: 120 }}>
        <h1 className={title}>Checkout.</h1>
        <div className={content}>
          <section className={right__side}>
            <p>Confirmation</p>
            <p>
              After the payment we will need you to upload some necessary
              information and documents about the deceased person to be able to
              finish your order. We won't be able to offer refunds once an order
              is placed so please make sure you have all necessary information
              and documentation before you place your order. By confirming you
              agree to the General Terms and Conditions and the Privacy Policy.
            </p>
            <label className={checkbox}>
              <input name="toc" type="checkbox" />
              <div className={tick}>
                <Tick />
              </div>
              <p>I understand and confirm.</p>
            </label>
          </section>
          <section className={left__side}>
            <div className={left__content}>
              <div>
                <p>Billing details</p>
                <div className={credit__cards}>
                  <Visa />
                  <Master />
                  <AmericanExpress />
                  <JCB />
                </div>
                <FormProvider {...methods}>
                  <form onSubmit={methods.handleSubmit(onSubmit)} className={form}>
                    <TextInput
                      label="Card number"
                      name="card_number"
                      required
                      type='number'
                      fn={(e) => e.target.value = e.target.value.slice(0, 16)}
                    />
                    <TextInput
                      label="ZIP code"
                      name="zip_code"
                      required
                      type='number'
                    />
                    <TextInput
                      label="Expiration Date"
                      name="card_date"
                      required
                      type='number'
                      fn={(e) => e.target.value = e.target.value.slice(0, 4)}
                    />
                    <TextInput
                      label="CVV/CVC"
                      name="card_cvc"
                      required
                      type='number'
                      fn={(e) => e.target.value = e.target.value.slice(0, 3)}
                    />
                    <button className={pay}>Pay</button>
                  </form>
                </FormProvider>
              </div>
            </div>
          </section>
        </div>
      </main>
    </PageTemplate>
  )
}

export default Checkout
