import { useContext } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { v4 as uuidv4 } from 'uuid'

import StoreContext from '../../context/store/storeContext'

import PageTemplate from '../PageTemplate'

import {
  title,
  description,
  card__container,
  card,
  card__top,
  card__bottom,
  card__icon,
  card__price
} from './SiteAction.module.css'

import { dataSites } from '../../../app/assets/images/siteImages'

const SiteAction = () => {
  const { siteId } = useParams()

  const sites = dataSites.filter((site) => site.id.toString() === siteId)
  const { site, actions } = sites[0]

  return (
    <PageTemplate>
      <main style={{ marginRight: 120 }}>
        <h1 className={title}>{site}. Select action. </h1>
        <p className={description}>
          Everwill will help you with this account, let us know which action
          you need help with today. After completing your order you'll need to
          prepare{' '}
          <strong>
            User Email, User Payment Information, Date of Death, Customer
            Passport or Official ID, User Full Name, and Power of Attorney
          </strong>{' '}
          for these actions.
        </p>
        <div className={card__container}>
          {actions.map((action) => (
            <ActionCard key={action.type} {...action} site={site} />
          ))}
        </div>
      </main>
    </PageTemplate>
  )
}

const ActionCard = ({ type, price, icon, desc, site }) => {
  const { addItem } = useContext(StoreContext)
  const history = useHistory()

  const handleClick = () => {
    addItem({ site, price, type, cartId: uuidv4() })
    history.push('/store')
  }

  return (
    <div className={card} onClick={handleClick}>
      <div className={card__top}>
        <div className={card__icon}>{icon}</div>
        <div className={card__price}>
          <span>{price} USD</span>
        </div>
      </div>
      <div className={card__bottom}>
        <strong>{type}</strong>
        <span>{desc}</span>
      </div>
    </div>
  )
}

export default SiteAction
