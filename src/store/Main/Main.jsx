import { Switch, Route } from 'react-router-dom'

import Header from '../Header/Header'
import Sites from '../Pages/Sites/Sites'
import SiteAction from '../Pages/SiteAction/SiteAction'
import Checkout from '../Pages/Checkout/Checkout'

import { main__container } from './Main.module.css'

const Main = () => {
  return (
    <div className={main__container}>
      <Header />
      <Switch>
        <Route exact path="/store" component={Sites} />
        <Route path="/store/checkout" component={Checkout} />
        <Route path="/store/:siteId" component={SiteAction} />
      </Switch>
    </div>
  )
}

export default Main
